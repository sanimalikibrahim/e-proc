<?php
defined('BASEPATH') or exit('No direct script access allowed');

class SupplierbarangModel extends CI_Model
{
  private $_table = 'supplier_barang';
  private $_tableView = 'view_supplier_barang';

  public function rules()
  {
    return array(
      [
        'field' => 'supplier_id',
        'label' => 'Supplier',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'jenis',
        'label' => 'Jenis',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'kode_barang',
        'label' => 'Kode Barang',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'nama_barang',
        'label' => 'Nama Barang',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'satuan',
        'label' => 'Satuan',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'quantity',
        'label' => 'Jumlah',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'harga_satuan',
        'label' => 'Harga Satuan',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'total_harga',
        'label' => 'Total Harga',
        'rules' => 'required|trim'
      ]
    );
  }

  public function getAll($params = array(), $orderField = null, $orderBy = 'asc')
  {
    $this->db->where($params);

    if (!is_null($orderField)) {
      $this->db->order_by($orderField, $orderBy);
    };

    return $this->db->get($this->_tableView)->result();
  }

  public function getDetail($params = array())
  {
    return $this->db->where($params)->get($this->_tableView)->row();
  }

  public function insert()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->supplier_id = $this->input->post('supplier_id');
      $this->jenis = $this->input->post('jenis');
      $this->kode_barang = $this->input->post('kode_barang');
      $this->nama_barang = $this->input->post('nama_barang');
      $this->merk = $this->input->post('merk');
      $this->satuan = $this->input->post('satuan');
      $this->quantity = $this->clean_number($this->input->post('quantity'));
      $this->harga_satuan = $this->clean_number($this->input->post('harga_satuan'));
      $this->total_harga = $this->clean_number($this->input->post('total_harga'));
      $this->parameter = $this->input->post('parameter');
      $this->created_by = $this->session->userdata('user')['id'];
      $this->db->insert($this->_table, $this);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function insertBatch($data)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->insert_batch($this->_table, $data);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function update($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->supplier_id = $this->input->post('supplier_id');
      $this->jenis = $this->input->post('jenis');
      $this->kode_barang = $this->input->post('kode_barang');
      $this->nama_barang = $this->input->post('nama_barang');
      $this->merk = $this->input->post('merk');
      $this->satuan = $this->input->post('satuan');
      $this->quantity = $this->clean_number($this->input->post('quantity'));
      $this->harga_satuan = $this->clean_number($this->input->post('harga_satuan'));
      $this->total_harga = $this->clean_number($this->input->post('total_harga'));
      $this->parameter = $this->input->post('parameter');
      $this->updated_at = date('Y-m-d H:i:s');
      $this->updated_by = $this->session->userdata('user')['id'];
      $this->db->update($this->_table, $this, array('id' => $id));

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function delete($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->delete($this->_table, array('id' => $id));

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  public function truncate()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->truncate($this->_table);

      $response = array('status' => true, 'data' => 'Data has been truncated.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to truncate your data.');
    };

    return $response;
  }

  function br2nl($text)
  {
    return str_replace("\r\n", '<br/>', htmlspecialchars_decode($text));
  }

  function clean_number($number)
  {
    return preg_replace('/[^0-9]/', '', $number);
  }
}
