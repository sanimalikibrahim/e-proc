<?php
defined('BASEPATH') or exit('No direct script access allowed');

class PoModel extends CI_Model
{
  private $_table = 'po';
  private $_tableView = 'view_po';
  private $_columns = array(); // Urutan (index) harus sama dengan template excel, dan penamaan harus sama dengan tabel (case-sensitive)

  public function getColumnName($columnIndex)
  {
    $temp = array_combine(range(1, count($this->_columns)), array_values($this->_columns)); // Reset index to 1
    $result = (isset($temp[$columnIndex])) ? $temp[$columnIndex] : 0; // Get value
    return $result;
  }

  public function rules()
  {
    return array(
      [
        'field' => 'nomor',
        'label' => 'Nomor',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'tanggal',
        'label' => 'Tanggal',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'supplier_id',
        'label' => 'Supplier',
        'rules' => 'required|trim'
      ],
    );
  }

  public function generateFromPr($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $pr = $this->db->where(array('id' => $id, 'status' => 1))->get('pr')->row();

      if (!is_null($pr)) {
        // Store master
        $this->pr_id = $id;
        $this->supplier_id = $pr->supplier_id;
        $this->nomor = $this->generateNomor();
        $this->tanggal = date('Y-m-d');
        $this->note = $pr->note;
        $this->total_price = $pr->total_price;
        $this->status = 0;
        $this->created_by = $this->session->userdata('user')['id'];
        $this->db->insert($this->_table, $this);

        $this->temp_id = $this->db->insert_id();
        // END ## Store Master

        // Store items
        $masterItemQuery = "
          INSERT INTO po_item (id, po_id, po_item_parent_id, nomor, description, note, quantity, unit, unit_price, total_price, is_bold, is_italic)
          SELECT t.id, '$this->temp_id', t.pr_item_parent_id, t.nomor, t.description, t.note, t.quantity, t.unit, t.unit_price, t.total_price, t.is_bold, t.is_italic
          FROM pr_item t
          WHERE t.pr_id = '$id'
        ";
        $this->db->query($masterItemQuery);
        // END ## Store items

        $response = array('status' => true, 'data' => 'Data has been saved.', 'data_id' => $this->temp_id);
      } else {
        $response = array('status' => false, 'data' => 'Purchase Request is not found.', 'data_id' => null);
      };
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to create PO data from PR.', 'data_id' => null);
    };

    return $response;
  }

  public function generateNomor()
  {
    $query = "SELECT LPAD(LEFT(nomor, 5) + 1, 5, '0') as auto_nomor FROM po ORDER BY LEFT(nomor, 5) DESC LIMIT 1";
    $result = $this->db->query($query)->row();

    if (count($result) > 0) {
      $nomor = $result->auto_nomor;
    } else {
      $nomor = '00001';
    };

    $roman_month = array(1 => 'I', 'II', 'III', 'IV', 'V', 'VI', 'VII', 'VIII', 'IX', 'X', 'XI', 'XII');
    $month = $roman_month[date('n')];
    $year = date('y');
    $result = $nomor . '/PO/AJM/' . $month . '/' . $year;

    return $result;
  }

  public function getAll($params = [])
  {
    return $this->db->where($params)->get($this->_tableView)->result();
  }

  public function getDetail($params = [])
  {
    return $this->db->where($params)->get($this->_tableView)->row();
  }

  public function insert()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $nomor = $this->input->post('nomor');
      $validateNomor = $this->getDetail(['nomor' => $nomor]);

      if (count($validateNomor) > 0) {
        $response = array('status' => false, 'data' => 'Nomor already exist.', 'data_id' => null);
      } else {
        $this->supplier_id = $this->input->post('supplier_id');
        $this->nomor = $nomor;
        $this->tipe = $this->input->post('tipe');
        $this->ref_nomor = $this->input->post('ref_nomor');
        $this->tanggal = $this->input->post('tanggal');
        $this->note = $this->input->post('note');
        // $this->total_price = $this->clean_number($this->input->post('total_price')); // Update this field only when pr_item inserted
        $this->status = 0;
        $this->created_by = $this->session->userdata('user')['id'];
        $this->db->insert($this->_table, $this);

        $this->temp_id = $this->db->insert_id();

        $response = array('status' => true, 'data' => 'Data has been saved.', 'data_id' => $this->temp_id);
      };
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.', 'data_id' => null);
    };

    return $response;
  }

  public function insertBatch($data)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->insert_batch($this->_table, $data);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function update($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');
    $temp = $this->getDetail(['id' => $id]);
    $existNomor = false;

    try {
      $nomor = $this->input->post('nomor');

      if ($nomor != $temp->nomor) {
        $validateNomor = $this->getDetail(['nomor' => $nomor]);

        if (count($validateNomor) > 0) {
          $existNomor = true;
        };
      };

      if ($existNomor === true) {
        $response = array('status' => false, 'data' => 'Nomor already exist.');
      } else {
        $this->supplier_id = $this->input->post('supplier_id');
        $this->nomor = $nomor;
        $this->tipe = $this->input->post('tipe');
        $this->ref_nomor = $this->input->post('ref_nomor');
        $this->tanggal = $this->input->post('tanggal');
        $this->note = $this->input->post('note');
        // $this->total_price = $this->clean_number($this->input->post('total_price')); // Update this field only when pr_item inserted
        $this->updated_at = date('Y-m-d H:i:s');
        $this->updated_by = $this->session->userdata('user')['id'];
        $this->db->update($this->_table, $this, ['id' => $id]);

        $response = array('status' => true, 'data' => 'Data has been saved.', 'data_id' => $id);
      };
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function revisi($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->update($this->_table, array('status' => 0, 'is_editable' => 0), ['id' => $id, 'status' => 3, 'is_editable' => 0]);

      $response = array('status' => true, 'data' => 'Revisi has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function updateTotalPrice($id = null)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $query = "
        UPDATE " . $this->_table . "
        SET total_price =  (
          SELECT (SUM(IFNULL(total_price, 0))) AS grand_total
          FROM po_item
          WHERE po_id = '$id'
        )
        WHERE id = '$id';
      ";
      $this->db->query($query);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function setStatus($id = null, $status = null)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->update($this->_table, array('status' => $status), ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function setEditable($id = null, $status = null)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->update($this->_table, array('is_editable' => $status), ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function delete($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->delete('po_item', ['po_id' => $id]);
      $this->db->delete($this->_table, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  public function truncate()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->truncate($this->_table);
      $this->db->truncate('po_item');

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  function br2nl($text)
  {
    return str_replace("\r\n", '<br/>', htmlspecialchars_decode($text));
  }

  function clean_number($number)
  {
    return preg_replace('/[^0-9.]/', '', $number);
  }
}
