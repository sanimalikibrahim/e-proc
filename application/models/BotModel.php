<?php
defined('BASEPATH') or exit('No direct script access allowed');

class BotModel extends CI_Model
{
  private $_table = 'bot';
  private $_tableView = '';
  private $_columns = array(); // Urutan (index) harus sama dengan template excel, dan penamaan harus sama dengan tabel (case-sensitive)

  public function getColumnName($columnIndex)
  {
    $temp = array_combine(range(1, count($this->_columns)), array_values($this->_columns)); // Reset index to 1
    $result = (isset($temp[$columnIndex])) ? $temp[$columnIndex] : 0; // Get value
    return $result;
  }

  public function rules()
  {
    return array(
      [
        'field' => 'nomor',
        'label' => 'Nomor',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'tanggal',
        'label' => 'Tanggal',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'customer_name',
        'label' => 'Customer Name',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'project_name',
        'label' => 'Project Name',
        'rules' => 'required|trim'
      ],
    );
  }

  public function generateFromBom($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $bom = $this->db->where(array('id' => $id, 'status' => 1, 'is_create_bot' => 0))->get('bom')->row();

      if (!is_null($bom)) {
        // Store master
        $this->bom_id = $id;
        $this->nomor = $this->generateNomor();
        $this->tanggal = date('Y-m-d');
        $this->po_nomor = $bom->po_nomor;
        $this->po_tanggal = $bom->po_tanggal;
        $this->customer_name = $bom->customer_name;
        $this->customer_address = $bom->customer_address;
        $this->customer_attn = $bom->customer_attn;
        $this->project_name = $bom->project_name;
        $this->status = 0;
        $this->created_by = $this->session->userdata('user')['id'];
        $this->db->insert($this->_table, $this);

        $this->temp_id = $this->db->insert_id();
        // END ## Store Master

        // Set lock editable
        $this->db->update('bom', array('is_create_bot' => 1), ['id' => $id]);

        $response = array('status' => true, 'data' => 'Data has been saved.', 'data_id' => $this->temp_id);
      } else {
        $response = array('status' => false, 'data' => 'Bill of Material is not found.', 'data_id' => null);
      };
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to create BoT data from BoM.', 'data_id' => null);
    };

    return $response;
  }

  public function generateNomor()
  {
    $query = "SELECT LPAD(LEFT(nomor, 5) + 1, 5, '0') as auto_nomor FROM bot ORDER BY LEFT(nomor, 5) DESC LIMIT 1";
    $result = $this->db->query($query)->row();

    if (count($result) > 0) {
      $nomor = $result->auto_nomor;
    } else {
      $nomor = '00001';
    };

    $roman_month = array(1 => 'I', 'II', 'III', 'IV', 'V', 'VI', 'VII', 'VIII', 'IX', 'X', 'XI', 'XII');
    $month = $roman_month[date('n')];
    $year = date('y');
    $result = $nomor . '/BOT/AJM/' . $month . '/' . $year;

    return $result;
  }

  public function getAll($params = [])
  {
    return $this->db->where($params)->get($this->_table)->result();
  }

  public function getDetail($params = [])
  {
    return $this->db->where($params)->get($this->_table)->row();
  }

  public function insert()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $nomor = $this->input->post('nomor');
      $validateNomor = $this->getDetail(['nomor' => $nomor]);

      if (count($validateNomor) > 0) {
        $response = array('status' => false, 'data' => 'Nomor already exist.', 'data_id' => null);
      } else {
        $this->nomor = $nomor;
        $this->tanggal = $this->input->post('tanggal');
        $this->po_nomor = $this->input->post('po_nomor');
        $this->po_tanggal = $this->input->post('po_tanggal');
        $this->customer_name = $this->input->post('customer_name');
        $this->customer_address = $this->input->post('customer_address');
        $this->customer_attn = $this->input->post('customer_attn');
        $this->project_name = $this->input->post('project_name');
        $this->note = $this->input->post('note');
        $this->status = 0;
        $this->created_by = $this->session->userdata('user')['id'];
        $this->db->insert($this->_table, $this);

        $this->temp_id = $this->db->insert_id();

        $response = array('status' => true, 'data' => 'Data has been saved.', 'data_id' => $this->temp_id);
      };
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.', 'data_id' => null);
    };

    return $response;
  }

  public function insertBatch($data)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->insert_batch($this->_table, $data);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function update($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');
    $temp = $this->getDetail(['id' => $id]);
    $existNomor = false;

    try {
      $nomor = $this->input->post('nomor');

      if ($nomor != $temp->nomor) {
        $validateNomor = $this->getDetail(['nomor' => $nomor]);

        if (count($validateNomor) > 0) {
          $existNomor = true;
        };
      };

      if ($existNomor === true) {
        $response = array('status' => false, 'data' => 'Nomor already exist.');
      } else {
        $this->nomor = $nomor;
        $this->tanggal = $this->input->post('tanggal');
        $this->po_nomor = $this->input->post('po_nomor');
        $this->po_tanggal = $this->input->post('po_tanggal');
        $this->customer_name = $this->input->post('customer_name');
        $this->customer_address = $this->input->post('customer_address');
        $this->customer_attn = $this->input->post('customer_attn');
        $this->project_name = $this->input->post('project_name');
        $this->note = $this->input->post('note');
        $this->updated_at = date('Y-m-d H:i:s');
        $this->updated_by = $this->session->userdata('user')['id'];
        $this->db->update($this->_table, $this, ['id' => $id]);

        $response = array('status' => true, 'data' => 'Data has been saved.', 'data_id' => $id);
      };
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function setStatus($id = null, $status = null)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->update($this->_table, array('status' => $status), ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }


  public function delete($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->delete('bot_item', ['bot_id' => $id]);
      $this->db->delete($this->_table, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  public function truncate()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->truncate($this->_table);
      $this->db->truncate('bot_item');

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  function br2nl($text)
  {
    return str_replace("\r\n", '<br/>', htmlspecialchars_decode($text));
  }

  function clean_number($number)
  {
    return preg_replace('/[^0-9.]/', '', $number);
  }
}
