<?php
defined('BASEPATH') or exit('No direct script access allowed');

class BomModel extends CI_Model
{
  private $_table = 'bom';
  private $_tableView = '';
  private $_columns = array(); // Urutan (index) harus sama dengan template excel, dan penamaan harus sama dengan tabel (case-sensitive)

  public function getColumnName($columnIndex)
  {
    $temp = array_combine(range(1, count($this->_columns)), array_values($this->_columns)); // Reset index to 1
    $result = (isset($temp[$columnIndex])) ? $temp[$columnIndex] : 0; // Get value
    return $result;
  }

  public function rules()
  {
    return array(
      [
        'field' => 'nomor',
        'label' => 'Nomor',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'tanggal',
        'label' => 'Tanggal',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'customer_name',
        'label' => 'Customer Name',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'project_name',
        'label' => 'Project Name',
        'rules' => 'required|trim'
      ],
    );
  }

  public function generateFromLom($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $lom = $this->db->where(array('id' => $id, 'status' => 1, 'is_create_bom' => 0))->get('lom')->row();

      if (!is_null($lom)) {
        // Store master
        $this->lom_id = $id;
        $this->nomor = $this->generateNomor();
        $this->tanggal = date('Y-m-d');
        $this->po_nomor = $lom->po_nomor;
        $this->po_tanggal = $lom->po_tanggal;
        $this->customer_name = $lom->customer_name;
        $this->customer_address = $lom->customer_address;
        $this->customer_attn = $lom->customer_attn;
        $this->project_name = $lom->project_name;
        $this->status = 0;
        $this->created_by = $this->session->userdata('user')['id'];
        $this->db->insert($this->_table, $this);

        $this->temp_id = $this->db->insert_id();
        // END ## Store Master

        // Store items
        $masterItemQuery = "
          INSERT INTO bom_item (id, bom_id, bom_item_parent_id, nomor, description, note, quantity, unit, is_bold, is_italic)
          SELECT t.id, '$this->temp_id', t.lom_item_parent_id, t.nomor, t.description, t.note, t.quantity, t.unit, t.is_bold, t.is_italic
          FROM lom_item t
          WHERE t.lom_id = '$id'
        ";
        $this->db->query($masterItemQuery);
        // END ## Store items

        // Set lock editable
        $this->db->update('lom', array('is_create_bom' => 1), ['id' => $id]);

        $response = array('status' => true, 'data' => 'Data has been saved.', 'data_id' => $this->temp_id);
      } else {
        $response = array('status' => false, 'data' => 'List of Material is not found.', 'data_id' => null);
      };
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to create BoM data from LoM.', 'data_id' => null);
    };

    return $response;
  }

  public function generateNomor()
  {
    $query = "SELECT LPAD(LEFT(nomor, 5) + 1, 5, '0') as auto_nomor FROM bom ORDER BY LEFT(nomor, 5) DESC LIMIT 1";
    $result = $this->db->query($query)->row();

    if (count($result) > 0) {
      $nomor = $result->auto_nomor;
    } else {
      $nomor = '00001';
    };

    $roman_month = array(1 => 'I', 'II', 'III', 'IV', 'V', 'VI', 'VII', 'VIII', 'IX', 'X', 'XI', 'XII');
    $month = $roman_month[date('n')];
    $year = date('y');
    $result = $nomor . '/BOM/AJM/' . $month . '/' . $year;

    return $result;
  }

  public function getAll($params = [])
  {
    return $this->db->where($params)->get($this->_table)->result();
  }

  public function getDetail($params = [])
  {
    return $this->db->where($params)->get($this->_table)->row();
  }

  public function insert()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $nomor = $this->input->post('nomor');
      $validateNomor = $this->getDetail(['nomor' => $nomor]);

      if (count($validateNomor) > 0) {
        $response = array('status' => false, 'data' => 'Nomor already exist.', 'data_id' => null);
      } else {
        $this->nomor = $nomor;
        $this->tanggal = $this->input->post('tanggal');
        $this->po_nomor = $this->input->post('po_nomor');
        $this->po_tanggal = $this->input->post('po_tanggal');
        $this->customer_name = $this->input->post('customer_name');
        $this->customer_address = $this->input->post('customer_address');
        $this->customer_attn = $this->input->post('customer_attn');
        $this->project_name = $this->input->post('project_name');
        $this->note = $this->input->post('note');
        $this->status = 0;
        $this->created_by = $this->session->userdata('user')['id'];
        $this->db->insert($this->_table, $this);

        $this->temp_id = $this->db->insert_id();

        $response = array('status' => true, 'data' => 'Data has been saved.', 'data_id' => $this->temp_id);
      };
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.', 'data_id' => null);
    };

    return $response;
  }

  public function insertBatch($data)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->insert_batch($this->_table, $data);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function update($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');
    $temp = $this->getDetail(['id' => $id]);
    $existNomor = false;

    try {
      $nomor = $this->input->post('nomor');

      if ($nomor != $temp->nomor) {
        $validateNomor = $this->getDetail(['nomor' => $nomor]);

        if (count($validateNomor) > 0) {
          $existNomor = true;
        };
      };

      if ($existNomor === true) {
        $response = array('status' => false, 'data' => 'Nomor already exist.');
      } else {
        $this->nomor = $nomor;
        $this->tanggal = $this->input->post('tanggal');
        $this->po_nomor = $this->input->post('po_nomor');
        $this->po_tanggal = $this->input->post('po_tanggal');
        $this->customer_name = $this->input->post('customer_name');
        $this->customer_address = $this->input->post('customer_address');
        $this->customer_attn = $this->input->post('customer_attn');
        $this->project_name = $this->input->post('project_name');
        $this->note = $this->input->post('note');
        $this->updated_at = date('Y-m-d H:i:s');
        $this->updated_by = $this->session->userdata('user')['id'];
        $this->db->update($this->_table, $this, ['id' => $id]);

        $response = array('status' => true, 'data' => 'Data has been saved.', 'data_id' => $id);
      };
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function setStatus($id = null, $status = null)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->update($this->_table, array('status' => $status), ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }


  public function delete($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->delete('bom_item', ['bom_id' => $id]);
      $this->db->delete($this->_table, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  public function truncate()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->truncate($this->_table);
      $this->db->truncate('bom_item');

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  function br2nl($text)
  {
    return str_replace("\r\n", '<br/>', htmlspecialchars_decode($text));
  }

  function clean_number($number)
  {
    return preg_replace('/[^0-9.]/', '', $number);
  }
}
