<?php
defined('BASEPATH') or exit('No direct script access allowed');

class BarangjenisModel extends CI_Model
{
  private $_table = 'barang_jenis';
  private $_tableView = '';

  public function rules()
  {
    return array(
      [
        'field' => 'nama',
        'label' => 'Nama',
        'rules' => 'required|trim'
      ],
    );
  }

  public function getAll_nama()
  {
    $query = "SELECT nama FROM " . $this->_table . " GROUP BY nama ASC";
    return $this->db->query($query)->result();
  }

  public function getAll_parameter($nama = null)
  {
    $query = "SELECT DISTINCT parameter FROM " . $this->_table . " WHERE LOWER(nama) = LOWER('$nama')";
    $parameters = $this->db->query($query)->result();
    $result = array();

    if (count($parameters) > 0) {
      foreach ($parameters as $index => $item) {
        $parameter = trim($item->parameter);

        if (!is_null($parameter) && !empty($parameter)) {
          $result[] = array(
            'key' => preg_replace('/\s+/', '_', strtolower($parameter)),
            'label' => $parameter
          );
        };
      };
    };

    return $result;
  }

  public function getAll($params = array(), $orderField = null, $orderBy = 'asc')
  {
    $this->db->where($params);

    if (!is_null($orderField)) {
      $this->db->order_by($orderField, $orderBy);
    };

    return $this->db->get($this->_table)->result();
  }

  public function getDetail($params = array())
  {
    return $this->db->where($params)->get($this->_table)->row();
  }

  public function insert()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->nama = $this->input->post('nama');
      $this->parameter = $this->input->post('parameter');
      $this->created_by = $this->session->userdata('user')['id'];
      $this->db->insert($this->_table, $this);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function insertBatch($data)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->insert_batch($this->_table, $data);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function update($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->nama = $this->input->post('nama');
      $this->parameter = $this->input->post('parameter');
      $this->updated_at = date('Y-m-d H:i:s');
      $this->updated_by = $this->session->userdata('user')['id'];
      $this->db->update($this->_table, $this, array('id' => $id));

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function delete($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->delete($this->_table, array('id' => $id));

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  public function truncate()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->truncate($this->_table);

      $response = array('status' => true, 'data' => 'Data has been truncated.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to truncate your data.');
    };

    return $response;
  }

  function br2nl($text)
  {
    return str_replace("\r\n", '<br/>', htmlspecialchars_decode($text));
  }

  function clean_number($number)
  {
    return preg_replace('/[^0-9]/', '', $number);
  }
}
