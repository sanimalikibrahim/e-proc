<?php
defined('BASEPATH') or exit('No direct script access allowed');

class SkbitemModel extends CI_Model
{
  private $_table = 'skb_item';
  private $_tableView = '';
  private $_columns = array(); // Urutan (index) harus sama dengan template excel, dan penamaan harus sama dengan tabel (case-sensitive)

  public function getColumnName($columnIndex)
  {
    $temp = array_combine(range(1, count($this->_columns)), array_values($this->_columns)); // Reset index to 1
    $result = (isset($temp[$columnIndex])) ? $temp[$columnIndex] : 0; // Get value
    return $result;
  }

  public function rules()
  {
    return array(
      [
        'field' => 'skb_id',
        'label' => 'SKB ID',
        'rules' => 'required|trim'
      ],
    );
  }

  public function getAll($params = [])
  {
    return $this->db->where($params)->get($this->_table)->result();
  }

  public function getAll_sorted($penawaranId = null, $parentId = null)
  {
    $items = $this->db
      ->where(array(
        'skb_id' => $penawaranId,
        'skb_item_parent_id' => $parentId
      ))
      ->get($this->_table)
      ->result();

    $result = array();

    if (count($items) > 0) {
      foreach ($items as $index => $item) {
        $child = $this->getAll_sorted($penawaranId, $item->id);
        $result[] = $item;

        if (count($child) > 0) {
          $result[] = $child;
        };
      };
    };

    $result = $this->nestedToSingle($result);
    return $result;
  }

  public function nestedToSingle(array $array)
  {
    $singleDimArray = [];

    foreach ($array as $item) {

      if (is_array($item)) {
        $singleDimArray = array_merge($singleDimArray, $this->nestedToSingle($item));
      } else {
        $singleDimArray[] = $item;
      }
    }

    return $singleDimArray;
  }

  public function getDetail($params = [])
  {
    return $this->db->where($params)->get($this->_table)->row();
  }

  public function insert()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->skb_id = $this->input->post('skb_id');
      $this->skb_item_parent_id = $this->clean_number($this->input->post('skb_item_parent_id'));
      $this->nomor = $this->input->post('nomor');
      $this->description = $this->input->post('description');
      $this->note = $this->input->post('note');
      $this->quantity = $this->clean_number($this->input->post('quantity'));
      $this->unit = $this->input->post('unit');
      $this->is_bold = $this->input->post('is_bold');
      $this->is_italic = $this->input->post('is_italic');
      $this->db->insert($this->_table, $this);

      $this->temp_id = $this->db->insert_id();

      $response = array('status' => true, 'data' => 'Data has been saved.', 'data_id' => $this->temp_id);
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.', 'data_id' => null);
    };

    return $response;
  }

  public function insertBatch($data)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->insert_batch($this->_table, $data);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function update($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->skb_id = $this->input->post('skb_id');
      $this->skb_item_parent_id = $this->clean_number($this->input->post('skb_item_parent_id'));
      $this->nomor = $this->input->post('nomor');
      $this->description = $this->input->post('description');
      $this->note = $this->input->post('note');
      $this->quantity = $this->clean_number($this->input->post('quantity'));
      $this->unit = $this->input->post('unit');
      $this->is_bold = $this->input->post('is_bold');
      $this->is_italic = $this->input->post('is_italic');
      $this->db->update($this->_table, $this, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been saved.', 'data_id' => $id);
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.', 'data_id' => $id);
    };

    return $response;
  }

  public function delete($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->delete($this->_table, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  public function truncate()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->truncate($this->_table);

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  function br2nl($text)
  {
    return str_replace("\r\n", '<br/>', htmlspecialchars_decode($text));
  }

  function clean_number($number)
  {
    if (!is_null($number) && !empty($number)) {
      return preg_replace('/[^0-9.]/', '', $number);
    } else {
      return null;
    };
  }
}
