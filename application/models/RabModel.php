<?php
defined('BASEPATH') or exit('No direct script access allowed');

class RabModel extends CI_Model
{
  private $_table = 'rab';
  private $_tableView = '';
  private $_columns = array(); // Urutan (index) harus sama dengan template excel, dan penamaan harus sama dengan tabel (case-sensitive)

  public function getColumnName($columnIndex)
  {
    $temp = array_combine(range(1, count($this->_columns)), array_values($this->_columns)); // Reset index to 1
    $result = (isset($temp[$columnIndex])) ? $temp[$columnIndex] : 0; // Get value
    return $result;
  }

  public function rules()
  {
    return array(
      [
        'field' => 'nomor',
        'label' => 'Nomor',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'tanggal',
        'label' => 'Tanggal',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'customer_name',
        'label' => 'Customer Name',
        'rules' => 'required|trim'
      ],
      [
        'field' => 'project_name',
        'label' => 'Project Name',
        'rules' => 'required|trim'
      ],
    );
  }

  public function generateNomor()
  {
    $query = "SELECT LPAD(LEFT(nomor, 5) + 1, 5, '0') as auto_nomor FROM rab ORDER BY LEFT(nomor, 5) DESC LIMIT 1";
    $result = $this->db->query($query)->row();

    if (count($result) > 0) {
      $nomor = $result->auto_nomor;
    } else {
      $nomor = '00001';
    };

    $roman_month = array(1 => 'I', 'II', 'III', 'IV', 'V', 'VI', 'VII', 'VIII', 'IX', 'X', 'XI', 'XII');
    $month = $roman_month[date('n')];
    $year = date('y');
    $result = $nomor . '/RAB/AJM/' . $month . '/' . $year;

    return $result;
  }

  public function getAll($params = [])
  {
    return $this->db->where($params)->get($this->_table)->result();
  }

  public function getDetail($params = [])
  {
    return $this->db->where($params)->get($this->_table)->row();
  }

  public function insert()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $nomor = $this->input->post('nomor');
      $validateNomor = $this->getDetail(['nomor' => $nomor]);

      if (count($validateNomor) > 0) {
        $response = array('status' => false, 'data' => 'Nomor already exist.', 'data_id' => null);
      } else {
        $this->nomor = $nomor;
        $this->tanggal = $this->input->post('tanggal');
        $this->customer_name = $this->input->post('customer_name');
        $this->customer_address = $this->input->post('customer_address');
        $this->customer_attn = $this->input->post('customer_attn');
        $this->project_name = $this->input->post('project_name');
        $this->note = $this->input->post('note');
        // $this->total_price = $this->clean_number($this->input->post('total_price')); // Update this field only when rab_item inserted
        $this->status = 0;
        $this->created_by = $this->session->userdata('user')['id'];
        $this->db->insert($this->_table, $this);

        $this->temp_id = $this->db->insert_id();

        $response = array('status' => true, 'data' => 'Data has been saved.', 'data_id' => $this->temp_id);
      };
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.', 'data_id' => null);
    };

    return $response;
  }

  public function insertBatch($data)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->insert_batch($this->_table, $data);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function update($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');
    $temp = $this->getDetail(['id' => $id]);
    $existNomor = false;

    try {
      $nomor = $this->input->post('nomor');

      if ($nomor != $temp->nomor) {
        $validateNomor = $this->getDetail(['nomor' => $nomor]);

        if (count($validateNomor) > 0) {
          $existNomor = true;
        };
      };

      if ($existNomor === true) {
        $response = array('status' => false, 'data' => 'Nomor already exist.');
      } else {
        $this->nomor = $nomor;
        $this->tanggal = $this->input->post('tanggal');
        $this->customer_name = $this->input->post('customer_name');
        $this->customer_address = $this->input->post('customer_address');
        $this->customer_attn = $this->input->post('customer_attn');
        $this->project_name = $this->input->post('project_name');
        $this->note = $this->input->post('note');
        // $this->total_price = $this->clean_number($this->input->post('total_price')); // Update this field only when rab_item inserted
        $this->updated_at = date('Y-m-d H:i:s');
        $this->updated_by = $this->session->userdata('user')['id'];
        $this->db->update($this->_table, $this, ['id' => $id]);

        $response = array('status' => true, 'data' => 'Data has been saved.', 'data_id' => $id);
      };
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function updateTotalPrice($id = null)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $query = "
        UPDATE " . $this->_table . "
        SET total_price =  (
          SELECT (SUM(IFNULL(material_total_price, 0)) + SUM(IFNULL(labour_total_price, 0))) AS grand_total
          FROM rab_item
          WHERE rab_id = '$id'
        )
        WHERE id = '$id';
      ";
      $this->db->query($query);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  // Currently not used, maybe later
  public function revisi($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');
    $existNomor = false;

    try {
      $nomor = $this->input->post('nomor');
      $validateNomor = $this->getDetail(['nomor' => $nomor]);
      $existNomor = (count($validateNomor) > 0) ? true : false;

      if ($existNomor === true) {
        $response = array('status' => false, 'data' => 'Nomor already exist.');
      } else {
        // Create archive
        $oldRab = $this->getDetail(array('id' => $id));
        $newRab = array();

        if (!is_null($oldRab)) {
          foreach ($oldRab as $index => $item) {
            $newRab[$index] = $item;
            $newRab['revisi_parent_id'] = $id;
            $newRab['revisi_nomor'] = (int) $this->input->post('revisi_nomor') - 1;
            $newRab['revisi_date'] = date('Y-m-d');

            unset($newRab['id']);
          };

          $this->db->insert('rab_arsip', $newRab);
          $rabArsip_id = $this->db->insert_id();

          // Insert items
          $oldRabItems = $this->db->where(array('rab_id' => $id))->get('rab_item')->result();
          $newRabItems = array();

          if (count($oldRabItems) > 0) {
            foreach ($oldRabItems as $index => $item) {
              $newRabItems[$index] = $item;
              $newRabItems[$index]->rab_id = $rabArsip_id;
            };

            if (count($newRabItems) > 0) {
              $this->db->insert_batch('penawaran_arsip_item', $newRabItems);
            };
          };
        };
        // END ## Create archive

        // Update for revisi
        $this->nomor = $nomor;
        $this->tanggal = $this->input->post('tanggal');
        $this->customer_name = $this->input->post('customer_name');
        $this->customer_address = $this->input->post('customer_address');
        $this->customer_attn = $this->input->post('customer_attn');
        $this->project_name = $this->input->post('project_name');
        $this->note = $this->input->post('note');
        $this->total_price = $this->clean_number($this->input->post('total_price'));
        $this->revisi_nomor = $this->input->post('revisi_nomor');
        $this->revisi_date = date('Y-m-d');
        $this->status = 0;
        $this->created_by = $this->session->userdata('user')['id'];
        $this->created_at = date('Y-m-d H:i:s');
        $this->updated_at = null;
        $this->updated_by = null;
        $this->db->update($this->_table, $this, ['id' => $id]);
        // END ## Update for revisi

        $response = array('status' => true, 'data' => 'Data has been saved.', 'data_id' => $id);
      };
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }

  public function setStatus($id = null, $status = null)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->update($this->_table, array('status' => $status), ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been saved.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to save your data.');
    };

    return $response;
  }


  public function delete($id)
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->delete('rab_item', ['rab_id' => $id]);
      $this->db->delete($this->_table, ['id' => $id]);

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  public function truncate()
  {
    $response = array('status' => false, 'data' => 'No operation.');

    try {
      $this->db->truncate($this->_table);
      $this->db->truncate('rab_item');

      $response = array('status' => true, 'data' => 'Data has been deleted.');
    } catch (\Throwable $th) {
      $response = array('status' => false, 'data' => 'Failed to delete your data.');
    };

    return $response;
  }

  function br2nl($text)
  {
    return str_replace("\r\n", '<br/>', htmlspecialchars_decode($text));
  }

  function clean_number($number)
  {
    return preg_replace('/[^0-9.]/', '', $number);
  }
}
