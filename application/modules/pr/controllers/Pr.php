<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . 'controllers/AppBackend.php');

class Pr extends AppBackend
{
  function __construct()
  {
    parent::__construct();
    $this->load->model([
      'AppModel',
      'SupplierModel',
      'SupplierbarangModel',
      'BarangjenisModel',
      'PrModel',
      'PritemModel',
      'PoModel',
      'SkbModel',
    ]);
    $this->load->library('form_validation');
  }

  public function index()
  {
    $this->_tableList('index');
  }

  private function _tableList($module = 'index', $pageTitle = null)
  {
    $agent = new Mobile_Detect;
    $pageTitle = (!is_null($pageTitle)) ? ' › ' . $pageTitle : '';
    $data = array(
      'app' => $this->app(),
      'main_js' => $this->load_main_js('pr', false, array(
        'controller' => $this,
        'module' => $module,
      )),
      'card_title' => 'Purchase Request' . $pageTitle,
      'is_mobile' => $agent->isMobile(),
      'module' => $module,
    );
    $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
    $this->template->load_view('index', $data, TRUE);
    $this->template->render();
  }

  public function wizard($id = null, $wizardId = 1)
  {
    $src = $this->input->get('src');

    if ($src === 'revisi') {
      $isRevisi = true;
    } else {
      $isRevisi = false;
    };

    switch ($wizardId) {
      case '1':
        $this->_wizardForm_1($id, $isRevisi);
        break;
      case '2':
        $this->_wizardForm_2($id, $isRevisi);
        break;
      default:
        $this->_wizardForm_1($id, $isRevisi);
        break;
    };
  }

  private function _wizardForm_1($id = null, $isRevisi = false)
  {
    $agent = new Mobile_Detect;
    $pr = $this->PrModel->getDetail(array('id' => $id, 'status' => 0));

    if (!is_null($id) && is_null($pr)) {
      show_404();
    } else {
      $supplier = $this->SupplierModel->getAll(array(), 'nama_supplier', 'asc');
      $supplierId_temp = (isset($pr->supplier_id)) ? $pr->supplier_id : null;

      $data = array(
        'app' => $this->app(),
        'main_js' => $this->load_main_js('pr', false, array(
          'controller' => $this,
          'pr_id' => $id,
          'is_revisi' => $isRevisi,
        )),
        'card_title' => 'Purchase Request',
        'is_mobile' => $agent->isMobile(),
        'pr' => $pr,
        'is_revisi' => $isRevisi,
        'generate_nomor' => $this->PrModel->generateNomor(),
        'supplier_list' => $this->init_list($supplier, 'id', 'nama_supplier', $supplierId_temp),
      );
      $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
      $this->template->load_view('wizard_form', $data, TRUE);
      $this->template->render();
    };
  }

  private function _wizardForm_2($id = null)
  {
    $agent = new Mobile_Detect;
    $pr = $this->PrModel->getDetail(array('id' => $id, 'status' => 0));

    if (!is_null($pr)) {
      $supplier = $this->SupplierModel->getDetail(array('id' => $pr->supplier_id));
      $barang = $this->SupplierbarangModel->getAll(array('supplier_id' => $pr->supplier_id));

      $data = array(
        'app' => $this->app(),
        'controller' => $this,
        'main_js' => $this->load_main_js('pr', false, array(
          'controller' => $this,
          'pr_id' => $id
        )),
        'card_title' => 'Purchase Request › Item',
        'is_mobile' => $agent->isMobile(),
        'pr' => $pr,
        'pr_item' => $this->PritemModel->getAll_sorted($id),
        'barang_list' => $this->init_list($barang, 'id', 'nama_barang'),
        'supplier' => $supplier,
      );
      $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
      $this->template->load_view('wizard_item_form', $data, TRUE);
      $this->template->render();
    } else {
      show_404();
    };
  }

  public function ajax_get_list()
  {
    $this->handle_ajax_request();
    $module = $this->input->get('src');
    $role = $this->session->userdata('user')['role'];

    switch ($module) {
      case 'index':
        if (strtolower($role) === strtolower('Procurement')) {
          $status = array(1);
          $pic = array();
        } else {
          $status = array();
          $pic = array('created_by' => $this->session->userdata('user')['id']);
        };
        break;
      default:
        $status = array();
        $pic = array('created_by' => $this->session->userdata('user')['id']);
        break;
    };

    $dtAjax_config = array(
      'table_name' => 'pr',
      'order_column' => 5,
      'order_column_dir' => 'desc',
      'static_conditional' => $pic,
      'static_conditional_in_key' => 'status',
      'static_conditional_in' => $status,
    );
    $response = $this->AppModel->getData_dtAjax($dtAjax_config);

    echo json_encode($response);
  }

  public function ajax_save($id = null)
  {
    $this->handle_ajax_request();
    $this->form_validation->set_rules($this->PrModel->rules());

    if ($this->form_validation->run() === true) {
      if (is_null($id)) {
        echo json_encode($this->PrModel->insert());
      } else {
        $isRevisi = $this->input->post('is_revisi');

        if (!is_null($isRevisi)) {
          echo json_encode($this->PrModel->revisi($id));
        } else {
          echo json_encode($this->PrModel->update($id));
        };
      };
    } else {
      $errors = validation_errors('<div>- ', '</div>');
      echo json_encode(array('status' => false, 'data' => $errors));
    };
  }

  public function ajax_set_status()
  {
    $this->handle_ajax_request();
    $prId = $this->input->get('pr_id');
    $status = $this->input->get('status');

    echo json_encode($this->PrModel->setStatus($prId, $status));
  }

  public function ajax_delete($id)
  {
    $this->handle_ajax_request();
    echo json_encode($this->PrModel->delete($id));
  }

  public function ajax_get_preview($id)
  {
    $this->handle_ajax_request();
    $pr = $this->PrModel->getDetail(array('id' => $id));

    if (!is_null($pr)) {
      $agent = new Mobile_Detect;
      $supplier = $this->SupplierModel->getDetail(array('id' => $pr->supplier_id));

      $data = array(
        'app' => $this->app(),
        'main_js' => $this->load_main_js('pr'),
        'card_title' => 'Purchase Request › View',
        'controller' => $this,
        'data_id' => $id,
        'pr' => $pr,
        'pr_item' => $this->PritemModel->getAll_sorted($id),
        'supplier' => $supplier,
      );

      if ($agent->isMobile()) {
        $this->load->view('_view_mobile', $data);
      } else {
        $this->load->view('_view', $data);
      };
    } else {
      show_404();
    };
  }

  // Pr Item
  public function ajax_get_pr_item_table($id = null)
  {
    $this->handle_ajax_request();
    $barang = $this->SupplierbarangModel->getAll();

    echo $this->load->view('_table_item', array(
      'app' => $this->app(),
      'pr' => $this->PrModel->getDetail(array('id' => $id)),
      'pr_item' => $this->PritemModel->getAll_sorted($id),
      'barang_list' => $this->init_list($barang, 'id', 'nama_barang'),
    ), TRUE);
  }

  public function ajax_save_item($id = null)
  {
    $this->handle_ajax_request();
    $this->form_validation->set_rules($this->PritemModel->rules());

    if ($this->form_validation->run() === true) {
      if (is_null($id)) {
        $transaction = $this->PritemModel->insert();
      } else {
        $transaction = $this->PritemModel->update($id);
      };

      if ($transaction['status'] === true) {
        // Store params when exist
        if (!is_null($this->input->post('description_as_barang'))) {
          $params = $this->input->post('description_param');
          $paramsPayload = array();

          if (!is_null($params)) {
            foreach ($params as $index => $item) {
              $paramsPayload[] = array(
                'pr_id' => $this->input->post('pr_id'),
                'pr_item_parent_id' => $transaction['data_id'],
                'description' => $item['label'] . ' : ' . $item['value'],
                'is_italic' => 1,
              );
            };

            if (count($paramsPayload) > 0) {
              $this->PritemModel->insertBatch($paramsPayload);
            };
          };
        };
        // END ## Store params when exist

        // Update master grand total
        $this->PrModel->updateTotalPrice($this->input->post('pr_id'));
      };

      echo json_encode($transaction);
    } else {
      $errors = validation_errors('<div>- ', '</div>');
      echo json_encode(array('status' => false, 'data' => $errors));
    };
  }

  public function ajax_add_empty_row($prId = null)
  {
    $this->handle_ajax_request();
    $_POST['pr_id'] = $prId;
    echo json_encode($this->PritemModel->insert());
  }

  public function ajax_delete_item($id)
  {
    $this->handle_ajax_request();
    echo json_encode($this->PritemModel->delete($id));
  }

  public function ajax_get_barang_param()
  {
    // $this->handle_ajax_request();
    $barangId = $this->input->get('barang_id');
    $barang = $this->SupplierbarangModel->getDetail(array('id' => $barangId));
    $result = null;

    if (!is_null($barang)) {
      $barangJenis = $this->BarangjenisModel->getAll_parameter($barang->jenis);
      $barangParameter = $barang->parameter;

      $result['unit'] = $barang->satuan;
      $result['unit_price'] = $barang->harga_satuan;
      $result['parameter'] = array();

      if (!is_null($barangParameter) && !empty($barangParameter)) {
        $barangParameter = json_decode($barangParameter);

        foreach ($barangParameter as $index => $item) {
          $findParamLabel = $this->searchInArrayObj($barangJenis, 'key', $index);

          if (count($findParamLabel) > 0) {
            $result['parameter'][] = array(
              'label' => $findParamLabel->label,
              'value' => $item
            );
          };
        };
      };
    };

    echo json_encode($result);
  }
  // END ## Pr Item

  public function generate_po($id = null)
  {
    $generate = $this->PoModel->generateFromPr($id);

    if ($generate['status'] === true) {
      $this->PrModel->setEditable($id, 1);
      redirect(base_url('po'));
    } else {
      show_error($generate['data'], '500');
    };
  }

  public function generate_skb($id = null)
  {
    $generate = $this->SkbModel->generateFromPr($id);

    if ($generate['status'] === true) {
      $this->PrModel->setEditable($id, 1);
      redirect(base_url('skb'));
    } else {
      show_error($generate['data'], '500');
    };
  }

  public function get_status($status)
  {
    switch ($status) {
      case '0':
        return 'Draft';
        break;
      case '1':
        return 'Submit';
        break;
      default:
        return 'Undefined';
        break;
    };
  }
}
