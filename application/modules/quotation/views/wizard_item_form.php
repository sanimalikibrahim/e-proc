<?php require_once('main.css.php') ?>
<?php $revisiNomor = null ?>
<?php $revisiNomorLabel = null ?>

<section id="suratjalan">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <span class="badge badge-info" style="position: absolute; margin-top: 2px;">
                        <?php if (isset($is_revisi) && $is_revisi == true) : ?>
                            <?php $revisiNomor = (!is_null($quotation->revisi_nomor)) ? (int) $quotation->revisi_nomor + 1 : 1; ?>
                            REV
                        <?php else : ?>
                            <?php $revisiNomorLabel = (isset($quotation->revisi_nomor) && !is_null($quotation->revisi_nomor)) ? ' : Rev. ' . $quotation->revisi_nomor : null; ?>
                            <?= (isset($quotation->id)) ? 'EDIT' : 'NEW' ?>
                        <?php endif ?>
                    </span>
                    <div style="margin-left: 50px;">
                        <h4 class="card-title"><?php echo (isset($card_title)) ? $card_title . $revisiNomorLabel : '' ?></h4>
                        <h6 class="card-subtitle"><?php echo (isset($card_subTitle)) ? $card_subTitle : '' ?></h6>
                    </div>
                    <div class="clear-card"></div>
                </div>
            </div>
            <div class="clear-card"></div>

            <!-- quotation -->
            <ul class="list-group list-group-flush">
                <li class="list-group-item">
                    <div class="row">
                        <div class="col-xs-12 col-auto"><b>Nomor :</b></div>
                        <div class="col-xs-12 col-md-10"><?= isset($quotation->nomor) ? $quotation->nomor : '' ?></div>
                    </div>
                </li>
                <li class="list-group-item">
                    <div class="row">
                        <div class="col-xs-12 col-auto"><b>Date :</b></div>
                        <div class="col-xs-12 col-md-10"><?= isset($quotation->tanggal) ? $controller->localizeDate($quotation->tanggal) : '' ?></div>
                    </div>
                </li>
                <li class="list-group-item">
                    <div class="row">
                        <div class="col-xs-12 col-auto"><b>Project Name :</b></div>
                        <div class="col-xs-12 col-md-10"><?= isset($quotation->project_name) ? $quotation->project_name : '' ?></div>
                    </div>
                </li>
                <li class="list-group-item">
                    <div class="row">
                        <div class="col-xs-12 col-auto"><b>Customer Name :</b></div>
                        <div class="col-xs-12 col-md-10"><?= isset($quotation->customer_name) ? $quotation->customer_name : '' ?></div>
                    </div>
                </li>
            </ul>
            <!-- END ## quotation -->

            <div class="mb-4"></div>
            <div id="table-quotation-item">
                <?php include_once("_table_item.php"); ?>
            </div>

            <small class="form-text text-muted">
                Fields with red stars (<label required></label>) are required.
            </small>

            <div class="row">
                <div class="col">
                    <div class="buttons-container">
                        <div class="row">
                            <div class="col">
                                <a href="<?= base_url('quotation/wizard/' . $quotation->id . '/1') ?>" class="btn btn--raised btn-dark btn--icon-text btn-custom mr-2">
                                    <i class="zmdi zmdi-long-arrow-left"></i>
                                    Back
                                </a>
                            </div>
                            <div class="col text-right">
                                <button class="btn btn--raised btn-primary btn--icon-text btn-custom quotation-action-sent spinner-action-button" data-id="<?= $quotation->id ?>">
                                    Submit
                                    <i class="zmdi zmdi-long-arrow-right"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>