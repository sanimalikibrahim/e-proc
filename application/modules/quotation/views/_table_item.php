<div class="table-responsive">
  <table class="table table-sm table-bordered table-hover">
    <thead style="text-align: center;">
      <tr>
        <th width="80">No</th>
        <th colspan="2">Works Description</th>
        <th colspan="2">Vol</th>
        <th colspan="2">Price (Rp)</th>
        <th width="50" class="text-center">#</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td colspan="8" class="p-0" style="height: 5px;"></td>
      </tr>
      <?php
      $subTotal = 0;
      ?>
      <?php if (count($quotation_item) > 0) : ?>
        <?php foreach ($quotation_item as $index => $item) : ?>
          <?php
          $isPreferences = '';
          $isPreferences .= ($item->is_bold == 1) ? 'font-weight: 600;' : '';
          $isPreferences .= ($item->is_italic == 1) ? 'font-style: italic;' : '';
          $subTotal = (!is_null($item->total_price) && !empty($item->total_price)) ? (float) $item->total_price + $subTotal : 0 + $subTotal;
          ?>
          <div class="item-rows item-rows-<?= $item->id ?>">
            <!-- Row data -->
            <tr class="item-row-wrapper item-row-wrapper-<?= $item->id ?>" style="<?= $isPreferences ?>">
              <td align="center" style="height: 30.48px;">
                <?= $item->nomor ?>
              </td>
              <td style="border-right-color: transparent;">
                <?= $item->description ?>
              </td>
              <td><?= $item->note ?></td>
              <td class="text-center" width="70"><?= (!is_null($item->quantity)) ? number_format($item->quantity, 0) : null ?></td>
              <td class="text-center" width="70"><?= $item->unit ?></td>
              <td class="text-right" width="130"><?= (!is_null($item->unit_price)) ? number_format($item->unit_price, 0) : null ?></td>
              <td class="text-right" width="130"><?= (!is_null($item->total_price)) ? number_format($item->total_price, 0) : null ?></td>
              <td align="center">
                <a href="javascript:;" data-id="<?= $item->id ?>" data-quotation_item_parent_id="<?= $item->quotation_item_parent_id ?>" data-nomor="<?= $item->nomor ?>" data-description="<?= $item->description ?>" data-note="<?= $item->note ?>" data-quantity="<?= $item->quantity ?>" data-unit="<?= $item->unit ?>" data-unit_price="<?= number_format($item->unit_price, 0, '', '') ?>" data-total_price="<?= number_format($item->total_price, 0, '', '') ?>" data-is_bold="<?= $item->is_bold ?>" data-is_italic="<?= $item->is_italic ?>" class="btn btn-dark btn-xs item-button-row item-button-row-edit item-button-row-edit-<?= $item->id ?>" title="Edit">
                  <i class="zmdi zmdi-edit"></i>
                </a>
              </td>
            </tr>
            <!-- END ## Row data -->
            <!-- Item inline form -->
            <tr class="item-form-row item-form-row-<?= $item->id ?> bg-light" style="display: none;">
              <td align="center">
                <span class="badge badge-warning item-form-row-icon-<?= $item->id ?>">N</span>
              </td>
              <td colspan="7" class="pl-0">
                <div class="item-form-input item-form-input-wrapper-<?= $item->id ?> bg-white p-3">
                  <!-- Loader -->
                  <div class="spinner spinner-<?= $item->id ?>">
                    <div class="lds-hourglass"></div>
                  </div>
                  <form method="post" id="item-form-input-<?= $item->id ?>">
                    <!-- Temporary hidden field -->
                    <input type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" value="<?= $this->security->get_csrf_hash(); ?>" />
                    <input type="hidden" name="quotation_id" value="<?= $quotation->id ?>" readonly />
                    <input type="hidden" name="quotation_item_parent_id" class="quotation_item-quotation_item_parent_id-<?= $item->id ?>" value="<?= $item->id ?>" readonly />

                    <div class="row">
                      <div class="col-xs-12 col-md-12">
                        <div class="row">
                          <div class="col-xs-12 col-md-2">
                            <div class="form-group mb-3">
                              <label>Item</label>
                              <input type="text" name="nomor" class="form-control quotation_item-nomor quotation_item-nomor-<?= $item->id ?>" placeholder="Item" readonly />
                              <i class="form-group__bar"></i>
                            </div>
                          </div>
                          <div class="col-xs-12 col-md-10">
                            <label>Preferences</label>
                            <div class="group-control mb-3">
                              <div class="row">
                                <div class="col-xs-12 col-md-1">
                                  <div class="form-check m-0">
                                    <input type="checkbox" name="is_bold" class="form-check-input quotation_item-is_bold-<?= $item->id ?>" id="pref-bold-<?= $item->id ?>" value="1">
                                    <label class="form-check-label" for="pref-bold-<?= $item->id ?>">Bold</label>
                                  </div>
                                </div>
                                <div class="col-xs-12 col-md-1">
                                  <div class="form-check m-0">
                                    <input type="checkbox" name="is_italic" class="form-check-input quotation_item-is_italic-<?= $item->id ?>" id="pref-italic-<?= $item->id ?>" value="1">
                                    <label class="form-check-label" for="pref-italic-<?= $item->id ?>">Italic</label>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-xs-12 col-md-12">
                          <div class="row">
                            <div class="col-xs-12 col-md-8">
                              <div class="form-group mb-3">
                                <label>Works Description</label>
                                <div class="input-group">
                                  <!-- Manual -->
                                  <input type="text" name="description_manual" class="form-control quotation_item-description_manual quotation_item-description_manual-<?= $item->id ?>" data-id="<?= $item->id ?>" placeholder="Works Description" style="border-top-right-radius: 0.35rem; border-bottom-right-radius: 0.35rem;" readonly />
                                  <!-- Auto -->
                                  <select name="description_auto" class="form-control quotation_item-description_auto quotation_item-description_auto-<?= $item->id ?> select2" data-id="<?= $item->id ?>" style="border-top-right-radius: 0.35rem; border-bottom-right-radius: 0.35rem; display: none;">
                                    <?= $barang_list ?>
                                  </select>
                                  <!-- Hidden temp -->
                                  <input type="hidden" name="description" class="quotation_item-description quotation_item-description-<?= $item->id ?>" readonly />
                                </div>
                                <!-- Params -->
                                <div class="form-control wrapper-params wrapper-params-<?= $item->id ?>" style="display: none;"></div>
                                <!-- END ## Params -->
                              </div>
                            </div>
                            <div class="col-xs-12 col-md-4">
                              <div class="form-group mb-3">
                                <label>Note</label>
                                <input type="text" name="note" class="form-control quotation_item-note quotation_item-note-<?= $item->id ?>" placeholder="Note" />
                                <i class="form-group__bar"></i>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-xs-12 col-md-2">
                            <div class="form-group mb-3">
                              <label>Quantity</label>
                              <input type="number" name="quantity" class="form-control quotation_item-quantity quotation_item-quantity-<?= $item->id ?>" data-id="<?= $item->id ?>" placeholder="Quantity" min="1" readonly />
                              <i class="form-group__bar"></i>
                            </div>
                          </div>
                          <div class="col-xs-12 col-md-2">
                            <div class="form-group mb-3">
                              <label>Unit</label>
                              <input type="text" name="unit" class="form-control quotation_item-unit quotation_item-unit-<?= $item->id ?>" placeholder="Unit" readonly />
                              <i class="form-group__bar"></i>
                            </div>
                          </div>
                          <div class="col-xs-12 col-md-2">
                            <div class="form-group mb-3">
                              <label>Unit Price</label>
                              <input type="text" name="unit_price" class="form-control mask-money quotation_item-unit_price quotation_item-unit_price-<?= $item->id ?>" data-id="<?= $item->id ?>" placeholder="Unit Price" />
                              <i class="form-group__bar"></i>
                            </div>
                          </div>
                          <div class="col-xs-12 col-md-2">
                            <div class="form-group mb-3">
                              <label>Total</label>
                              <input type="text" name="total_price" class="form-control mask-money quotation_item-total_price quotation_item-total_price-<?= $item->id ?>" data-id="<?= $item->id ?>" placeholder="Total" readonly />
                              <i class="form-group__bar"></i>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-xs-12 col-md-12">
                        <a href="javascript:;" class="btn btn-secondary item-form-input-cancel">Cancel</a>
                        <a href="javascript:;" class="btn btn-success item-form-input-save" data-id="<?= $item->id ?>"><i class="zmdi zmdi-save"></i> Save</a>
                      </div>
                    </div>
                  </form>
                </div>
              </td>
            </tr>
            <!-- END ## Item inline form -->
          </div>
        <?php endforeach ?>
      <?php endif ?>
      </td>
      </tr>
</div>
<!-- END ## Header inline form -->
</tbody>
<tfoot>
  <tr>
    <td colspan="8" class="p-0" style="height: 5px;"></td>
  </tr>
  <tr>
    <td colspan="3">&nbsp;</td>
    <th colspan="3" class="text-center">
      T o t a l
    </th>
    <th class="text-right"><?= number_format($subTotal) ?></th>
    <th>&nbsp;</th>
  </tr>
</tfoot>
</table>
</div>