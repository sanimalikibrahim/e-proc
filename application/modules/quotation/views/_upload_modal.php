<div class="modal fade" id="modal-form-upload" data-backdrop="static" data-keyboard="false" tabindex="-1">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title pull-left">Upload PO Dari Customer</h5>
      </div>
      <div class="spinner">
        <div class="lds-hourglass"></div>
      </div>
      <div class="modal-body">
        <div class="alert alert-info">
          <i class="zmdi zmdi-info"></i>
          Anda hanya dapat mengunggah berkas satu kali.
        </div>

        <form id="form-quotation-upload" enctype="multipart/form-data">
          <!-- Temporary field -->
          <input type="hidden" name="quotation_id" class="quotation_id" readonly />

          <div class="form-group">
            <label required>File</label>
            <div class="upload-inline">
              <div class="upload-button">
                <input type="file" name="file" class="upload-pure-button quotation-file" />
              </div>
              <div class="upload-preview">
                No file chosen
              </div>
            </div>
          </div>

          <small class="form-text text-muted">
            Fields with red stars (<label required></label>) are required. <br />
          </small>

        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success btn--icon-text quotation-action-save-upload">
          <i class="zmdi zmdi-save"></i> Save
        </button>
        <button type="button" class="btn btn-light btn--icon-text quotation-action-cancel-upload" data-dismiss="modal">
          Cancel
        </button>
      </div>
    </div>
  </div>
</div>