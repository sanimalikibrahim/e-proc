<?php require_once('main.css.php') ?>

<div class="preview-po" style="border: 0; padding: 0;">
  <div class="preview-header">
    <!-- Kop Surat -->
    <?php include_once(APPPATH . 'modules/_partial/kop_surat.php') ?>
    <!-- END ## Kop Surat -->
  </div>
  <div class="preview-body">
    <table style="width: 100%; margin-bottom: 15px;">
      <tr>
        <td style="font-size: 12px; width: 80px;">Tanggal</td>
        <td style="font-size: 12px; width: 10px;">:</td>
        <td style="font-size: 12px;"><?= isset($quotation->tanggal) ? $controller->localizeDate($quotation->tanggal) : null ?></td>
      </tr>
      <tr>
        <td style="font-size: 12px; width: 80px;">No</td>
        <td c width: 10px;">:</td>
        <td style="font-size: 12px;"><?= isset($quotation->nomor) ? $quotation->nomor : null ?></td>
      </tr>
      <tr>
        <td colspan="3" class="p-0" style="height: 10px;"></td>
      </tr>
      <tr>
        <td colspan="3" style="font-size: 12px;">
          Kepada Yth, <br>
          <b><?= isset($quotation->customer_name) ? $quotation->customer_name : null ?></b> <br>
          <?= isset($quotation->customer_address) ? $quotation->customer_address : null ?> <br>
          <b><u><?= isset($quotation->customer_attn) ? $quotation->customer_attn : null ?></u></b>
        </td>
      </tr>
      <tr>
        <td colspan="3" class="p-0" style="height: 10px;"></td>
      </tr>
      <tr>
        <td colspan="3" style="font-size: 12px;">
          Dengar hormat, <br>
          Berdasarkan permintaan Bapak/Ibu untuk <?= isset($quotation->project_name) ? $quotation->project_name : null ?>,
          maka kami ajukan penawaran dengan rincian sebagai berikut :
        </td>
      </tr>
    </table>

    <table class="table-order-item" style="border-collapse: collapse;">
      <thead style="text-align: center;">
        <tr>
          <th width="60">No</th>
          <th colspan="2">Works Description</th>
          <th colspan="2">Vol</th>
          <th colspan="2">Price (Rp)</th>
        </tr>
      </thead>
      <tbody>
        <?php
        $no = 1;
        $subTotal = 0;
        ?>
        <?php if (isset($quotation_item) && count($quotation_item) > 0) : ?>
          <?php foreach ($quotation_item as $key => $item) : ?>
            <?php
            $isPreferences = '';
            $isPreferences .= ($item->is_bold == 1) ? 'font-weight: 600;' : '';
            $isPreferences .= ($item->is_italic == 1) ? 'font-style: italic;' : '';
            $subTotal = (!is_null($item->total_price) && !empty($item->total_price)) ? (float) $item->total_price + $subTotal : 0 + $subTotal;
            ?>
            <tr class="item-row-wrapper item-row-wrapper-<?= $item->id ?>" style="<?= $isPreferences ?>">
              <td align="center" style="height: 30.48px;"><?= $item->nomor ?></td>
              <td style="border-right-color: transparent;"><?= $item->description ?></td>
              <td><?= $item->note ?></td>
              <td class="text-center" width="70"><?= (!is_null($item->quantity)) ? number_format($item->quantity, 0) : null ?></td>
              <td class="text-center" width="70"><?= $item->unit ?></td>
              <td class="text-right" width="130"><?= (!is_null($item->unit_price)) ? number_format($item->unit_price, 0) : null ?></td>
              <td class="text-right" width="130"><?= (!is_null($item->total_price)) ? number_format($item->total_price, 0) : null ?></td>
            </tr>
          <?php endforeach; ?>
        <?php else : ?>
          <tr>
            <td colspan="8" style="padding: 15px;">
              No data available in table
            </td>
          </tr>
        <?php endif; ?>
      </tbody>
      <tfoot>
        <tr>
          <td colspan="8" class="p-0" style="height: 5px;"></td>
        </tr>
        <tr>
          <td colspan="3">&nbsp;</td>
          <th colspan="3" class="text-center">
            T o t a l
          </th>
          <th class="text-right"><?= number_format($subTotal) ?></th>
        </tr>
      </tfoot>
    </table>
    <div style="margin-bottom: 15px;"></div>

    <?php if (!is_null($quotation->note) && !empty($quotation->note)) : ?>
      <span style="font-size: 12px;">Notes :</span>
      <div style="font-size: 12px;"><?= (isset($quotation->note)) ? str_replace("\r\n", '<br/>', htmlspecialchars_decode($quotation->note)) : null ?></div>
    <?php endif ?>

    <div style="margin-bottom: 15px;"></div>
    <div style="font-size: 12px;">
      Demikian penawaran ini kami sampaikan dengan harapan dapat terjalinnya kerjasama yang baik, atas perhatiannya kami ucapkan terimakasih.
    </div>
    <div style="margin-bottom: 20px;"></div>

    <span style="font-size: 12px;">Propose by,</span> <br />
    <span style="font-size: 12px;"><u>PT. ARYA JAYA</u></span>
  </div>
</div>