<script type="text/javascript">
  $(document).ready(function() {
    var _key = "";
    var _section = "po";
    var _table_list = "table-list";
    var _table_item_list = "table-po_item";
    var _form_pr = "form-po";
    var _prId = "<?= $po_id ?>";
    var _modal_upload = "modal-form-upload";
    var _form_upload = "form-po-upload";
    var _modal_approval = "modal-form-approval";
    var _form_approval = "form-po-approval";
    var _modal_view = "modal-po-view";
    var _modal_view_riwayat = "modal-po-view-riwayat";
    var _module = "<?= $module ?>";
    var _modal_source = "";
    var _listFilter = "<?= (!is_null($this->input->get('ref'))) ? $this->input->get('ref') : null ?>";
    var _role = "<?= $this->session->userdata('user')['role'] ?>";

    // Initialize DataTables : List
    if ($("#" + _table_list)[0]) {
      var table_pr = $("#" + _table_list).DataTable({
        processing: true,
        serverSide: true,
        ajax: {
          url: "<?php echo base_url('po/ajax_get_list/?src=') ?>" + _module,
          type: "get"
        },
        columns: [{
            data: null,
            render: function(data, type, row, meta) {
              return meta.row + meta.settings._iDisplayStart + 1;
            }
          },
          {
            data: "nomor",
            render: function(data, type, row, meta) {
              var dom = "";
              var prNomor = (row.pr_nomor != null && row.pr_nomor != "") ? row.pr_nomor : "-";

              dom += `<div><span class="badge badge-secondary" style="width: 30px; height: 15px;">PR</span> ${prNomor}</div>`;
              dom += `<div><span class="badge badge-secondary" style="width: 30px; height: 15px;">PO</span> ${data}</div>`;

              return dom;
            }
          },
          {
            data: "tanggal"
          },
          {
            data: "total_price",
            render: function(data, type, row, meta) {
              if (data !== null) {
                return meta.settings.fnFormatNumber(data.replace(".0000", ""));
              } else {
                return 0;
              };
            }
          },
          {
            data: "status",
            render: function(data, type, row, meta) {
              var statusName = getStatus(data);
              var badgeType = "info";
              var revisi = (row.revisi_nomor != null && row.revisi_nomor != "") ? " (Rev. " + row.revisi_nomor + ")" : "";

              if (row.is_editable == 1) {
                statusName = "SKB Sudah Dibuat";
              };

              return '<span class="badge badge-' + badgeType + '">' + statusName + revisi + '</span>';
            }
          },
          {
            data: "created_at"
          },
          {
            data: null,
            className: "text-center",
            render: function(data, type, row, meta) {
              var htmlDom = "-";

              htmlDom = '<div class="action">';
              htmlDom += '<a href="javascript:;" class="action-view btn btn-light btn-sm mr-1" data-toggle="modal" data-target="#' + _modal_view + '"><i class="zmdi zmdi-eye"></i></a>&nbsp;';

              if (row.status == "0") {
                // Draft
                htmlDom += '<a href="<?= base_url('po/wizard/') ?>' + row.id + '/1" class="action-edit btn btn-dark btn-sm mr-1"><i class="zmdi zmdi-edit"></i></a>';
              };

              if (_role.toLowerCase() === 'procurement' && row.status == "2" && row.is_editable == 0) {
                htmlDom += `<a href="<?= base_url('po/generate_skb/') ?>${row.id}" class="action-generate_skb btn btn-success btn-sm">Buat SKB</a>`;
              };

              if (_role.toLowerCase() === 'procurement' && row.status == "3" && row.is_editable == 0) {
                htmlDom += `<a href="<?= base_url('po/revisi/') ?>${row.id}" class="action-revisi btn btn-success btn-sm">Revisi</a>`;
              };

              if (_role.toLowerCase() === 'finance' && row.status == "1") {
                // Approval
                htmlDom += `
                  <div class="dropdown show">
                    <a class="btn btn-success dropdown-toggle" href="#" role="button" id="actionPo" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Approval</a>
                    <div class="dropdown-menu" aria-labelledby="actionPo">
                      <a href="javascript:;" data-url="<?= base_url('po/ajax_set_status/?po_id=') ?>${row.id}&status=2" data-status="Approve" class="action-approval dropdown-item">Approve</a>
                      <a href="javascript:;" data-url="<?= base_url('po/ajax_set_status/?po_id=') ?>${row.id}&status=3" data-status="Reject" class="action-approval dropdown-item">Reject</a>
                    </div>
                  </div>
                `;
              };

              htmlDom += '</div>';

              return htmlDom;
            }
          }
        ],
        autoWidth: !1,
        responsive: {
          details: {
            // display: $.fn.dataTable.Responsive.display.modal(),
            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
              tableClass: "table dt-details"
            }),
            type: "inline",
            target: 'tr',
          }
        },
        columnDefs: [{
          className: 'desktop',
          targets: [0, 1, 2, 3, 4, 5, 6]
        }, {
          className: 'tablet',
          targets: [0, 1, 2]
        }, {
          className: 'mobile',
          targets: [0, 1]
        }, {
          responsivePriority: 1,
          targets: 0
        }, {
          responsivePriority: 2,
          targets: -1
        }],
        pageLength: 15,
        language: {
          searchPlaceholder: "Search...",
          sProcessing: '<div style="text-align: center;"><div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div></div>'
        },
        sDom: '<"dataTables_ct"><"dataTables__top"fb>rt<"dataTables__bottom"ip><"clear">',
        buttons: [{
          extend: "excelHtml5",
          title: "Export Result"
        }, {
          extend: "print",
          title: "Export Result"
        }],
        search: {
          search: _listFilter
        },
        initComplete: function(a, b) {
          $(this).closest(".dataTables_wrapper").find(".dataTables__top").prepend(
            '<div class="dataTables_buttons hidden-sm-down actions">' +
            '<span class="actions__item zmdi zmdi-refresh" data-table-action="reload" title="Reload" />' +
            '</div>'
          );
        },
      });

      $(".dataTables_filter input[type=search]").focus(function() {
        $(this).closest(".dataTables_filter").addClass("dataTables_filter--toggled")
      });

      $(".dataTables_filter input[type=search]").blur(function() {
        $(this).closest(".dataTables_filter").removeClass("dataTables_filter--toggled")
      });

      $("body").on("click", "[data-table-action]", function(a) {
        a.preventDefault();
        var b = $(this).data("table-action");
        if ("reload" === b) {
          $("#" + _table_list).DataTable().ajax.reload(null, false);
        };
      });
    };

    // po ITEM
    $(document).on("click", "a.item-button-row-add", function(e) {
      e.preventDefault();
      var ref = $(this).attr("data-id");
      var addButton = $(".item-button-row");
      var form = $(".item-form-row-" + ref);
      var formIcon = $(".item-form-row-icon-" + ref);
      var formHeader = $(".header-form-row");
      var formInput = $("#item-form-input-" + ref);
      var otherInput = $(".item-other-input-" + ref);
      var rowWrapper = $(".item-row-wrapper-" + ref);
      var wrapperParam = $(".wrapper-params-" + ref);
      var checkboxAsBarang = $(".description-as_barang-" + ref);

      formIcon.html('N');
      addButton.fadeOut(100);
      form.fadeIn(100);
      formHeader.fadeOut(100);
      formInput.trigger("reset");
      otherInput.show();
      rowWrapper.addClass("bg-green text-white");
      wrapperParam.hide();
      checkboxAsBarang.removeAttr("disabled");
      $(".po_item-description_manual-" + ref).show();
      $(".po_item-description_auto-" + ref).select2().next().hide();

      _key = "";

      $('.mask-decimal').mask('#,##0,00', {
        reverse: true
      });
      $('.mask-money').mask('#,##0', {
        reverse: true
      });
    });

    $(document).on("click", "a.item-form-input-cancel", function(e) {
      e.preventDefault();
      var addButton = $(".item-button-row");
      var form = $(".item-form-row");
      var formHeader = $(".header-form-row");
      var rowWrapper = $(".item-row-wrapper");

      addButton.fadeIn(100);
      form.fadeOut(100);
      formHeader.fadeIn(100);
      rowWrapper.removeClass("bg-green text-white");
    });

    $(document).on("click", "a.item-form-input-save", function(e) {
      e.preventDefault();
      var ref = $(this).attr("data-id");
      var addButton = $(".item-button-row");
      var form = $(".item-form-row-" + ref);
      var formHeader = $(".header-form-row");
      var formData = $("#item-form-input-" + ref);
      var rowWrapper = $(".item-row-wrapper-" + ref);

      adjustSpinner(ref);
      try {
        $.ajax({
          type: "post",
          url: "<?php echo base_url('po/ajax_save_item/') ?>" + _key,
          data: formData.serialize(),
          success: function(response) {
            var response = JSON.parse(response);

            if (response.status === true) {
              fetchDataPrItem().then(() => {
                addButton.fadeIn(100);
                form.fadeOut(100);
                formHeader.fadeIn(100);
                rowWrapper.removeClass("bg-green text-white");
              });

              notify(response.data, "success");
            } else {
              notify(response.data, "danger");
            };
          }
        });
      } catch (error) {
        notify("Failed to save your data.", "danger");
      };
    });

    $(document).on("click", "a.item-button-row-edit", function(e) {
      e.preventDefault();
      var ref = $(this).attr("data-id");
      var po_item_parent_id = $(this).attr("data-po_item_parent_id");
      var nomor = $(this).attr("data-nomor");
      var description = $(this).attr("data-description");
      var note = $(this).attr("data-note");
      var quantity = $(this).attr("data-quantity");
      var unit = $(this).attr("data-unit");
      var unit_price = $(this).attr("data-unit_price");
      unit_price = (unit_price == 0) ? null : unit_price;
      var total_price = $(this).attr("data-total_price");
      total_price = (total_price == 0) ? null : total_price;
      var is_bold = $(this).attr("data-is_bold");
      var is_italic = $(this).attr("data-is_italic");

      var addButton = $(".item-button-row");
      var form = $(".item-form-row-" + ref);
      var formIcon = $(".item-form-row-icon-" + ref);
      var formHeader = $(".header-form-row");
      var formInput = "#item-form-input-" + ref;
      var rowWrapper = $(".item-row-wrapper-" + ref);
      var wrapperParam = $(".wrapper-params-" + ref);
      var checkboxAsBarang = $(".description-as_barang-" + ref);

      formIcon.html('E');
      addButton.fadeOut(100);
      form.fadeIn(100);
      formHeader.fadeOut(100);
      rowWrapper.addClass("bg-green text-white");

      wrapperParam.hide();
      $(".po_item-description_manual-" + ref).show();
      $(".po_item-description_auto-" + ref).select2().next().hide();
      checkboxAsBarang.attr("disabled", true);

      // Fill input value
      _key = ref;
      $(formInput).trigger("reset");
      $(formInput + " .po_item-po_item_parent_id-" + ref).val(po_item_parent_id).trigger("input");
      $(formInput + " .po_item-nomor-" + ref).val(nomor).trigger("input");
      $(formInput + " .po_item-description-" + ref).val(description).trigger("input");
      $(formInput + " .po_item-description_manual-" + ref).val(description).trigger("input");
      $(formInput + " .po_item-note-" + ref).val(note).trigger("input");
      $(formInput + " .po_item-quantity-" + ref).val(quantity).trigger("input");
      $(formInput + " .po_item-unit-" + ref).val(unit).trigger("input");
      $(formInput + " .po_item-unit_price-" + ref).val(unit_price).trigger("input");
      $(formInput + " .po_item-total_price-" + ref).val(total_price).trigger("input");

      if (is_bold == 1) {
        $(formInput + " .po_item-is_bold-" + ref).attr("checked", true);
      } else {
        $(formInput + " .po_item-is_bold-" + ref).removeAttr("checked");
      };
      if (is_italic == 1) {
        $(formInput + " .po_item-is_italic-" + ref).attr("checked", true);
      } else {
        $(formInput + " .po_item-is_italic-" + ref).removeAttr("checked");
      };

      $('.mask-decimal').mask('#,##0.00', {
        reverse: true
      });
      $('.mask-money').mask('#,##0', {
        reverse: true
      });
    });

    $(document).on("click", "a.item-button-row-delete", function(e) {
      e.preventDefault();
      var ref = $(this).attr("data-id");

      swal({
        title: "Are you sure to delete?",
        text: "Once deleted, you will not be able to recover this data!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false
      }).then((result) => {
        if (result.value) {
          $.ajax({
            type: "delete",
            url: "<?php echo base_url('po/ajax_delete_item/') ?>" + ref,
            dataType: "json",
            success: function(response) {
              if (response.status) {
                fetchDataPrItem();
                notify(response.data, "success");
              } else {
                notify(response.data, "danger");
              };
            }
          });
        };
      });
    });

    $(document).on("click keyup", ".po_item-quantity", function(e) {
      e.preventDefault();
      var ref = $(this).attr("data-id");

      calculateUnitPrice(ref);
    });

    $(document).on("keyup", ".po_item-unit_price", function(e) {
      e.preventDefault();
      var ref = $(this).attr("data-id");

      calculateUnitPrice(ref);
    });

    $(document).on("click", "a.header-button-row-add", function(e) {
      e.preventDefault();
      var itemButton = $(".item-button-row");
      var addButton = $(".header-button-row-add");
      var addButtonEmpty = $(".header-button-row-add-empty");
      var formIcon = $(".header-form-row-icon");
      var formInput = $("#header-form-input");
      var wrapperParam = $(".wrapper-params");

      formIcon.html('N');
      formIcon.fadeIn(100);
      itemButton.fadeOut(100);
      addButton.hide();
      addButtonEmpty.hide();
      formInput.fadeIn(100);
      formInput.trigger("reset");
      wrapperParam.hide();
      $(".po_item-description_manual").show();
      $(".po_item-description_auto").select2().next().hide();

      _key = "";
    });

    $(document).on("click", "a.header-form-input-cancel", function(e) {
      e.preventDefault();
      var itemButton = $(".item-button-row");
      var addButton = $(".header-button-row-add");
      var addButtonEmpty = $(".header-button-row-add-empty");
      var formIcon = $(".header-form-row-icon");
      var formInput = $("#header-form-input");

      itemButton.fadeIn(100);
      addButton.fadeIn(100);
      addButtonEmpty.fadeIn(100);
      formIcon.fadeOut(100);
      formInput.fadeOut(100);
    });

    $(document).on("click", "a.header-form-input-save", function(e) {
      e.preventDefault();
      var itemButton = $(".item-button-row");
      var addButton = $(".header-button-row-add");
      var addButtonEmpty = $(".header-button-row-add-empty");
      var formInput = $("#header-form-input");

      adjustSpinnerHeader("header");
      try {
        $.ajax({
          type: "post",
          url: "<?php echo base_url('po/ajax_save_item/') ?>" + _key,
          data: formInput.serialize(),
          success: function(response) {
            var response = JSON.parse(response);

            if (response.status === true) {
              fetchDataPrItem().then(() => {
                itemButton.fadeIn(100);
                addButton.fadeIn(100);
                addButtonEmpty.fadeIn(100);
                formInput.fadeOut(100);
              });

              notify(response.data, "success");
            } else {
              notify(response.data, "danger");
            };
          }
        });
      } catch (error) {
        notify("Failed to save your data.", "danger");
      };
    });

    $(document).on("click", "a.header-button-row-add-empty", function(e) {
      e.preventDefault();
      try {
        var prId = $(this).attr("data-po_id");

        $.ajax({
          type: "post",
          url: "<?php echo base_url('po/ajax_add_empty_row/') ?>" + prId,
          success: function(response) {
            var response = JSON.parse(response);

            if (response.status === true) {
              fetchDataPrItem();
              notify(response.data, "success");
            } else {
              notify(response.data, "danger");
            };
          }
        });
      } catch (error) {
        notify("Failed to save your data.", "danger");
      };
    });

    // Handle input method
    $(document).on("click", ".description-as_barang", function() {
      var isAuto = $(this).is(":checked");
      var ref = $(this).attr("data-id");
      var manual = $(".po_item-description_manual-" + ref);
      var auto = $(".po_item-description_auto-" + ref);
      var hiddeName = $(".po_item-description-" + ref);
      var wrapperParam = $(".wrapper-params-" + ref);

      // Clear input before
      manual.val("").trigger("input");
      auto.val($(".po_item-description_auto-" + ref + " option:first").val()).trigger("change");
      hiddeName.val("").trigger("input");
      wrapperParam.hide();

      if (isAuto === true) {
        manual.hide();
        auto.select2().next().show();
      } else {
        manual.show();
        auto.select2().next().hide();
      };
    });

    // Handle input method: Manual
    $(document).on("keyup", ".po_item-description_manual", function() {
      var value = $(this).val();
      var ref = $(this).attr("data-id");
      var hiddeName = $(".po_item-description-" + ref);

      hiddeName.val(value);
    });

    // Handle input method: Auto
    $(document).on("select2:select", ".po_item-description_auto", function(e) {
      var data = e.params.data;
      var ref = $(this).attr("data-id");
      var hiddeName = $(".po_item-description-" + ref);
      var wrapperParam = $(".wrapper-params-" + ref);

      hiddeName.val(data.text);

      // Retrive parameter
      if (data.id != "" && data.id != null) {
        adjustSpinnerHeader("header");
        adjustSpinner(ref);

        $.ajax({
          type: "get",
          url: "<?= base_url('po/ajax_get_barang_param/') ?>",
          data: {
            barang_id: data.id,
          },
          dataType: "json",
          success: function(response) {
            var component = "";

            $(".po_item-unit-" + ref).val(response.unit).trigger("keyup");
            $(".po_item-unit_price-" + ref).val(response.unit_price).trigger("keyup");

            // if (response.parameter.length > 0) {
            //   response.parameter.map((item, index) => {
            //     component += `
            //       <div class="mb-2">
            //         ${item.label} : ${item.value}
            //         <input type="hidden" name="description_param[${index}][label]" value="${item.label}" readonly />
            //         <input type="hidden" name="description_param[${index}][value]" value="${item.value}" readonly />
            //       </div>
            //     `;
            //   });
            //   wrapperParam.show().html(component);
            // } else {
            //   wrapperParam.hide();
            // };

            $('.mask-money').unmask().mask('#,##0', {
              reverse: true
            });
          }
        });
      };
    });

    async function fetchDataPrItem() {
      var prId = "<?= $po_id ?>";

      await $.ajax({
        type: "get",
        url: "<?php echo base_url('po/ajax_get_po_item_table/') ?>" + prId,
        success: function(response) {
          $("#table-po-item").html(response);
        }
      });
    };

    function calculateUnitPrice(ref) {
      var quantity = $(".po_item-quantity-" + ref).val().replace(/[^\d.]/g, "");
      quantity = (quantity != "") ? quantity : 0;

      var unitPrice = $(".po_item-unit_price-" + ref).val().replace(/[^\d]/g, "");
      unitPrice = (unitPrice != "") ? unitPrice : 0;

      var total = $(".po_item-total_price-" + ref);
      var totalTemp = quantity * unitPrice;

      if (quantity == "" && unitPrice == "") {
        totalTemp = "";
      };

      total.val(totalTemp).trigger("input");
    };

    function adjustSpinner(ref) {
      $(".spinner-" + ref).css("width", $(".item-form-input-wrapper-" + ref).width() + 27);
      $(".spinner-" + ref).css("height", $(".item-form-input-wrapper-" + ref).height() + 27);
      $(".spinner-" + ref).css("margin-top", "-13px");
      $(".spinner-" + ref).css("margin-left", "-13px");
    };

    function adjustSpinnerHeader() {
      $(".spinner-header").css("width", $(".header-form-input-wrapper").width());
      $(".spinner-header").css("height", $(".header-form-input-wrapper").height());
      $(".spinner-header").css("margin-top", "-13px");
      $(".spinner-header").css("margin-left", "-13px");
    };
    // END ## po ITEM

    // Handle data submit
    $("#" + _form_pr + " button.po-action-save").on("click", function(e) {
      e.preventDefault();

      $.ajax({
        type: "post",
        url: "<?php echo base_url('po/ajax_save/') ?>" + _prId,
        data: $("#" + _form_pr).serialize(),
        success: function(response) {
          var response = JSON.parse(response);
          if (response.status === true) {
            notify(response.data, "success");
            location.href = "<?= base_url('po/wizard/') ?>" + response.data_id + "/2";
          } else {
            notify(response.data, "danger");
          };
        }
      });
    });

    // Handle data delete
    $("#" + _table_list).on("click", "a.action-delete", function(e) {
      e.preventDefault();
      var temp = table_pr.row($(this).closest('tr')).data();

      swal({
        title: "Are you sure to delete?",
        text: "Once deleted, you will not be able to recover this data!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false
      }).then((result) => {
        if (result.value) {
          $.ajax({
            type: "delete",
            url: "<?php echo base_url('po/ajax_delete/') ?>" + temp.id,
            dataType: "json",
            success: function(response) {
              if (response.status) {
                $("#" + _table_list).DataTable().ajax.reload(null, false);
                notify(response.data, "success");
              } else {
                notify(response.data, "danger");
              };
            }
          });
        };
      });
    });

    // Handle data approval
    $("#" + _table_list).on("click", "a.action-approval", function(e) {
      e.preventDefault();
      var url = $(this).attr("data-url");
      var status = $(this).attr("data-status");

      swal({
        title: "Are you sure to " + status + "?",
        text: "You will not be able to recover this data!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false
      }).then((result) => {
        if (result.value) {
          $.ajax({
            type: "get",
            url: url,
            dataType: "json",
            success: function(response) {
              if (response.status) {
                $("#" + _table_list).DataTable().ajax.reload(null, false);
                notify(response.data, "success");
              } else {
                notify(response.data, "danger");
              };
            }
          });
        };
      });
    });

    // Handle view
    $("#" + _table_list).on("click", "a.action-view", function(e) {
      e.preventDefault();
      var temp = table_pr.row($(this).closest('tr')).data();

      $("#" + _modal_view + " .approval-preview").html("Please wait...");

      $.ajax({
        type: "get",
        url: "<?= base_url('po/ajax_get_preview/') ?>" + temp.id,
        success: function(response) {
          $(".approval-preview").html(response);
          $(".modal-po-view-title").html("Purchase Order : " + getStatus(temp.status));
        }
      });

      _modal_source = "view";
    });

    // Handle generate SKB
    $("#" + _table_list).on("click", "a.action-generate_skb", function(e) {
      e.preventDefault();
      var url = $(this).attr("href");

      swal({
        title: "Anda akan membuat SKB, lanjutkan?",
        text: "Sumber data diambil berdasarkan Purchase Request.",
        type: "question",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false
      }).then((result) => {
        if (result.value) {
          location.href = url;
        };
      });
    });

    // Handle revisi
    $("#" + _table_list).on("click", "a.action-revisi", function(e) {
      e.preventDefault();
      var url = $(this).attr("href");

      swal({
        title: "Anda akan melakukan revisi, lanjutkan?",
        text: "Data akan dikembalikan pada status draft.",
        type: "question",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false
      }).then((result) => {
        if (result.value) {
          location.href = url;
        };
      });
    });

    // Handle submit / publish
    $(document).on("click", "button.po-action-sent", function(e) {
      e.preventDefault();
      var ref = $(this).attr("data-id");

      swal({
        title: "Are you sure to submit?",
        text: "Once submited, you will not be able to recover this data!",
        type: "question",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false
      }).then((result) => {
        if (result.value) {
          $.ajax({
            type: "get",
            url: "<?php echo base_url('po/ajax_set_status/') ?>",
            data: {
              po_id: ref,
              status: 1,
            },
            dataType: "json",
            success: function(response) {
              if (response.status) {
                notify(response.data, "success");

                setTimeout(function() {
                  location.href = "<?= base_url('po') ?>";
                }, 2000);
              } else {
                notify(response.data, "danger");
              };
            }
          });
        };
      });
    });

    // Handle po print
    $(document).on("click", "button.action-po-print", function(e) {
      e.preventDefault();
      $(".po-print-area").printThis();
    });
  });

  function getStatus(status) {
    switch (status.toString()) {
      case '0':
        return 'Draft';
        break;
      case '1':
        return 'Menunggu Persetujuan Finance';
        break;
      case '2':
        return 'Disetujui';
        break;
      case '3':
        return 'Ditolak';
        break;
      default:
        return 'Undefined';
        break;
    };
  };
</script>