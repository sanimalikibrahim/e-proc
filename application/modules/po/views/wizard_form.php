<?php require_once('main.css.php') ?>
<?php $revisiNomor = null ?>
<?php $revisiNomorLabel = null ?>

<section id="suratjalan">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <span class="badge badge-info" style="position: absolute; margin-top: 2px;">
                        <?php if (isset($is_revisi) && $is_revisi == true) : ?>
                            <?php $revisiNomor = (!is_null($po->revisi_nomor)) ? (int) $po->revisi_nomor + 1 : 1; ?>
                            REV
                        <?php else : ?>
                            <?php $revisiNomorLabel = (isset($po->revisi_nomor) && !is_null($po->revisi_nomor)) ? ' : Rev. ' . $po->revisi_nomor : null; ?>
                            <?= (isset($po->id)) ? 'EDIT' : 'NEW' ?>
                        <?php endif ?>
                    </span>
                    <div style="margin-left: 50px;">
                        <h4 class="card-title"><?php echo (isset($card_title)) ? $card_title . $revisiNomorLabel : '' ?></h4>
                        <h6 class="card-subtitle"><?php echo (isset($card_subTitle)) ? $card_subTitle : '' ?></h6>
                    </div>
                    <div class="clear-card"></div>
                </div>
            </div>
            <div class="clear-card"></div>

            <form id="form-po" enctype="multipart/form-data" autocomplete="off">
                <input type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" value="<?= $this->security->get_csrf_hash(); ?>" />

                <!-- Temporary hidden field -->
                <?php if (isset($is_revisi) && $is_revisi == true) : ?>
                    <input type="hidden" name="is_revisi" value="true" readonly />
                    <input type="hidden" name="revisi_nomor" value="<?= $revisiNomor ?>" readonly />

                    <div class="alert alert-secondary mb-4">
                        <i class="zmdi zmdi-info"></i>
                        <b>Revisi <?= $revisiNomor ?></b>, silahkan masukan nomor baru! (Prev: <?= $po->nomor ?>)
                    </div>
                <?php endif ?>

                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label required>Nomor</label>
                            <input type="text" name="nomor" class="form-control po-nomor" placeholder="Nomor" value="<?= (isset($po->nomor)) ? $po->nomor : $generate_nomor ?>" readonly required />
                            <i class="form-group__bar"></i>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label required>Date</label>
                            <input type="text" name="tanggal" class="form-control po-tanggal flatpickr-date bg-white" placeholder="Date" value="<?= (isset($po->tanggal)) ? $po->tanggal : null ?>" required />
                            <i class=" form-group__bar"></i>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label>PR Nomor</label>
                            <input type="text" name="pr_nomor" class="form-control po-pr_nomor" placeholder="PR Nomor" value="<?= (isset($po->pr_nomor)) ? $po->pr_nomor : null ?>" />
                            <i class="form-group__bar"></i>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label>Ref. Nomor</label>
                            <input type="text" name="ref_nomor" class="form-control po-ref_nomor" placeholder="Ref. Nomor" value="<?= (isset($po->ref_nomor)) ? $po->ref_nomor : null ?>" />
                            <i class="form-group__bar"></i>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label>Type</label>
                            <div class="select">
                                <select name="tipe" class="form-control select2 po-tipe" data-placeholder="Select &#8595;">
                                    <?= $tipe_list ?>
                                </select>
                                <i class="form-group__bar"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label required>Supplier</label>
                            <div class="select">
                                <select name="supplier_id" class="form-control select2 po-supplier_id" data-placeholder="Select &#8595;" required>
                                    <?= $supplier_list ?>
                                </select>
                                <i class="form-group__bar"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label>Note</label>
                    <textarea name="note" class="form-control textarea-autosize text-counter po-note" rows="5" data-max-length="1000" placeholder="Note" style="overflow: hidden; overflow-wrap: break-word; height: 31px;"><?= (isset($po->note)) ? $po->note : null ?></textarea>
                    <i class="form-group__bar"></i>
                </div>

                <small class="form-text text-muted">
                    Fields with red stars (<label required></label>) are required.
                </small>

                <div class="row">
                    <div class="col">
                        <div class="buttons-container">
                            <div class="row">
                                <div class="col">
                                    <a href="<?= base_url('po') ?>" class="btn btn-dark">Cancel</a>
                                </div>
                                <div class="col text-right">
                                    <button class="btn btn--raised btn-primary btn--icon-text btn-custom po-action-save spinner-action-button">
                                        Next
                                        <i class="zmdi zmdi-long-arrow-right"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>
</section>