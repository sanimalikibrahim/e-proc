<?php require_once('main.css.php') ?>
<?php $revisiNomor = null ?>
<?php $revisiNomorLabel = null ?>

<section id="suratjalan">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <span class="badge badge-info" style="position: absolute; margin-top: 2px;">
                        <?php if (isset($is_revisi) && $is_revisi == true) : ?>
                            <?php $revisiNomor = (!is_null($po->revisi_nomor)) ? (int) $po->revisi_nomor + 1 : 1; ?>
                            REV
                        <?php else : ?>
                            <?php $revisiNomorLabel = (isset($po->revisi_nomor) && !is_null($po->revisi_nomor)) ? ' : Rev. ' . $po->revisi_nomor : null; ?>
                            <?= (isset($po->id)) ? 'EDIT' : 'NEW' ?>
                        <?php endif ?>
                    </span>
                    <div style="margin-left: 50px;">
                        <h4 class="card-title"><?php echo (isset($card_title)) ? $card_title . $revisiNomorLabel : '' ?></h4>
                        <h6 class="card-subtitle"><?php echo (isset($card_subTitle)) ? $card_subTitle : '' ?></h6>
                    </div>
                    <div class="clear-card"></div>
                </div>
            </div>
            <div class="clear-card"></div>

            <!-- po -->
            <ul class="list-group list-group-flush">
                <li class="list-group-item">
                    <div class="row">
                        <div class="col-xs-12 col-auto"><b>Nomor :</b></div>
                        <div class="col-xs-12 col-md-10"><?= isset($po->nomor) ? $po->nomor : '' ?></div>
                    </div>
                </li>
                <li class="list-group-item">
                    <div class="row">
                        <div class="col-xs-12 col-auto"><b>PR Nomor :</b></div>
                        <div class="col-xs-12 col-md-10"><?= isset($po->pr_nomor) ? $po->pr_nomor : '' ?></div>
                    </div>
                </li>
                <li class="list-group-item">
                    <div class="row">
                        <div class="col-xs-12 col-auto"><b>Ref. Nomor :</b></div>
                        <div class="col-xs-12 col-md-10"><?= isset($po->ref_nomor) ? $po->ref_nomor : '' ?></div>
                    </div>
                </li>
                <li class="list-group-item">
                    <div class="row">
                        <div class="col-xs-12 col-auto"><b>Date :</b></div>
                        <div class="col-xs-12 col-md-10"><?= isset($po->tanggal) ? $controller->localizeDate($po->tanggal) : '' ?></div>
                    </div>
                </li>
                <li class="list-group-item">
                    <div class="row">
                        <div class="col-xs-12 col-auto"><b>Tipe :</b></div>
                        <div class="col-xs-12 col-md-10"><?= isset($po->tipe) ? $po->tipe : '' ?></div>
                    </div>
                </li>
                <li class="list-group-item">
                    <div class="row">
                        <div class="col-xs-12 col-auto"><b>Supplier :</b></div>
                        <div class="col-xs-12 col-md-10"><?= isset($supplier->nama_supplier) ? $supplier->nama_supplier : '' ?></div>
                    </div>
                </li>
            </ul>
            <!-- END ## po -->

            <div class="mb-4"></div>
            <div id="table-po-item">
                <?php include_once("_table_item.php"); ?>
            </div>

            <small class="form-text text-muted">
                Fields with red stars (<label required></label>) are required.
            </small>

            <div class="row">
                <div class="col">
                    <div class="buttons-container">
                        <div class="row">
                            <div class="col">
                                <a href="<?= base_url('po/wizard/' . $po->id . '/1') ?>" class="btn btn--raised btn-dark btn--icon-text btn-custom mr-2">
                                    <i class="zmdi zmdi-long-arrow-left"></i>
                                    Back
                                </a>
                            </div>
                            <div class="col text-right">
                                <button class="btn btn--raised btn-primary btn--icon-text btn-custom po-action-sent spinner-action-button" data-id="<?= $po->id ?>">
                                    Submit
                                    <i class="zmdi zmdi-long-arrow-right"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>