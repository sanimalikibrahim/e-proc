<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . 'controllers/AppBackend.php');

class Po extends AppBackend
{
  function __construct()
  {
    parent::__construct();
    $this->load->model([
      'AppModel',
      'SupplierModel',
      'SupplierbarangModel',
      'BarangjenisModel',
      'PoModel',
      'PoitemModel',
      'SkbModel',
    ]);
    $this->load->library('form_validation');
  }

  public function index()
  {
    $this->_tableList('index');
  }

  private function _tableList($module = 'index', $pageTitle = null)
  {
    $agent = new Mobile_Detect;
    $pageTitle = (!is_null($pageTitle)) ? ' › ' . $pageTitle : '';
    $data = array(
      'app' => $this->app(),
      'main_js' => $this->load_main_js('po', false, array(
        'controller' => $this,
        'module' => $module,
      )),
      'card_title' => 'Purchase Order' . $pageTitle,
      'is_mobile' => $agent->isMobile(),
      'module' => $module,
    );
    $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
    $this->template->load_view('index', $data, TRUE);
    $this->template->render();
  }

  public function wizard($id = null, $wizardId = 1)
  {
    $src = $this->input->get('src');

    if ($src === 'revisi') {
      $isRevisi = true;
    } else {
      $isRevisi = false;
    };

    switch ($wizardId) {
      case '1':
        $this->_wizardForm_1($id, $isRevisi);
        break;
      case '2':
        $this->_wizardForm_2($id, $isRevisi);
        break;
      default:
        $this->_wizardForm_1($id, $isRevisi);
        break;
    };
  }

  private function _wizardForm_1($id = null, $isRevisi = false)
  {
    $agent = new Mobile_Detect;
    $pr = $this->PoModel->getDetail(array('id' => $id, 'status' => 0));

    if (!is_null($id) && is_null($pr)) {
      show_404();
    } else {
      $supplier = $this->SupplierModel->getAll(array(), 'nama_supplier', 'asc');
      $supplierId_temp = (isset($pr->supplier_id)) ? $pr->supplier_id : null;

      $tipe = array(
        array('key' => 'Standar', 'text' => 'Standar'),
      );
      $tipe_temp = (isset($pr->tipe)) ? $pr->tipe : null;

      $data = array(
        'app' => $this->app(),
        'main_js' => $this->load_main_js('po', false, array(
          'controller' => $this,
          'po_id' => $id,
          'is_revisi' => $isRevisi,
        )),
        'card_title' => 'Purchase Order',
        'is_mobile' => $agent->isMobile(),
        'po' => $pr,
        'is_revisi' => $isRevisi,
        'generate_nomor' => $this->PoModel->generateNomor(),
        'supplier_list' => $this->init_list($supplier, 'id', 'nama_supplier', $supplierId_temp),
        'tipe_list' => $this->init_list($tipe, 'key', 'text', $tipe_temp),
      );
      $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
      $this->template->load_view('wizard_form', $data, TRUE);
      $this->template->render();
    };
  }

  private function _wizardForm_2($id = null)
  {
    $agent = new Mobile_Detect;
    $pr = $this->PoModel->getDetail(array('id' => $id, 'status' => 0));

    if (!is_null($pr)) {
      $supplier = $this->SupplierModel->getDetail(array('id' => $pr->supplier_id));
      $barang = $this->SupplierbarangModel->getAll(array('supplier_id' => $pr->supplier_id));

      $data = array(
        'app' => $this->app(),
        'controller' => $this,
        'main_js' => $this->load_main_js('po', false, array(
          'controller' => $this,
          'po_id' => $id
        )),
        'card_title' => 'Purchase Order › Item',
        'is_mobile' => $agent->isMobile(),
        'po' => $pr,
        'po_item' => $this->PoitemModel->getAll_sorted($id),
        'barang_list' => $this->init_list($barang, 'id', 'nama_barang'),
        'supplier' => $supplier,
      );
      $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
      $this->template->load_view('wizard_item_form', $data, TRUE);
      $this->template->render();
    } else {
      show_404();
    };
  }

  public function ajax_get_list()
  {
    $this->handle_ajax_request();
    $module = $this->input->get('src');
    $role = $this->session->userdata('user')['role'];

    switch ($module) {
      case 'index':
        if (strtolower($role) === strtolower('finance')) {
          $status = array(1, 2, 3);
          $pic = array();
        } else {
          $status = array();
          $pic = array('created_by' => $this->session->userdata('user')['id']);
        };
        break;
      default:
        $status = array();
        $pic = array('created_by' => $this->session->userdata('user')['id']);
        break;
    };

    $dtAjax_config = array(
      'table_name' => 'view_po',
      'order_column' => 5,
      'order_column_dir' => 'desc',
      'static_conditional' => $pic,
      'static_conditional_in_key' => 'status',
      'static_conditional_in' => $status,
    );
    $response = $this->AppModel->getData_dtAjax($dtAjax_config);

    echo json_encode($response);
  }

  public function ajax_save($id = null)
  {
    $this->handle_ajax_request();
    $this->form_validation->set_rules($this->PoModel->rules());

    if ($this->form_validation->run() === true) {
      if (is_null($id)) {
        echo json_encode($this->PoModel->insert());
      } else {
        $isRevisi = $this->input->post('is_revisi');

        if (!is_null($isRevisi)) {
          echo json_encode($this->PoModel->revisi($id));
        } else {
          echo json_encode($this->PoModel->update($id));
        };
      };
    } else {
      $errors = validation_errors('<div>- ', '</div>');
      echo json_encode(array('status' => false, 'data' => $errors));
    };
  }

  public function ajax_set_status()
  {
    $this->handle_ajax_request();
    $poId = $this->input->get('po_id');
    $status = $this->input->get('status');

    echo json_encode($this->PoModel->setStatus($poId, $status));
  }

  public function ajax_delete($id)
  {
    $this->handle_ajax_request();
    echo json_encode($this->PoModel->delete($id));
  }

  public function ajax_get_preview($id)
  {
    $this->handle_ajax_request();
    $pr = $this->PoModel->getDetail(array('id' => $id));

    if (!is_null($pr)) {
      $agent = new Mobile_Detect;
      $supplier = $this->SupplierModel->getDetail(array('id' => $pr->supplier_id));

      $data = array(
        'app' => $this->app(),
        'main_js' => $this->load_main_js('po'),
        'card_title' => 'Purchase Order › View',
        'controller' => $this,
        'data_id' => $id,
        'po' => $pr,
        'po_item' => $this->PoitemModel->getAll_sorted($id),
        'supplier' => $supplier,
      );

      if ($agent->isMobile()) {
        $this->load->view('_view_mobile', $data);
      } else {
        $this->load->view('_view', $data);
      };
    } else {
      show_404();
    };
  }

  // PO Item
  public function ajax_get_po_item_table($id = null)
  {
    $this->handle_ajax_request();
    $barang = $this->SupplierbarangModel->getAll();

    echo $this->load->view('_table_item', array(
      'app' => $this->app(),
      'po' => $this->PoModel->getDetail(array('id' => $id)),
      'po_item' => $this->PoitemModel->getAll_sorted($id),
      'barang_list' => $this->init_list($barang, 'id', 'nama_barang'),
    ), TRUE);
  }

  public function ajax_save_item($id = null)
  {
    $this->handle_ajax_request();
    $this->form_validation->set_rules($this->PoitemModel->rules());

    if ($this->form_validation->run() === true) {
      if (is_null($id)) {
        $transaction = $this->PoitemModel->insert();
      } else {
        $transaction = $this->PoitemModel->update($id);
      };

      if ($transaction['status'] === true) {
        // Store params when exist
        if (!is_null($this->input->post('description_as_barang'))) {
          $params = $this->input->post('description_param');
          $paramsPayload = array();

          if (!is_null($params)) {
            foreach ($params as $index => $item) {
              $paramsPayload[] = array(
                'po_id' => $this->input->post('po_id'),
                'po_item_parent_id' => $transaction['data_id'],
                'description' => $item['label'] . ' : ' . $item['value'],
                'is_italic' => 1,
              );
            };

            if (count($paramsPayload) > 0) {
              $this->PoitemModel->insertBatch($paramsPayload);
            };
          };
        };
        // END ## Store params when exist

        // Update master grand total
        $this->PoModel->updateTotalPrice($this->input->post('po_id'));
      };

      echo json_encode($transaction);
    } else {
      $errors = validation_errors('<div>- ', '</div>');
      echo json_encode(array('status' => false, 'data' => $errors));
    };
  }

  public function ajax_add_empty_row($poId = null)
  {
    $this->handle_ajax_request();
    $_POST['po_id'] = $poId;
    echo json_encode($this->PoitemModel->insert());
  }

  public function ajax_delete_item($id)
  {
    $this->handle_ajax_request();
    echo json_encode($this->PoitemModel->delete($id));
  }

  public function ajax_get_barang_param()
  {
    // $this->handle_ajax_request();
    $barangId = $this->input->get('barang_id');
    $barang = $this->SupplierbarangModel->getDetail(array('id' => $barangId));
    $result = null;

    if (!is_null($barang)) {
      $barangJenis = $this->BarangjenisModel->getAll_parameter($barang->jenis);
      $barangParameter = $barang->parameter;

      $result['unit'] = $barang->satuan;
      $result['unit_price'] = $barang->harga_satuan;
      $result['parameter'] = array();

      if (!is_null($barangParameter) && !empty($barangParameter)) {
        $barangParameter = json_decode($barangParameter);

        foreach ($barangParameter as $index => $item) {
          $findParamLabel = $this->searchInArrayObj($barangJenis, 'key', $index);

          if (count($findParamLabel) > 0) {
            $result['parameter'][] = array(
              'label' => $findParamLabel->label,
              'value' => $item
            );
          };
        };
      };
    };

    echo json_encode($result);
  }
  // END ## PO Item

  public function generate_skb($id = null)
  {
    $generate = $this->SkbModel->generateFromPo($id);

    if ($generate['status'] === true) {
      $this->PoModel->setEditable($id, 1);
      redirect(base_url('skb/wizard/' . $generate['data_id'] . '/1'));
    } else {
      show_error($generate['data'], '500');
    };
  }

  public function revisi($id)
  {
    $transaction = $this->PoModel->revisi($id);

    if ($transaction['status'] === true) {
      redirect(base_url('po/wizard/' . $id . '/1'));
    } else {
      show_error($transaction['data'], '500');
    };
  }

  public function get_status($status)
  {
    switch ($status) {
      case '0':
        return 'Draft';
        break;
      case '1':
        return 'Menunggu Persetujuan Finance';
        break;
      case '2':
        return 'Disetujui';
        break;
      case '3':
        return 'Ditolak';
        break;
      default:
        return 'Undefined';
        break;
    };
  }
}
