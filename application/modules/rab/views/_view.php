<?php require_once('main.css.php') ?>

<?php if ((isset($rab_attachment) && count($rab_attachment) > 0)) : ?>
  <div class="hidden-print mb-4">
    <div id="rab-other-wrapper">
      <!-- Attachment -->
      <div class="card m-0">
        <div class="card-header p-1" id="rab-attachment">
          <h5 class="mb-0">
            <button class="btn btn-link" data-toggle="collapse" data-target="#attachment-list" aria-expanded="true" aria-controls="attachment-list">
              <i class="zmdi zmdi-attachment mr-1" style="width: 10px; text-align: left;"></i>
              Attachment (<?= count($rab_attachment) ?>)
            </button>
          </h5>
        </div>
        <div id="attachment-list" class="collapse" aria-labelledby="rab-attachment" data-parent="#rab-other-wrapper">
          <div class="card-body p-3 pb-0">
            <div style="display: flex;">
              <div class="form-group" style="margin-bottom: 0px; flex: 1;">
                <div class="attachment p-0" style="margin-top: 0px; background: #fff;">
                  <?php if (count($rab_attachment) > 0) : ?>
                    <?php foreach ($rab_attachment as $key => $item) : ?>
                      <?php $isImage = (strpos($item->file_type, 'image') !== false) ? true : false ?>
                      <div class="attachment-item attachment-data-item-<?php echo $item->id ?>">
                        <div class="row">
                          <div class="col-auto">
                            <?php if ($isImage) : ?>
                              <img src="<?php echo base_url($item->file_name_thumb) ?>" class="attachment-preview attachment-preview-img" />
                            <?php else : ?>
                              <div class="attachment-preview attachment-preview-file">
                                <i class="zmdi zmdi-file-text"></i>
                              </div>
                            <?php endif; ?>
                          </div>
                          <div class="col-auto" style="padding-left: 0;">
                            <div style="<?php echo ($app->is_mobile) ? 'text-overflow: ellipsis; overflow: hidden; width: 150px; height: 1.2em; white-space: nowrap;' : '' ?>">
                              <a href="<?php echo base_url($item->file_name) ?>" data-fancybox class="<?= $isImage ? 'data-fancybox' : 'data-fancybox-iframe' ?>">
                                <span><?php echo $item->file_raw_name ?></span>
                              </a>
                            </div>
                            <span class="attachment-size">
                              <?php echo $item->file_size ?> KB
                            </span>
                          </div>
                        </div>
                      </div>
                    <?php endforeach; ?>
                  <?php else : ?>
                    <label class="pb-3">No data available</label>
                  <?php endif; ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- END ## Attachment -->
    </div>
  </div>
<?php endif ?>

<div class="preview-po" style="border: 0; padding: 0;">
  <div class="preview-header">
    <!-- Kop Surat -->
    <?php include_once(APPPATH . 'modules/_partial/kop_surat.php') ?>
    <!-- END ## Kop Surat -->
  </div>
  <div class="preview-body">
    <table style="width: 100%; margin-bottom: 15px;">
      <tr>
        <td style="font-size: 12px;" colspan="4">
          <b><u>RINCIAN ANGGARAN BIAYA</u></b>
          <div class="mb-2"></div>
        </td>
      </tr>
      <tr>
        <td style="font-size: 12px; width: 100px;">Project Name</td>
        <td style="font-size: 12px; width: 10px;">:</td>
        <td style="font-size: 12px;" colspan="2"><?= (isset($rab->project_name)) ? $rab->project_name : null ?></td>
        <td style="font-size: 12px;">&nbsp;</td>
      </tr>
      <tr>
        <td style="font-size: 12px; width: 100px;">Location</td>
        <td style="font-size: 12px; width: 10px;">:</td>
        <td style="font-size: 12px;" colspan="2"><?= (isset($rab->customer_name)) ? $rab->customer_name : null ?></td>
        <td style="font-size: 12px; text-align: right;"><?= isset($rab->tanggal) ? $controller->localizeDate($rab->tanggal) : null ?></td>
      </tr>
    </table>

    <table class="table-order-item" style="border-collapse: collapse;">
      <thead style="text-align: center;">
        <tr>
          <th rowspan="2" width="60">Item</th>
          <th rowspan="2" colspan="2">Works Description</th>
          <th rowspan="2" width="70">Qty</th>
          <th rowspan="2" width="70">Unit</th>
          <th colspan="2">Material / Equipments</th>
          <th colspan="2">Labour</th>
        </tr>
        <tr>
          <th width="110">Unit</th>
          <th width="110">Total</th>
          <th width="110">Unit</th>
          <th width="110">Total</th>
        </tr>
      </thead>
      <tbody>
        <?php
        $no = 1;
        $subTotal_material = 0;
        $subTotal_labour = 0;
        ?>
        <?php if (isset($rab_item) && count($rab_item) > 0) : ?>
          <?php foreach ($rab_item as $key => $item) : ?>
            <?php
            $isPreferences = '';
            $isPreferences .= ($item->is_bold == 1) ? 'font-weight: 600;' : '';
            $isPreferences .= ($item->is_italic == 1) ? 'font-style: italic;' : '';
            $subTotal_material = (!is_null($item->material_total_price) && !empty($item->material_total_price)) ? (float) $item->material_total_price + $subTotal_material : 0 + $subTotal_material;
            $subTotal_labour = (!is_null($item->labour_total_price) && !empty($item->labour_total_price)) ? (float) $item->labour_total_price + $subTotal_labour : 0 + $subTotal_labour;
            ?>
            <tr class="item-row-wrapper item-row-wrapper-<?= $item->id ?>" style="<?= $isPreferences ?>">
              <td align="center" style="height: 30.48px;"><?= $item->nomor ?></td>
              <td style="border-right-color: transparent;"><?= $item->description ?></td>
              <td><?= $item->note ?></td>
              <td style="text-align: right;"><?= (!is_null($item->quantity)) ? number_format($item->quantity, 0) : null ?></td>
              <td><?= $item->unit ?></td>
              <td style="text-align: right;"><?= (!is_null($item->material_unit_price)) ? number_format($item->material_unit_price, 0) : null ?></td>
              <td style="text-align: right;"><?= (!is_null($item->material_total_price)) ? number_format($item->material_total_price, 0) : null ?></td>
              <td style="text-align: right;"><?= (!is_null($item->labour_unit_price)) ? number_format($item->labour_unit_price, 0) : null ?></td>
              <td style="text-align: right;"><?= (!is_null($item->labour_total_price)) ? number_format($item->labour_total_price, 0) : null ?></td>
            </tr>
          <?php endforeach; ?>
        <?php else : ?>
          <tr>
            <td colspan="9" style="padding: 15px;">
              No data available in table
            </td>
          </tr>
        <?php endif; ?>
      </tbody>
      <tfoot>
        <tr>
          <td colspan="9" class="p-0" style="height: 5px;"></td>
        </tr>
        <tr>
          <th colspan="7" style="text-align: right;"><?= number_format($subTotal_material) ?></th>
          <th>&nbsp;</th>
          <th style="text-align: right;"><?= number_format($subTotal_labour) ?></th>
        </tr>
        <tr>
          <td colspan="9" class="p-0" style="height: 5px;"></td>
        </tr>
        <tr>
          <td colspan="8" class="text-center">
            <span style="padding-left: 13rem;">T O T A L</span>
          </td>
          <th style="text-align: right;"><u><?= number_format($subTotal_material + $subTotal_labour) ?></u></th>
        </tr>
      </tfoot>
    </table>
    <div style="margin-bottom: 15px;"></div>

    <?php if (!is_null($rab->note) && !empty($rab->note)) : ?>
      <span style="font-size: 12px;">Notes :</span>
      <div style="font-size: 12px;"><?= (isset($rab->note)) ? str_replace("\r\n", '<br/>', htmlspecialchars_decode($rab->note)) : null ?></div>
      <div style="margin-bottom: 30px;"></div>
    <?php endif ?>

    <span style="font-size: 12px;">Propose by,</span> <br />
    <span style="font-size: 12px;"><u>PT. ARYA JAYA</u></span>
  </div>
</div>