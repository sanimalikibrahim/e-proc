<div class="modal fade" id="modal-rab-view" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-xl modal-dialog-centered" style="<?php echo ($is_mobile) ? 'max-width: 98%' : '' ?>">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title pull-left modal-rab-view-title">Rincian Anggaran Biaya</h5>
      </div>
      <div class="spinner">
        <div class="lds-hourglass"></div>
      </div>
      <div class="modal-body">
        <div class="approval-preview rab-print-area">Please wait...</div>
      </div>
      <div class="modal-footer">
        <div class="btn-group" role="group">
          <button class="btn btn-secondary btn--icon-text action-rab-print">
            <i class="zmdi zmdi-print"></i> Print
          </button>
        </div>
        <button type="button" class="btn btn-light btn--icon-text" data-dismiss="modal">
          Close
        </button>
      </div>
    </div>
  </div>
</div>