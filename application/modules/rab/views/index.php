<?php require_once('main.css.php') ?>
<?php include_once('_view_modal.php') ?>
<?php
$activeRole = strtolower($this->session->userdata('user')['role']);
$roleAllowedCreate = array('engineering');
?>

<section id="rab">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title"><?php echo (isset($card_title)) ? $card_title : '' ?></h4>
            <h6 class="card-subtitle"><?php echo (isset($card_subTitle)) ? $card_subTitle : '' ?></h6>

            <?php if (in_array($activeRole, $roleAllowedCreate)) : ?>
                <div class="table-action">
                    <div class="buttons">
                        <a href="<?= base_url('rab/wizard') ?>" class="btn btn--raised btn-primary btn--icon-text">
                            <i class="zmdi zmdi-plus"></i> Add New
                        </a>
                    </div>
                </div>
            <?php endif ?>

            <div class="table-responsive">
                <table id="table-list" class="table table-bordered">
                    <thead class="thead-default">
                        <tr>
                            <th width="100">No</th>
                            <th>Nomor</th>
                            <th width="110">Tanggal</th>
                            <th>Project Name</th>
                            <th>Grand Total</th>
                            <th>Status</th>
                            <th width="200">Created At</th>
                            <th width="170" style="text-align: center;">#</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</section>