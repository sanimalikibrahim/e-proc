<?php require_once('main.css.php') ?>
<?php $revisiNomor = null ?>
<?php $revisiNomorLabel = null ?>

<section id="suratjalan">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <span class="badge badge-info" style="position: absolute; margin-top: 2px;">
                        <?php if (isset($is_revisi) && $is_revisi == true) : ?>
                            <?php $revisiNomor = (!is_null($rab->revisi_nomor)) ? (int) $rab->revisi_nomor + 1 : 1; ?>
                            REV
                        <?php else : ?>
                            <?php $revisiNomorLabel = (isset($rab->revisi_nomor) && !is_null($rab->revisi_nomor)) ? ' : Rev. ' . $rab->revisi_nomor : null; ?>
                            <?= (isset($rab->id)) ? 'EDIT' : 'NEW' ?>
                        <?php endif ?>
                    </span>
                    <div style="margin-left: 50px;">
                        <h4 class="card-title"><?php echo (isset($card_title)) ? $card_title . $revisiNomorLabel : '' ?></h4>
                        <h6 class="card-subtitle"><?php echo (isset($card_subTitle)) ? $card_subTitle : '' ?></h6>
                    </div>
                    <div class="clear-card"></div>
                </div>
            </div>
            <div class="clear-card"></div>

            <form id="form-rab" enctype="multipart/form-data" autocomplete="off">
                <input type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" value="<?= $this->security->get_csrf_hash(); ?>" />

                <!-- Temporary hidden field -->
                <?php if (isset($is_revisi) && $is_revisi == true) : ?>
                    <input type="hidden" name="is_revisi" value="true" readonly />
                    <input type="hidden" name="revisi_nomor" value="<?= $revisiNomor ?>" readonly />

                    <div class="alert alert-secondary mb-4">
                        <i class="zmdi zmdi-info"></i>
                        <b>Revisi <?= $revisiNomor ?></b>, silahkan masukan nomor baru! (Prev: <?= $rab->nomor ?>)
                    </div>
                <?php endif ?>

                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label required>Nomor</label>
                            <input type="text" name="nomor" class="form-control rab-nomor" placeholder="Nomor" value="<?= (isset($rab->nomor)) ? $rab->nomor : $generate_nomor ?>" readonly required />
                            <i class="form-group__bar"></i>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label required>Date</label>
                            <input type="text" name="tanggal" class="form-control rab-tanggal flatpickr-date bg-white" placeholder="Date" value="<?= (isset($rab->tanggal)) ? $rab->tanggal : null ?>" required />
                            <i class=" form-group__bar"></i>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label required>Project Name</label>
                    <input type="text" name="project_name" class="form-control rab-project_name" placeholder="Project Name" value="<?= (isset($rab->project_name)) ? $rab->project_name : null ?>" required />
                    <i class="form-group__bar"></i>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label required>Customer Name</label>
                            <input type="text" name="customer_name" class="form-control rab-customer_name" placeholder="Customer Name" value="<?= (isset($rab->customer_name)) ? $rab->customer_name : null ?>" required />
                            <i class="form-group__bar"></i>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label>Customer Attn</label>
                            <input type="text" name="customer_attn" class="form-control rab-customer_attn" placeholder="Customer Atn" value="<?= (isset($rab->customer_attn)) ? $rab->customer_attn : null ?>" />
                            <i class="form-group__bar"></i>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label>Customer Address</label>
                    <input type="text" name="customer_address" class="form-control rab-customer_address" placeholder="Customer Address" value="<?= (isset($rab->customer_address)) ? $rab->customer_address : null ?>" />
                    <i class="form-group__bar"></i>
                </div>
                <div class="form-group">
                    <label>Note</label>
                    <textarea name="note" class="form-control textarea-autosize text-counter rab-note" rows="1" data-max-length="1000" placeholder="Note" style="overflow: hidden; overflow-wrap: break-word; height: 31px;"><?= (isset($rab->note)) ? $rab->note : null ?></textarea>
                    <i class="form-group__bar"></i>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="form-group" style="margin-bottom: 0px;">
                            <label>Attachment</label>
                            <?php if (isset($rab->status) && (in_array((int) $rab->status, [0]))) : ?>
                                <a href="javascript:;" class="btn btn-sm btn-success btn--raised attachment-add" style="margin-left: 10px;">
                                    <i class="zmdi zmdi-file-plus"></i> Add File
                                </a>
                                <div class="attachment-input-wrapper"></div>
                            <?php endif; ?>
                        </div>
                        <div class="form-group">
                            <div class="attachment">
                                <?php if (isset($rab_attachment) && count($rab_attachment) > 0) : ?>
                                    <?php foreach ($rab_attachment as $key => $item) : ?>
                                        <?php $isImage = (strpos($item->file_type, 'image') !== false) ? true : false ?>
                                        <div class="attachment-item attachment-data-item-<?php echo $item->id ?>">
                                            <div class="row">
                                                <div class="col-auto">
                                                    <?php if ($isImage) : ?>
                                                        <img src="<?php echo base_url($item->file_name_thumb) ?>" class="attachment-preview attachment-preview-img" />
                                                    <?php else : ?>
                                                        <div class="attachment-preview attachment-preview-file">
                                                            <i class="zmdi zmdi-file-text"></i>
                                                        </div>
                                                    <?php endif; ?>
                                                </div>
                                                <div class="col" style="padding-left: 0;">
                                                    <div style="<?php echo ($is_mobile) ? 'text-overflow: ellipsis; overflow: hidden; width: 200px; height: 1.2em; white-space: nowrap;' : '' ?>">
                                                        <a href="<?php echo base_url($item->file_name) ?>" data-fancybox class="<?= $isImage ? 'data-fancybox' : 'data-fancybox-iframe' ?>">
                                                            <span><?php echo $item->file_raw_name ?></span>
                                                        </a>
                                                    </div>
                                                    <?php if (!isset($rab->status) || (in_array((int) $rab->status, [0]))) : ?>
                                                        <a href="javascript:;" class="link-black attachment-delete-existing" data-id="<?php echo $item->id ?>">
                                                            <small><i class="zmdi zmdi-close-circle"></i> Delete</small>
                                                        </a>
                                                    <?php endif; ?>
                                                    <span class="attachment-size">
                                                        <?php if (!isset($rab->status) || (in_array((int) $rab->status, [0]))) : ?>
                                                            <i class="zmdi zmdi-minus"></i>
                                                        <?php endif; ?>
                                                        <?php echo $item->file_size ?> KB
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                <?php else : ?>
                                    <label>No data available</label>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>

                <small class="form-text text-muted">
                    Fields with red stars (<label required></label>) are required.
                </small>

                <div class="row">
                    <div class="col">
                        <div class="buttons-container">
                            <div class="row">
                                <div class="col">
                                    <a href="<?= base_url('rab') ?>" class="btn btn-dark">Cancel</a>
                                </div>
                                <div class="col text-right">
                                    <button class="btn btn--raised btn-primary btn--icon-text btn-custom rab-action-save spinner-action-button">
                                        Save & Next
                                        <i class="zmdi zmdi-long-arrow-right"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>
</section>