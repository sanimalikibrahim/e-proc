<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . 'controllers/AppBackend.php');

class Rab extends AppBackend
{
  function __construct()
  {
    parent::__construct();
    $this->load->model([
      'AppModel',
      'SupplierbarangModel',
      'BarangjenisModel',
      'DocumentModel',
      'RabModel',
      'RabitemModel',
      'BqModel',
      'LomModel',
    ]);
    $this->load->library('form_validation');
  }

  public function index()
  {
    $this->_tableList('index');
  }

  private function _tableList($module = 'index', $pageTitle = null)
  {
    $agent = new Mobile_Detect;
    $pageTitle = (!is_null($pageTitle)) ? ' › ' . $pageTitle : '';
    $data = array(
      'app' => $this->app(),
      'main_js' => $this->load_main_js('rab', false, array(
        'controller' => $this,
        'module' => $module
      )),
      'card_title' => 'Rincian Anggaran Biaya' . $pageTitle,
      'is_mobile' => $agent->isMobile(),
      'module' => $module
    );
    $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
    $this->template->load_view('index', $data, TRUE);
    $this->template->render();
  }

  public function wizard($id = null, $wizardId = 1)
  {
    $src = $this->input->get('src');

    if ($src === 'revisi') {
      $isRevisi = true;
    } else {
      $isRevisi = false;
    };

    switch ($wizardId) {
      case '1':
        $this->_wizardForm_1($id, $isRevisi);
        break;
      case '2':
        $this->_wizardForm_2($id, $isRevisi);
        break;
      default:
        $this->_wizardForm_1($id, $isRevisi);
        break;
    };
  }

  private function _wizardForm_1($id = null, $isRevisi = false)
  {
    $agent = new Mobile_Detect;
    $rab = $this->RabModel->getDetail(array('id' => $id, 'status' => 0));

    if (!is_null($id) && is_null($rab)) {
      show_404();
    } else {
      $data = array(
        'app' => $this->app(),
        'main_js' => $this->load_main_js('rab', false, array(
          'controller' => $this,
          'rab_id' => $id,
          'is_revisi' => $isRevisi,
        )),
        'card_title' => 'Rincian Anggaran Biaya',
        'is_mobile' => $agent->isMobile(),
        'rab' => $rab,
        'is_revisi' => $isRevisi,
        'generate_nomor' => $this->RabModel->generateNomor(),
        'rab_attachment' => $this->DocumentModel->getAll(['ref' => 'rab', 'ref_id' => $id]),
      );
      $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
      $this->template->load_view('wizard_form', $data, TRUE);
      $this->template->render();
    };
  }

  private function _wizardForm_2($id = null)
  {
    $agent = new Mobile_Detect;
    $rab = $this->RabModel->getDetail(array('id' => $id, 'status' => 0));
    $barang = $this->SupplierbarangModel->getAll();

    if (!is_null($rab)) {
      $data = array(
        'app' => $this->app(),
        'controller' => $this,
        'main_js' => $this->load_main_js('rab', false, array(
          'controller' => $this,
          'rab_id' => $id
        )),
        'card_title' => 'Rincian Anggaran Biaya › Item',
        'is_mobile' => $agent->isMobile(),
        'rab' => $rab,
        'rab_item' => $this->RabitemModel->getAll_sorted($id),
        'barang_list' => $this->init_list($barang, 'id', 'nama_barang'),
        'rab_attachment' => $this->DocumentModel->getAll(['ref' => 'rab', 'ref_id' => $id]),
      );
      $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
      $this->template->load_view('wizard_item_form', $data, TRUE);
      $this->template->render();
    } else {
      show_404();
    };
  }

  public function ajax_get_list()
  {
    $this->handle_ajax_request();
    $module = $this->input->get('src');
    $role = $this->session->userdata('user')['role'];

    switch ($module) {
      case 'index':
        if (strtolower($role) === strtolower('finance')) {
          $status = array(1);
          $pic = array();
        } else if (strtolower($role) === strtolower('sales')) {
          $status = array(1);
          $pic = array();
        } else if (strtolower($role) === strtolower('teknik')) {
          $status = array(1);
          $pic = array();
        } else {
          $status = array();
          $pic = array('created_by' => $this->session->userdata('user')['id']);
        };
        break;
      default:
        $status = array();
        $pic = array('created_by' => $this->session->userdata('user')['id']);
        break;
    };

    $dtAjax_config = array(
      'table_name' => 'rab',
      'order_column' => 6,
      'order_column_dir' => 'desc',
      'static_conditional' => $pic,
      'static_conditional_in_key' => 'status',
      'static_conditional_in' => $status,
    );
    $response = $this->AppModel->getData_dtAjax($dtAjax_config);

    echo json_encode($response);
  }

  public function ajax_save($id = null)
  {
    $this->handle_ajax_request();
    $this->form_validation->set_rules($this->RabModel->rules());

    if ($this->form_validation->run() === true) {
      if (is_null($id)) {
        $transaction = $this->RabModel->insert();
      } else {
        $isRevisi = $this->input->post('is_revisi');

        if (!is_null($isRevisi)) {
          $transaction = $this->RabModel->revisi($id);
        } else {
          $transaction = $this->RabModel->update($id);
        };
      };

      if ($transaction['status'] === true) {
        $tempId = $transaction['data_id'];

        // Handle attachment upload
        $attachment = $this->_save_attachment($tempId);
        if ($attachment['status'] === false) {
          $transaction = $attachment;
        };
        // END ## Handle attachment upload
      };

      echo json_encode($transaction);
    } else {
      $errors = validation_errors('<div>- ', '</div>');
      echo json_encode(array('status' => false, 'data' => $errors));
    };
  }

  private function _save_attachment($id)
  {
    if (!empty($_FILES['file_name'])) {
      $cpUpload = new CpUpload();
      $files = $cpUpload->re_arrange($_FILES['file_name']);
      $post = array();
      $error = '';
      $directory = 'rab';

      foreach ($files as $item) {
        if (!empty($item['name'])) {
          $upload = $cpUpload->run($item, $directory, true, true, 'jpg|jpeg|png|pdf', true);

          if ($upload->status === true) {
            $post[] = array(
              'ref' => $directory,
              'ref_id' => $id,
              'description' => $_POST['nomor'],
              'file_raw_name' => $upload->data->raw_name . $upload->data->file_ext,
              'file_raw_name_thumb' => ($upload->data->is_image) ? $upload->data->raw_name . '_thumb' . $upload->data->file_ext : null,
              'file_name' => $upload->data->base_path,
              'file_name_thumb' => ($upload->data->is_image) ? 'directory/' . $directory . '/' . $upload->data->raw_name . '_thumb' . $upload->data->file_ext : null,
              'file_size' => $upload->data->file_size,
              'file_type' => $upload->data->file_type,
              'file_ext' => $upload->data->file_ext,
              'created_by' =>  $this->session->userdata('user')['id']
            );
          } else {
            $error .= $upload->data;
          };
        };
      };

      if (empty($error) && count($post) > 0) {
        return $this->DocumentModel->insertBatch($post);
      } else {
        return array('status' => false, 'data' => $error);
      };
    } else {
      return array('status' => true, 'data' => 'Skip, no data to upload.');
    };
  }

  public function ajax_set_status()
  {
    $this->handle_ajax_request();
    $rabId = $this->input->get('rab_id');
    $status = $this->input->get('status');

    echo json_encode($this->RabModel->setStatus($rabId, $status));
  }

  public function ajax_delete($id)
  {
    $this->handle_ajax_request();
    echo json_encode($this->RabModel->delete($id));
  }

  public function ajax_get_preview($id)
  {
    $this->handle_ajax_request();
    $rab = $this->RabModel->getDetail(array('id' => $id));

    if (!is_null($rab)) {
      $agent = new Mobile_Detect;
      $data = array(
        'app' => $this->app(),
        'main_js' => $this->load_main_js('rab'),
        'card_title' => 'Rincian Anggaran Biaya › View',
        'controller' => $this,
        'data_id' => $id,
        'rab' => $rab,
        'rab_item' => $this->RabitemModel->getAll_sorted($id),
        'rab_attachment' => $this->DocumentModel->getAll(['ref' => 'rab', 'ref_id' => $id]),
      );

      if ($agent->isMobile()) {
        $this->load->view('_view_mobile', $data);
      } else {
        $this->load->view('_view', $data);
      };
    } else {
      show_404();
    };
  }

  // Rincian Anggaran Biaya Item
  public function ajax_get_rab_item_table($id = null)
  {
    $this->handle_ajax_request();
    $barang = $this->SupplierbarangModel->getAll();

    echo $this->load->view('_table_item', array(
      'rab' => $this->RabModel->getDetail(array('id' => $id)),
      'rab_item' => $this->RabitemModel->getAll_sorted($id),
      'barang_list' => $this->init_list($barang, 'id', 'nama_barang'),
    ), TRUE);
  }

  public function ajax_save_item($id = null)
  {
    $this->handle_ajax_request();
    $this->form_validation->set_rules($this->RabitemModel->rules());

    if ($this->form_validation->run() === true) {
      if (is_null($id)) {
        $transaction = $this->RabitemModel->insert();
      } else {
        $transaction = $this->RabitemModel->update($id);
      };

      if ($transaction['status'] === true) {
        // Store params when exist
        if (!is_null($this->input->post('description_as_barang'))) {
          $params = $this->input->post('description_param');
          $paramsPayload = array();

          if (!is_null($params)) {
            foreach ($params as $index => $item) {
              $paramsPayload[] = array(
                'rab_id' => $this->input->post('rab_id'),
                'rab_item_parent_id' => $transaction['data_id'],
                'description' => $item['label'] . ' : ' . $item['value'],
                'is_italic' => 1,
              );
            };

            if (count($paramsPayload) > 0) {
              $this->RabitemModel->insertBatch($paramsPayload);
            };
          };
        };
        // END ## Store params when exist

        // Update master grand total
        $this->RabModel->updateTotalPrice($this->input->post('rab_id'));
      };

      echo json_encode($transaction);
    } else {
      $errors = validation_errors('<div>- ', '</div>');
      echo json_encode(array('status' => false, 'data' => $errors));
    };
  }

  public function ajax_add_empty_row($rabId = null)
  {
    $this->handle_ajax_request();
    $_POST['rab_id'] = $rabId;
    echo json_encode($this->RabitemModel->insert());
  }

  public function ajax_delete_item($id)
  {
    $this->handle_ajax_request();
    echo json_encode($this->RabitemModel->delete($id));
  }

  public function ajax_get_barang_param()
  {
    // $this->handle_ajax_request();
    $barangId = $this->input->get('barang_id');
    $barang = $this->SupplierbarangModel->getDetail(array('id' => $barangId));
    $result = null;

    if (!is_null($barang)) {
      $barangJenis = $this->BarangjenisModel->getAll_parameter($barang->jenis);
      $barangParameter = $barang->parameter;

      $result['unit'] = $barang->satuan;
      $result['material_unit_price'] = $barang->harga_satuan;
      $result['parameter'] = array();

      if (!is_null($barangParameter) && !empty($barangParameter)) {
        $barangParameter = json_decode($barangParameter);

        foreach ($barangParameter as $index => $item) {
          $findParamLabel = $this->searchInArrayObj($barangJenis, 'key', $index);

          if (count($findParamLabel) > 0) {
            $result['parameter'][] = array(
              'label' => $findParamLabel->label,
              'value' => $item
            );
          };
        };
      };
    };

    echo json_encode($result);
  }
  // END ## Rincian Anggaran Biaya Item

  public function ajax_delete_attachment($id)
  {
    $this->handle_ajax_request();
    echo json_encode($this->DocumentModel->delete($id));
  }

  public function generate_bq($id = null)
  {
    $generate = $this->BqModel->generateFromRab($id);

    if ($generate['status'] === true) {
      redirect(base_url('bq/edit/' . $generate['data_id']));
    } else {
      show_error($generate['data'], '500');
    };
  }

  public function generate_lom($id = null)
  {
    $generate = $this->LomModel->generateFromRab($id);

    if ($generate['status'] === true) {
      redirect(base_url('lom/wizard/' . $generate['data_id'] . '/1'));
    } else {
      show_error($generate['data'], '500');
    };
  }

  public function get_status($status)
  {
    switch ($status) {
      case '0':
        return 'Draft';
        break;
      case '1':
        return 'Submit';
        break;
      default:
        return 'Undefined';
        break;
    };
  }
}
