<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . 'controllers/AppBackend.php');

class Dashboard extends AppBackend
{
	function __construct()
	{
		parent::__construct();
		$this->load->model(array(
			'NotificationModel',
			'DashboardModel'
		));
	}

	public function index()
	{
		$data = array(
			'app' => $this->app(),
			'main_js' => $this->load_main_js('dashboard'),
			'page_title' => 'ٱلسَّلَامُ عَلَيْكُمْ‎',
			'page_subTitle' => 'Welcome to ' . $this->app()->app_name . ' v' . $this->app()->app_version,
		);

		$this->template->set('title', $data['app']->app_name, TRUE);
		$this->template->load_view('index', $data, TRUE);
		$this->template->render();
	}
}
