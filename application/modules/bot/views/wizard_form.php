<?php require_once('main.css.php') ?>
<?php $revisiNomor = null ?>
<?php $revisiNomorLabel = null ?>

<section id="suratjalan">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <span class="badge badge-info" style="position: absolute; margin-top: 2px;">
                        <?php if (isset($is_revisi) && $is_revisi == true) : ?>
                            <?php $revisiNomor = (!is_null($bot->revisi_nomor)) ? (int) $bot->revisi_nomor + 1 : 1; ?>
                            REV
                        <?php else : ?>
                            <?php $revisiNomorLabel = (isset($bot->revisi_nomor) && !is_null($bot->revisi_nomor)) ? ' : Rev. ' . $bot->revisi_nomor : null; ?>
                            <?= (isset($bot->id)) ? 'EDIT' : 'NEW' ?>
                        <?php endif ?>
                    </span>
                    <div style="margin-left: 50px;">
                        <h4 class="card-title"><?php echo (isset($card_title)) ? $card_title . $revisiNomorLabel : '' ?></h4>
                        <h6 class="card-subtitle"><?php echo (isset($card_subTitle)) ? $card_subTitle : '' ?></h6>
                    </div>
                    <div class="clear-card"></div>
                </div>
            </div>
            <div class="clear-card"></div>

            <form id="form-bot" enctype="multipart/form-data" autocomplete="off">
                <input type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" value="<?= $this->security->get_csrf_hash(); ?>" />

                <!-- Temporary hidden field -->
                <?php if (isset($is_revisi) && $is_revisi == true) : ?>
                    <input type="hidden" name="is_revisi" value="true" readonly />
                    <input type="hidden" name="revisi_nomor" value="<?= $revisiNomor ?>" readonly />

                    <div class="alert alert-secondary mb-4">
                        <i class="zmdi zmdi-info"></i>
                        <b>Revisi <?= $revisiNomor ?></b>, silahkan masukan nomor baru! (Prev: <?= $bot->nomor ?>)
                    </div>
                <?php endif ?>

                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label required>Nomor</label>
                            <input type="text" name="nomor" class="form-control bot-nomor" placeholder="Nomor" value="<?= (isset($bot->nomor)) ? $bot->nomor : $generate_nomor ?>" readonly required />
                            <i class="form-group__bar"></i>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label required>Date</label>
                            <input type="text" name="tanggal" class="form-control bot-tanggal flatpickr-date bg-white" placeholder="Date" value="<?= (isset($bot->tanggal)) ? $bot->tanggal : null ?>" required />
                            <i class=" form-group__bar"></i>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label>PO Nomor</label>
                            <input type="text" name="po_nomor" class="form-control bot-po_nomor" placeholder="PO Nomor" value="<?= (isset($bot->po_nomor)) ? $bot->po_nomor : null ?>" readonly />
                            <i class="form-group__bar"></i>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label>PO Date</label>
                            <input type="text" name="po_tanggal" class="form-control bot-po_tanggal" placeholder="PO Date" value="<?= (isset($bot->po_tanggal)) ? $bot->po_tanggal : null ?>" readonly />
                            <i class=" form-group__bar"></i>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label required>Project Name</label>
                    <input type="text" name="project_name" class="form-control bot-project_name" placeholder="Project Name" value="<?= (isset($bot->project_name)) ? $bot->project_name : null ?>" required readonly />
                    <i class="form-group__bar"></i>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label required>Customer Name</label>
                            <input type="text" name="customer_name" class="form-control bot-customer_name" placeholder="Customer Name" value="<?= (isset($bot->customer_name)) ? $bot->customer_name : null ?>" required readonly />
                            <i class="form-group__bar"></i>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label>Customer Attn</label>
                            <input type="text" name="customer_attn" class="form-control bot-customer_attn" placeholder="Customer Atn" value="<?= (isset($bot->customer_attn)) ? $bot->customer_attn : null ?>" readonly />
                            <i class="form-group__bar"></i>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label>Customer Address</label>
                    <input type="text" name="customer_address" class="form-control bot-customer_address" placeholder="Customer Address" value="<?= (isset($bot->customer_address)) ? $bot->customer_address : null ?>" readonly />
                    <i class="form-group__bar"></i>
                </div>
                <div class="form-group">
                    <label>Note</label>
                    <textarea name="note" class="form-control textarea-autosize text-counter bot-note" rows="1" data-max-length="1000" placeholder="Note" style="overflow: hidden; overflow-wrap: break-word; height: 31px;"><?= (isset($bot->note)) ? $bot->note : null ?></textarea>
                    <i class="form-group__bar"></i>
                </div>

                <small class="form-text text-muted">
                    Fields with red stars (<label required></label>) are required.
                </small>

                <div class="row">
                    <div class="col">
                        <div class="buttons-container">
                            <div class="row">
                                <div class="col">
                                    <a href="<?= base_url('bot') ?>" class="btn btn-dark">Cancel</a>
                                </div>
                                <div class="col text-right">
                                    <button class="btn btn--raised btn-primary btn--icon-text btn-custom bot-action-save spinner-action-button">
                                        Next
                                        <i class="zmdi zmdi-long-arrow-right"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>
</section>