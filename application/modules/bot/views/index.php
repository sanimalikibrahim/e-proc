<?php require_once('main.css.php') ?>
<?php include_once('_view_modal.php') ?>

<section id="bot">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title"><?php echo (isset($card_title)) ? $card_title : '' ?></h4>
            <h6 class="card-subtitle"><?php echo (isset($card_subTitle)) ? $card_subTitle : '' ?></h6>

            <div class="table-responsive">
                <table id="table-list" class="table table-bordered">
                    <thead class="thead-default">
                        <tr>
                            <th width="100">No</th>
                            <th>Nomor</th>
                            <th>Tanggal</th>
                            <th>Customer</th>
                            <th>Project Name</th>
                            <th>Status</th>
                            <th width="200">Created At</th>
                            <th width="170" style="text-align: center;">#</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</section>