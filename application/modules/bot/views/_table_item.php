<div class="table-responsive">
  <table class="table table-sm table-bordered table-hover">
    <thead style="text-align: center;">
      <tr>
        <th width="80">NO</th>
        <th colspan="2">MATERIAL / PART</th>
        <th colspan="2">QTY</th>
        <th width="150">TO COLLECTED</th>
        <th width="100">REMARK</th>
        <th width="100" class="text-center">#</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td colspan="8" class="p-0" style="height: 5px;"></td>
      </tr>
      <?php if (count($bot_item) > 0) : ?>
        <?php foreach ($bot_item as $index => $item) : ?>
          <?php
          $isPreferences = '';
          $isPreferences .= ($item->is_bold == 1) ? 'font-weight: 600;' : '';
          $isPreferences .= ($item->is_italic == 1) ? 'font-style: italic;' : '';
          ?>
          <div class="item-rows item-rows-<?= $item->id ?>">
            <!-- Row data -->
            <tr class="item-row-wrapper item-row-wrapper-<?= $item->id ?>" style="<?= $isPreferences ?>">
              <td align="center" style="height: 30.48px;">
                <?= $item->nomor ?>
              </td>
              <td style="border-right-color: transparent;">
                <?= $item->description ?>

                <?php if (!is_null($item->nomor) && !empty($item->nomor)) : ?>
                  <a href="javascript:;" data-id="<?= $item->id ?>" class="ml-2 item-button-row item-button-row-add item-button-row-<?= $item->id ?>" title="Add Items Below"><i class="zmdi zmdi-plus-circle"></i></a>
                <?php endif ?>
              </td>
              <td><?= $item->note ?></td>
              <td class="text-center" width="70"><?= (!is_null($item->quantity)) ? number_format($item->quantity, 0) : null ?></td>
              <td class="text-center" width="70"><?= $item->unit ?></td>
              <td class="text-center"><?= $item->to_collected ?></td>
              <td class="text-center"><?= $item->remark ?></td>
              <td align="center">
                <a href="javascript:;" data-id="<?= $item->id ?>" data-bot_item_parent_id="<?= $item->bot_item_parent_id ?>" data-nomor="<?= $item->nomor ?>" data-description="<?= $item->description ?>" data-note="<?= $item->note ?>" data-quantity="<?= $item->quantity ?>" data-unit="<?= $item->unit ?>" data-is_bold="<?= $item->is_bold ?>" data-is_italic="<?= $item->is_italic ?>" data-to_collected="<?= $item->to_collected ?>" data-remark="<?= $item->remark ?>" data-mos1_date="<?= $item->mos1_date ?>" data-mos1_qty="<?= $item->mos1_qty ?>" data-mos1_cl="<?= $item->mos1_cl ?>" data-mos2_date="<?= $item->mos2_date ?>" data-mos2_qty="<?= $item->mos2_qty ?>" data-mos2_cl="<?= $item->mos2_cl ?>" data-mos3_date="<?= $item->mos3_date ?>" data-mos3_qty="<?= $item->mos3_qty ?>" data-mos3_cl="<?= $item->mos3_cl ?>" class="btn btn-dark btn-xs item-button-row item-button-row-edit item-button-row-edit-<?= $item->id ?>" title="Edit">
                  <i class="zmdi zmdi-edit"></i>
                </a>
                <a href="javascript:;" data-id="<?= $item->id ?>" class="btn btn-danger btn-xs item-button-row item-button-row-delete item-button-row-delete-<?= $item->id ?>" title="Delete"><i class="zmdi zmdi-delete"></i></a>
              </td>
            </tr>
            <!-- END ## Row data -->
            <!-- Item inline form -->
            <tr class="item-form-row item-form-row-<?= $item->id ?> bg-light" style="display: none;">
              <td align="center">
                <span class="badge badge-warning item-form-row-icon-<?= $item->id ?>">N</span>
              </td>
              <td colspan="7" class="pl-0">
                <div class="item-form-input item-form-input-wrapper-<?= $item->id ?> bg-white p-3">
                  <!-- Loader -->
                  <div class="spinner spinner-<?= $item->id ?>">
                    <div class="lds-hourglass"></div>
                  </div>
                  <form method="post" id="item-form-input-<?= $item->id ?>">
                    <!-- Temporary hidden field -->
                    <input type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" value="<?= $this->security->get_csrf_hash(); ?>" />
                    <input type="hidden" name="bot_id" value="<?= $bot->id ?>" readonly />
                    <input type="hidden" name="bot_item_parent_id" class="bot_item-bot_item_parent_id-<?= $item->id ?>" value="<?= $item->id ?>" readonly />

                    <div class="row">
                      <div class="col-xs-12 col-md-12">
                        <div class="row">
                          <div class="col-xs-12 col-md-2">
                            <div class="form-group mb-3">
                              <label>Item</label>
                              <input type="text" name="nomor" class="form-control bot_item-nomor bot_item-nomor-<?= $item->id ?>" placeholder="Item" />
                              <i class="form-group__bar"></i>
                            </div>
                          </div>
                          <div class="col-xs-12 col-md-10">
                            <label>Preferences</label>
                            <div class="group-control mb-3">
                              <div class="row">
                                <div class="col-xs-12 col-md-1">
                                  <div class="form-check m-0">
                                    <input type="checkbox" name="is_bold" class="form-check-input bot_item-is_bold-<?= $item->id ?>" id="pref-bold-<?= $item->id ?>" value="1">
                                    <label class="form-check-label" for="pref-bold-<?= $item->id ?>">Bold</label>
                                  </div>
                                </div>
                                <div class="col-xs-12 col-md-1">
                                  <div class="form-check m-0">
                                    <input type="checkbox" name="is_italic" class="form-check-input bot_item-is_italic-<?= $item->id ?>" id="pref-italic-<?= $item->id ?>" value="1">
                                    <label class="form-check-label" for="pref-italic-<?= $item->id ?>">Italic</label>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-xs-12 col-md-12">
                          <div class="row">
                            <div class="col-xs-12 col-md-8">
                              <div class="form-group mb-3">
                                <label>Nama Part / Material</label>
                                <div class="input-group">
                                  <div class="input-group-prepend">
                                    <div class="form-control p-2" style="border-top-right-radius: 0; border-bottom-right-radius: 0;">
                                      <div class="form-check">
                                        <input type="checkbox" name="description_as_barang" class="form-check-input description-as_barang description-as_barang-<?= $item->id ?>" id="description-as_barang-<?= $item->id ?>" data-id="<?= $item->id ?>" value="1">
                                        <label class="form-check-label mt-1" for="description-as_barang-<?= $item->id ?>">Barang</label>
                                      </div>
                                    </div>
                                  </div>
                                  <!-- Manual -->
                                  <input type="text" name="description_manual" class="form-control bot_item-description_manual bot_item-description_manual-<?= $item->id ?>" data-id="<?= $item->id ?>" placeholder="Nama Part / Material" style="border-top-right-radius: 0.35rem; border-bottom-right-radius: 0.35rem;" />
                                  <!-- Auto -->
                                  <select name="description_auto" class="form-control bot_item-description_auto bot_item-description_auto-<?= $item->id ?> select2" data-id="<?= $item->id ?>" style="border-top-right-radius: 0.35rem; border-bottom-right-radius: 0.35rem; display: none;">
                                    <?= $barang_list ?>
                                  </select>
                                  <!-- Hidden temp -->
                                  <input type="hidden" name="description" class="bot_item-description bot_item-description-<?= $item->id ?>" readonly />
                                </div>
                                <!-- Params -->
                                <div class="form-control wrapper-params wrapper-params-<?= $item->id ?>" style="display: none;"></div>
                                <!-- END ## Params -->
                              </div>
                            </div>
                            <div class="col-xs-12 col-md-4">
                              <div class="form-group mb-3">
                                <label>Note</label>
                                <input type="text" name="note" class="form-control bot_item-note bot_item-note-<?= $item->id ?>" placeholder="Note" />
                                <i class="form-group__bar"></i>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-xs-12 col-md-2">
                            <div class="form-group mb-3">
                              <label>Quantity</label>
                              <input type="number" name="quantity" class="form-control bot_item-quantity bot_item-quantity-<?= $item->id ?>" data-id="<?= $item->id ?>" placeholder="Quantity" min="1" />
                              <i class="form-group__bar"></i>
                            </div>
                          </div>
                          <div class="col-xs-12 col-md-2">
                            <div class="form-group mb-3">
                              <label>Unit</label>
                              <input type="text" name="unit" class="form-control bot_item-unit bot_item-unit-<?= $item->id ?>" placeholder="Unit" />
                              <i class="form-group__bar"></i>
                            </div>
                          </div>
                          <div class="col-xs-12 col-md-2">
                            <div class="form-group mb-3">
                              <label>To Collected</label>
                              <input type="text" name="to_collected" class="form-control bot_item-to_collected bot_item-to_collected-<?= $item->id ?>" placeholder="To Collected" />
                              <i class="form-group__bar"></i>
                            </div>
                          </div>
                          <div class="col-xs-12 col-md-2">
                            <div class="form-group mb-3">
                              <label>Remark</label>
                              <input type="text" name="remark" class="form-control bot_item-remark bot_item-remark-<?= $item->id ?>" placeholder="Remark" />
                              <i class="form-group__bar"></i>
                            </div>
                          </div>
                        </div>
                        <table class="table table-xs table-condensed table-bordered">
                          <thead>
                            <tr class="text-center">
                              <th colspan="9">MOS</th>
                            </tr>
                            <tr class="text-center">
                              <th colspan="3">1</th>
                              <th colspan="3">2</th>
                              <th colspan="3">3</th>
                            </tr>
                            <tr class="text-center">
                              <th>Date</th>
                              <th>Qty</th>
                              <th>CL(√)</th>
                              <th>Date</th>
                              <th>Qty</th>
                              <th>CL(√)</th>
                              <th>Date</th>
                              <th>Qty</th>
                              <th>CL(√)</th>
                            </tr>
                          </thead>
                          <tr>
                            <td>
                              <input type="text" name="mos1_date" class="form-control form-control-sm bot_item-mos1_date bot_item-mos1_date-<?= $item->id ?> flatpickr-date bg-white" placeholder="Date" />
                            </td>
                            <td>
                              <input type="number" name="mos1_qty" class="form-control form-control-sm bot_item-mos_qty bot_item-mos1_qty bot_item-mos1_qty-<?= $item->id ?>" placeholder="Qty" data-id="<?= $item->id ?>" min="1" />
                            </td>
                            <td class="text-center">
                              <div class="form-check">
                                <input type="checkbox" name="mos1_cl" class="form-check-input bot_item-mos1_cl-<?= $item->id ?>" data-id="<?= $item->id ?>" value="1">
                              </div>
                            </td>
                            <td>
                              <input type="text" name="mos2_date" class="form-control form-control-sm bot_item-mos2_date bot_item-mos2_date-<?= $item->id ?> flatpickr-date bg-white" placeholder="Date" />
                            </td>
                            <td>
                              <input type="number" name="mos2_qty" class="form-control form-control-sm bot_item-mos_qty bot_item-mos2_qty bot_item-mos2_qty-<?= $item->id ?>" placeholder="Qty" data-id="<?= $item->id ?>" min="1" />
                            </td>
                            <td class="text-center">
                              <div class="form-check">
                                <input type="checkbox" name="mos2_cl" class="form-check-input bot_item-mos2_cl-<?= $item->id ?>" data-id="<?= $item->id ?>" value="1">
                              </div>
                            </td>
                            <td>
                              <input type="text" name="mos3_date" class="form-control form-control-sm bot_item-mos3_date bot_item-mos3_date-<?= $item->id ?> flatpickr-date bg-white" placeholder="Date" />
                            </td>
                            <td>
                              <input type="number" name="mos3_qty" class="form-control form-control-sm bot_item-mos_qty bot_item-mos3_qty bot_item-mos3_qty-<?= $item->id ?>" placeholder="Qty" data-id="<?= $item->id ?>" min="1" />
                            </td>
                            <td class="text-center">
                              <div class="form-check">
                                <input type="checkbox" name="mos3_cl" class="form-check-input bot_item-mos3_cl-<?= $item->id ?>" data-id="<?= $item->id ?>" value="1">
                              </div>
                            </td>
                          </tr>
                        </table>
                      </div>
                    </div>
                    <div class="col-xs-12 col-md-12">
                      <a href="javascript:;" class="btn btn-secondary item-form-input-cancel">Cancel</a>
                      <a href="javascript:;" class="btn btn-success item-form-input-save" data-id="<?= $item->id ?>"><i class="zmdi zmdi-save"></i> Save</a>
                    </div>
                </div>
                </form>
          </div>
          </td>
          </tr>
          <!-- END ## Item inline form -->
</div>
<?php endforeach ?>
<?php endif ?>

<!-- Header inline form -->
<div class="header-button">
  <tr class="header-form-row bg-light">
    <td align="center">
      <span class="badge badge-warning">NR</span>
    </td>
    <td colspan="7" class="pl-0">
      <a href="javascript:;" class="btn btn-secondary btn-sm header-button-row-add"><i class="zmdi zmdi-plus-circle"></i> Add New Row</a>
      <a href="javascript:;" class="btn btn-light btn-sm header-button-row-add-empty" data-bot_id="<?= $bot->id ?>"><i class="zmdi zmdi-plus-circle"></i> Add Empty Row</a>

      <div class="header-form-input-wrapper">
        <form method="post" id="header-form-input" class="bg-white p-3" style="display: none;">
          <!-- Loader -->
          <div class="spinner spinner-header">
            <div class="lds-hourglass"></div>
          </div>

          <!-- Temporary hidden field -->
          <input type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" value="<?= $this->security->get_csrf_hash(); ?>" />
          <input type="hidden" name="bot_id" value="<?= $bot->id ?>" readonly />
          <input type="hidden" name="bot_item_parent_id" value="" readonly />

          <div class="row">
            <div class="col-xs-12 col-md-12">
              <div class="row">
                <div class="col-xs-12 col-md-2">
                  <div class="form-group mb-3">
                    <label>Item</label>
                    <input type="text" name="nomor" class="form-control bot_item-nomor" placeholder="Item" />
                    <i class="form-group__bar"></i>
                  </div>
                </div>
                <div class="col-xs-12 col-md-10">
                  <label>Preferences</label>
                  <div class="group-control mb-3">
                    <div class="row">
                      <div class="col-xs-12 col-md-1">
                        <div class="form-check m-0">
                          <input type="checkbox" name="is_bold" class="form-check-input" id="pref-bold" value="1">
                          <label class="form-check-label" for="pref-bold">Bold</label>
                        </div>
                      </div>
                      <div class="col-xs-12 col-md-1">
                        <div class="form-check m-0">
                          <input type="checkbox" name="is_italic" class="form-check-input" id="pref-italic" value="1">
                          <label class="form-check-label" for="pref-italic">Italic</label>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-xs-12 col-md-12">
                <div class="row">
                  <div class="col-xs-12 col-md-8">
                    <div class="form-group mb-3">
                      <label>Nama Part / Material</label>
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <div class="form-control p-2" style="border-top-right-radius: 0; border-bottom-right-radius: 0;">
                            <div class="form-check">
                              <input type="checkbox" name="description_as_barang" class="form-check-input description-as_barang" id="description-as_barang-x1" data-id="x1" value="1">
                              <label class="form-check-label mt-1" for="description-as_barang-x1">Barang</label>
                            </div>
                          </div>
                        </div>
                        <!-- Manual -->
                        <input type="text" name="description_manual" class="form-control bot_item-description_manual bot_item-description_manual-x1" data-id="x1" placeholder="Nama Part / Material" style="border-top-right-radius: 0.35rem; border-bottom-right-radius: 0.35rem;" />
                        <!-- Auto -->
                        <select name="description_auto" class="form-control bot_item-description_auto bot_item-description_auto-x1 select2" data-id="x1" style="border-top-right-radius: 0.35rem; border-bottom-right-radius: 0.35rem; display: none;">
                          <?= $barang_list ?>
                        </select>
                        <!-- Hidden temp -->
                        <input type="hidden" name="description" class="bot_item-description bot_item-description-x1" readonly />
                      </div>
                      <!-- Params -->
                      <div class="form-control wrapper-params wrapper-params-x1" style="display: none;"></div>
                      <!-- END ## Params -->
                    </div>
                  </div>
                  <div class="col-xs-12 col-md-4">
                    <div class="form-group mb-3">
                      <label>Note</label>
                      <input type="text" name="note" class="form-control bot_item-note" placeholder="Note" />
                      <i class="form-group__bar"></i>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-xs-12 col-md-2">
                  <div class="form-group mb-3">
                    <label>Quantity</label>
                    <input type="number" name="quantity" class="form-control bot_item-quantity bot_item-quantity-x1" placeholder="Quantity" data-id="x1" min="1" />
                    <i class="form-group__bar"></i>
                  </div>
                </div>
                <div class="col-xs-12 col-md-2">
                  <div class="form-group mb-3">
                    <label>Unit</label>
                    <input type="text" name="unit" class="form-control bot_item-unit bot_item-unit-x1" placeholder="Unit" />
                    <i class="form-group__bar"></i>
                  </div>
                </div>
                <div class="col-xs-12 col-md-2">
                  <div class="form-group mb-3">
                    <label>To Collected</label>
                    <input type="text" name="to_collected" class="form-control bot_item-to_collected bot_item-to_collected-x1" placeholder="To Collected" />
                    <i class="form-group__bar"></i>
                  </div>
                </div>
                <div class="col-xs-12 col-md-2">
                  <div class="form-group mb-3">
                    <label>Remark</label>
                    <input type="text" name="remark" class="form-control bot_item-remark bot_item-remark-x1" placeholder="Remark" />
                    <i class="form-group__bar"></i>
                  </div>
                </div>
              </div>
              <table class="table table-xs table-condensed table-bordered">
                <thead>
                  <tr class="text-center">
                    <th colspan="9">MOS</th>
                  </tr>
                  <tr class="text-center">
                    <th colspan="3">1</th>
                    <th colspan="3">2</th>
                    <th colspan="3">3</th>
                  </tr>
                  <tr class="text-center">
                    <th>Date</th>
                    <th>Qty</th>
                    <th>CL(√)</th>
                    <th>Date</th>
                    <th>Qty</th>
                    <th>CL(√)</th>
                    <th>Date</th>
                    <th>Qty</th>
                    <th>CL(√)</th>
                  </tr>
                </thead>
                <tr>
                  <td>
                    <input type="text" name="mos1_date" class="form-control form-control-sm bot_item-mos1_date bot_item-mos1_date-x1 flatpickr-date bg-white" placeholder="Date" />
                  </td>
                  <td>
                    <input type="number" name="mos1_qty" class="form-control form-control-sm bot_item-mos_qty bot_item-mos1_qty bot_item-mos1_qty-x1" placeholder="Qty" data-id="x1" min="1" />
                  </td>
                  <td class="text-center">
                    <div class="form-check">
                      <input type="checkbox" name="mos1_cl" class="form-check-input bot_item-mos1_cl-x1" data-id="x1" value="1">
                    </div>
                  </td>
                  <td>
                    <input type="text" name="mos2_date" class="form-control form-control-sm bot_item-mos2_date bot_item-mos2_date-x1 flatpickr-date bg-white" placeholder="Date" />
                  </td>
                  <td>
                    <input type="number" name="mos2_qty" class="form-control form-control-sm bot_item-mos_qty bot_item-mos2_qty bot_item-mos2_qty-x1" placeholder="Qty" data-id="x1" min="1" />
                  </td>
                  <td class="text-center">
                    <div class="form-check">
                      <input type="checkbox" name="mos2_cl" class="form-check-input bot_item-mos2_cl-x1" data-id="x1" value="1">
                    </div>
                  </td>
                  <td>
                    <input type="text" name="mos3_date" class="form-control form-control-sm bot_item-mos3_date bot_item-mos3_date-x1 flatpickr-date bg-white" placeholder="Date" />
                  </td>
                  <td>
                    <input type="number" name="mos3_qty" class="form-control form-control-sm bot_item-mos_qty bot_item-mos3_qty bot_item-mos3_qty-x1" placeholder="Qty" data-id="x1" min="1" />
                  </td>
                  <td class="text-center">
                    <div class="form-check">
                      <input type="checkbox" name="mos3_cl" class="form-check-input bot_item-mos3_cl-x1" data-id="x1" value="1">
                    </div>
                  </td>
                </tr>
              </table>
            </div>
            <div class="col-xs-12 col-md-12">
              <a href="javascript:;" class="btn btn-secondary header-form-input-cancel">Cancel</a>
              <a href="javascript:;" class="btn btn-success header-form-input-save"><i class="zmdi zmdi-save"></i> Save</a>
            </div>
          </div>
      </div>
      </form>
</div>
</td>
</tr>
</div>
<!-- END ## Header inline form -->
</tbody>
</table>
</div>