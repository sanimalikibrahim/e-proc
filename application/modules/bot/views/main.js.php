<script type="text/javascript">
  $(document).ready(function() {
    var _key = "";
    var _section = "bot";
    var _table_list = "table-list";
    var _table_item_list = "table-bot_item";
    var _form_bot = "form-bot";
    var _botId = "<?= $bot_id ?>";
    var _modal_upload = "modal-form-upload";
    var _form_upload = "form-bot-upload";
    var _modal_approval = "modal-form-approval";
    var _form_approval = "form-bot-approval";
    var _modal_view = "modal-bot-view";
    var _modal_view_riwayat = "modal-bot-view-riwayat";
    var _module = "<?= $module ?>";
    var _modal_source = "";
    var _listFilter = "<?= (!is_null($this->input->get('ref'))) ? $this->input->get('ref') : null ?>";

    // Initialize DataTables : List
    if ($("#" + _table_list)[0]) {
      var table_bot = $("#" + _table_list).DataTable({
        processing: true,
        serverSide: true,
        ajax: {
          url: "<?php echo base_url('bot/ajax_get_list/?src=') ?>" + _module,
          type: "get"
        },
        columns: [{
            data: null,
            render: function(data, type, row, meta) {
              return meta.row + meta.settings._iDisplayStart + 1;
            }
          },
          {
            data: "nomor"
          },
          {
            data: "tanggal"
          },
          {
            data: "customer_name"
          },
          {
            data: "project_name"
          },
          {
            data: "status",
            render: function(data, type, row, meta) {
              var statusName = getStatus(data);
              var badgeType = "info";
              var revisi = (row.revisi_nomor != null && row.revisi_nomor != "") ? " (Rev. " + row.revisi_nomor + ")" : "";

              return '<span class="badge badge-' + badgeType + '">' + statusName + revisi + '</span>';
            }
          },
          {
            data: "created_at"
          },
          {
            data: null,
            className: "text-center",
            render: function(data, type, row, meta) {
              var htmlDom = "-";

              htmlDom = '<div class="action">';
              htmlDom += '<a href="javascript:;" class="action-view btn btn-light btn-sm mr-1" data-toggle="modal" data-target="#' + _modal_view + '"><i class="zmdi zmdi-eye"></i></a>&nbsp;';

              if (row.status == "0") {
                // Draft
                htmlDom += '<a href="<?= base_url('bot/wizard/') ?>' + row.id + '/1" class="action-edit btn btn-dark btn-sm mr-1"><i class="zmdi zmdi-edit"></i></a>&nbsp;';
              };

              htmlDom += '</div>';

              return htmlDom;
            }
          }
        ],
        autoWidth: !1,
        responsive: {
          details: {
            // display: $.fn.dataTable.Responsive.display.modal(),
            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
              tableClass: "table dt-details"
            }),
            type: "inline",
            target: 'tr',
          }
        },
        columnDefs: [{
          className: 'desktop',
          targets: [0, 1, 2, 3, 4, 5, 6, 7]
        }, {
          className: 'tablet',
          targets: [0, 1, 2]
        }, {
          className: 'mobile',
          targets: [0, 3]
        }, {
          responsivePriority: 1,
          targets: 0
        }, {
          responsivePriority: 2,
          targets: -1
        }],
        pageLength: 15,
        language: {
          searchPlaceholder: "Search...",
          sProcessing: '<div style="text-align: center;"><div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div></div>'
        },
        sDom: '<"dataTables_ct"><"dataTables__top"fb>rt<"dataTables__bottom"ip><"clear">',
        buttons: [{
          extend: "excelHtml5",
          title: "Export Result"
        }, {
          extend: "print",
          title: "Export Result"
        }],
        search: {
          search: _listFilter
        },
        initComplete: function(a, b) {
          $(this).closest(".dataTables_wrapper").find(".dataTables__top").prepend(
            '<div class="dataTables_buttons hidden-sm-down actions">' +
            '<span class="actions__item zmdi zmdi-refresh" data-table-action="reload" title="Reload" />' +
            '</div>'
          );
        },
      });

      $(".dataTables_filter input[type=search]").focus(function() {
        $(this).closest(".dataTables_filter").addClass("dataTables_filter--toggled")
      });

      $(".dataTables_filter input[type=search]").blur(function() {
        $(this).closest(".dataTables_filter").removeClass("dataTables_filter--toggled")
      });

      $("body").on("click", "[data-table-action]", function(a) {
        a.preventDefault();
        var b = $(this).data("table-action");
        if ("reload" === b) {
          $("#" + _table_list).DataTable().ajax.reload(null, false);
        };
      });
    };

    // bot ITEM
    $(document).on("click", "a.item-button-row-add", function(e) {
      e.preventDefault();
      var ref = $(this).attr("data-id");
      var addButton = $(".item-button-row");
      var form = $(".item-form-row-" + ref);
      var formIcon = $(".item-form-row-icon-" + ref);
      var formHeader = $(".header-form-row");
      var formInput = $("#item-form-input-" + ref);
      var otherInput = $(".item-other-input-" + ref);
      var rowWrapper = $(".item-row-wrapper-" + ref);
      var wrapperParam = $(".wrapper-params-" + ref);
      var checkboxAsBarang = $(".description-as_barang-" + ref);

      formIcon.html('N');
      addButton.fadeOut(100);
      form.fadeIn(100);
      formHeader.fadeOut(100);
      formInput.trigger("reset");
      otherInput.show();
      rowWrapper.addClass("bg-green text-white");
      wrapperParam.hide();
      checkboxAsBarang.removeAttr("disabled");
      $(".bot_item-description_manual-" + ref).show();
      $(".bot_item-description_auto-" + ref).select2().next().hide();

      _key = "";

      $('.mask-decimal').mask('#,##0,00', {
        reverse: true
      });
      $('.mask-money').mask('#,##0', {
        reverse: true
      });
      $(".flatpickr-date").flatpickr();
    });

    $(document).on("click", "a.item-form-input-cancel", function(e) {
      e.preventDefault();
      var addButton = $(".item-button-row");
      var form = $(".item-form-row");
      var formHeader = $(".header-form-row");
      var rowWrapper = $(".item-row-wrapper");

      addButton.fadeIn(100);
      form.fadeOut(100);
      formHeader.fadeIn(100);
      rowWrapper.removeClass("bg-green text-white");
    });

    $(document).on("click", "a.item-form-input-save", function(e) {
      e.preventDefault();
      var ref = $(this).attr("data-id");
      var addButton = $(".item-button-row");
      var form = $(".item-form-row-" + ref);
      var formHeader = $(".header-form-row");
      var formData = $("#item-form-input-" + ref);
      var rowWrapper = $(".item-row-wrapper-" + ref);

      adjustSpinner(ref);
      try {
        $.ajax({
          type: "post",
          url: "<?php echo base_url('bot/ajax_save_item/') ?>" + _key,
          data: formData.serialize(),
          success: function(response) {
            var response = JSON.parse(response);

            if (response.status === true) {
              fetchDataBotItem().then(() => {
                addButton.fadeIn(100);
                form.fadeOut(100);
                formHeader.fadeIn(100);
                rowWrapper.removeClass("bg-green text-white");
              });

              notify(response.data, "success");
            } else {
              notify(response.data, "danger");
            };
          }
        });
      } catch (error) {
        notify("Failed to save your data.", "danger");
      };
    });

    $(document).on("click", "a.item-button-row-edit", function(e) {
      e.preventDefault();
      var ref = $(this).attr("data-id");
      var bot_item_parent_id = $(this).attr("data-bot_item_parent_id");
      var nomor = $(this).attr("data-nomor");
      var description = $(this).attr("data-description");
      var note = $(this).attr("data-note");
      var quantity = $(this).attr("data-quantity");
      var unit = $(this).attr("data-unit");
      var is_bold = $(this).attr("data-is_bold");
      var is_italic = $(this).attr("data-is_italic");
      var to_collected = $(this).attr("data-to_collected");
      var remark = $(this).attr("data-remark");
      var mos1_date = $(this).attr("data-mos1_date");
      var mos1_qty = $(this).attr("data-mos1_qty");
      var mos1_cl = $(this).attr("data-mos1_cl");
      var mos2_date = $(this).attr("data-mos2_date");
      var mos2_qty = $(this).attr("data-mos2_qty");
      var mos2_cl = $(this).attr("data-mos2_cl");
      var mos3_date = $(this).attr("data-mos3_date");
      var mos3_qty = $(this).attr("data-mos3_qty");
      var mos3_cl = $(this).attr("data-mos3_cl");

      var addButton = $(".item-button-row");
      var form = $(".item-form-row-" + ref);
      var formIcon = $(".item-form-row-icon-" + ref);
      var formHeader = $(".header-form-row");
      var formInput = "#item-form-input-" + ref;
      var rowWrapper = $(".item-row-wrapper-" + ref);
      var wrapperParam = $(".wrapper-params-" + ref);
      var checkboxAsBarang = $(".description-as_barang-" + ref);

      formIcon.html('E');
      addButton.fadeOut(100);
      form.fadeIn(100);
      formHeader.fadeOut(100);
      rowWrapper.addClass("bg-green text-white");

      wrapperParam.hide();
      $(".bot_item-description_manual-" + ref).show();
      $(".bot_item-description_auto-" + ref).select2().next().hide();
      checkboxAsBarang.attr("disabled", true);

      $(".flatpickr-date").flatpickr();

      // Fill input value
      _key = ref;
      $(formInput).trigger("reset");
      $(formInput + " .bot_item-bot_item_parent_id-" + ref).val(bot_item_parent_id).trigger("input");
      $(formInput + " .bot_item-nomor-" + ref).val(nomor).trigger("input");
      $(formInput + " .bot_item-description-" + ref).val(description).trigger("input");
      $(formInput + " .bot_item-description_manual-" + ref).val(description).trigger("input");
      $(formInput + " .bot_item-note-" + ref).val(note).trigger("input");
      $(formInput + " .bot_item-quantity-" + ref).val(quantity).trigger("input");
      $(formInput + " .bot_item-unit-" + ref).val(unit).trigger("input");
      $(formInput + " .bot_item-to_collected-" + ref).val(to_collected).trigger("input");
      $(formInput + " .bot_item-remark-" + ref).val(remark).trigger("input");
      $(formInput + " .bot_item-mos1_date-" + ref).val(mos1_date).trigger("input");
      $(formInput + " .bot_item-mos1_qty-" + ref).val(mos1_qty).trigger("input");
      $(formInput + " .bot_item-mos1_cl-" + ref).val(mos1_cl).trigger("input");
      $(formInput + " .bot_item-mos2_date-" + ref).val(mos2_date).trigger("input");
      $(formInput + " .bot_item-mos2_qty-" + ref).val(mos2_qty).trigger("input");
      $(formInput + " .bot_item-mos2_cl-" + ref).val(mos2_cl).trigger("input");
      $(formInput + " .bot_item-mos3_date-" + ref).val(mos3_date).trigger("input");
      $(formInput + " .bot_item-mos3_qty-" + ref).val(mos3_qty).trigger("input");
      $(formInput + " .bot_item-mos3_cl-" + ref).val(mos3_cl).trigger("input");

      if (is_bold == 1) {
        $(formInput + " .bot_item-is_bold-" + ref).attr("checked", true);
      } else {
        $(formInput + " .bot_item-is_bold-" + ref).removeAttr("checked");
      };
      if (is_italic == 1) {
        $(formInput + " .bot_item-is_italic-" + ref).attr("checked", true);
      } else {
        $(formInput + " .bot_item-is_italic-" + ref).removeAttr("checked");
      };
      if (mos1_cl == 1) {
        $(formInput + " .bot_item-mos1_cl-" + ref).attr("checked", true);
      } else {
        $(formInput + " .bot_item-mos1_cl-" + ref).removeAttr("checked");
      };
      if (mos2_cl == 1) {
        $(formInput + " .bot_item-mos2_cl-" + ref).attr("checked", true);
      } else {
        $(formInput + " .bot_item-mos2_cl-" + ref).removeAttr("checked");
      };
      if (mos3_cl == 1) {
        $(formInput + " .bot_item-mos3_cl-" + ref).attr("checked", true);
      } else {
        $(formInput + " .bot_item-mos3_cl-" + ref).removeAttr("checked");
      };
      // END ## Fill input value

      $('.mask-decimal').mask('#,##0.00', {
        reverse: true
      });
      $('.mask-money').mask('#,##0', {
        reverse: true
      });
    });

    $(document).on("click", "a.item-button-row-delete", function(e) {
      e.preventDefault();
      var ref = $(this).attr("data-id");

      swal({
        title: "Are you sure to delete?",
        text: "Once deleted, you will not be able to recover this data!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false
      }).then((result) => {
        if (result.value) {
          $.ajax({
            type: "delete",
            url: "<?php echo base_url('bot/ajax_delete_item/') ?>" + ref,
            dataType: "json",
            success: function(response) {
              if (response.status) {
                fetchDataBotItem();
                notify(response.data, "success");
              } else {
                notify(response.data, "danger");
              };
            }
          });
        };
      });
    });

    $(document).on("click", "a.header-button-row-add", function(e) {
      e.preventDefault();
      var itemButton = $(".item-button-row");
      var addButton = $(".header-button-row-add");
      var addButtonEmpty = $(".header-button-row-add-empty");
      var formIcon = $(".header-form-row-icon");
      var formInput = $("#header-form-input");
      var wrapperParam = $(".wrapper-params");

      formIcon.html('N');
      formIcon.fadeIn(100);
      itemButton.fadeOut(100);
      addButton.hide();
      addButtonEmpty.hide();
      formInput.fadeIn(100);
      formInput.trigger("reset");
      wrapperParam.hide();
      $(".bot_item-description_manual").show();
      $(".bot_item-description_auto").select2().next().hide();

      $(".flatpickr-date").flatpickr();

      _key = "";
    });

    $(document).on("click", "a.header-form-input-cancel", function(e) {
      e.preventDefault();
      var itemButton = $(".item-button-row");
      var addButton = $(".header-button-row-add");
      var addButtonEmpty = $(".header-button-row-add-empty");
      var formIcon = $(".header-form-row-icon");
      var formInput = $("#header-form-input");

      itemButton.fadeIn(100);
      addButton.fadeIn(100);
      addButtonEmpty.fadeIn(100);
      formIcon.fadeOut(100);
      formInput.fadeOut(100);
    });

    $(document).on("click", "a.header-form-input-save", function(e) {
      e.preventDefault();
      var itemButton = $(".item-button-row");
      var addButton = $(".header-button-row-add");
      var addButtonEmpty = $(".header-button-row-add-empty");
      var formInput = $("#header-form-input");

      adjustSpinnerHeader("header");
      try {
        $.ajax({
          type: "post",
          url: "<?php echo base_url('bot/ajax_save_item/') ?>" + _key,
          data: formInput.serialize(),
          success: function(response) {
            var response = JSON.parse(response);

            if (response.status === true) {
              fetchDataBotItem().then(() => {
                itemButton.fadeIn(100);
                addButton.fadeIn(100);
                addButtonEmpty.fadeIn(100);
                formInput.fadeOut(100);
              });

              notify(response.data, "success");
            } else {
              notify(response.data, "danger");
            };
          }
        });
      } catch (error) {
        notify("Failed to save your data.", "danger");
      };
    });

    $(document).on("click", "a.header-button-row-add-empty", function(e) {
      e.preventDefault();
      try {
        var botId = $(this).attr("data-bot_id");

        $.ajax({
          type: "post",
          url: "<?php echo base_url('bot/ajax_add_empty_row/') ?>" + botId,
          success: function(response) {
            var response = JSON.parse(response);

            if (response.status === true) {
              fetchDataBotItem();
              notify(response.data, "success");
            } else {
              notify(response.data, "danger");
            };
          }
        });
      } catch (error) {
        notify("Failed to save your data.", "danger");
      };
    });

    // Handle input method
    $(document).on("click", ".description-as_barang", function() {
      var isAuto = $(this).is(":checked");
      var ref = $(this).attr("data-id");
      var manual = $(".bot_item-description_manual-" + ref);
      var auto = $(".bot_item-description_auto-" + ref);
      var hiddeName = $(".bot_item-description-" + ref);
      var wrapperParam = $(".wrapper-params-" + ref);

      // Clear input before
      manual.val("").trigger("input");
      auto.val($(".bot_item-description_auto-" + ref + " option:first").val()).trigger("change");
      hiddeName.val("").trigger("input");
      wrapperParam.hide();

      if (isAuto === true) {
        manual.hide();
        auto.select2().next().show();
      } else {
        manual.show();
        auto.select2().next().hide();
      };
    });

    // Handle input method: Manual
    $(document).on("keyup", ".bot_item-description_manual", function() {
      var value = $(this).val();
      var ref = $(this).attr("data-id");
      var hiddeName = $(".bot_item-description-" + ref);

      hiddeName.val(value);
    });

    // Handle input method: Auto
    $(document).on("select2:select", ".bot_item-description_auto", function(e) {
      var data = e.params.data;
      var ref = $(this).attr("data-id");
      var hiddeName = $(".bot_item-description-" + ref);
      var wrapperParam = $(".wrapper-params-" + ref);

      hiddeName.val(data.text);

      // Retrive parameter
      if (data.id != "" && data.id != null) {
        adjustSpinnerHeader("header");
        adjustSpinner(ref);

        $.ajax({
          type: "get",
          url: "<?= base_url('bot/ajax_get_barang_param/') ?>",
          data: {
            barang_id: data.id,
          },
          dataType: "json",
          success: function(response) {
            var component = "";

            $(".bot_item-unit-" + ref).val(response.unit).trigger("keyup");

            if (response.parameter.length > 0) {
              response.parameter.map((item, index) => {
                component += `
                  <div class="mb-2">
                    ${item.label} : ${item.value}
                    <input type="hidden" name="description_param[${index}][label]" value="${item.label}" readonly />
                    <input type="hidden" name="description_param[${index}][value]" value="${item.value}" readonly />
                  </div>
                `;
              });
              wrapperParam.show().html(component);
            } else {
              wrapperParam.hide();
            };

            $('.mask-money').unmask().mask('#,##0', {
              reverse: true
            });
          }
        });
      };
    });

    // Handle calculate to collected
    $(document).on("keyup click", ".bot_item-mos_qty, .bot_item-quantity", function() {
      var ref = $(this).attr("data-id");
      var collected = $(".bot_item-to_collected-" + ref);
      var qty = $(".bot_item-quantity-" + ref).val();
      var mosQty = $(".bot_item-mos_qty[data-id*='" + ref + "']");
      var remark = $(".bot_item-remark-" + ref);
      var result = 0;
      var remarkStatus = "";

      if (mosQty.length > 0) {
        $.each(mosQty, function(index, item) {
          var value = $(item).val();
          value = (value != "") ? parseFloat(value) : 0;

          result = result + value;
        });

        result = (result > 0) ? result : "";

        if (result == qty) {
          remarkStatus = "Done";
        } else if (result > qty) {
          remarkStatus = "> Quantity";
        } else {
          remarkStatus = "";
        };
      };

      collected.val(result).trigger("input");
      remark.val(remarkStatus).trigger("input");
    });

    async function fetchDataBotItem() {
      var botId = "<?= $bot_id ?>";

      await $.ajax({
        type: "get",
        url: "<?php echo base_url('bot/ajax_get_bot_item_table/') ?>" + botId,
        success: function(response) {
          $("#table-bot-item").html(response);
        }
      });
    };

    function adjustSpinner(ref) {
      $(".spinner-" + ref).css("width", $(".item-form-input-wrapper-" + ref).width() + 27);
      $(".spinner-" + ref).css("height", $(".item-form-input-wrapper-" + ref).height() + 27);
      $(".spinner-" + ref).css("margin-top", "-13px");
      $(".spinner-" + ref).css("margin-left", "-13px");
    };

    function adjustSpinnerHeader() {
      $(".spinner-header").css("width", $(".header-form-input-wrapper").width());
      $(".spinner-header").css("height", $(".header-form-input-wrapper").height());
      $(".spinner-header").css("margin-top", "-13px");
      $(".spinner-header").css("margin-left", "-13px");
    };
    // END ## bot ITEM

    // Handle data submit
    $("#" + _form_bot + " button.bot-action-save").on("click", function(e) {
      e.preventDefault();

      $.ajax({
        type: "post",
        url: "<?php echo base_url('bot/ajax_save/') ?>" + _botId,
        data: $("#" + _form_bot).serialize(),
        success: function(response) {
          var response = JSON.parse(response);
          if (response.status === true) {
            notify(response.data, "success");
            location.href = "<?= base_url('bot/wizard/') ?>" + response.data_id + "/2";
          } else {
            notify(response.data, "danger");
          };
        }
      });
    });

    // Handle data delete
    $("#" + _table_list).on("click", "a.action-delete", function(e) {
      e.preventDefault();
      var temp = table_bot.row($(this).closest('tr')).data();

      swal({
        title: "Are you sure to delete?",
        text: "Once deleted, you will not be able to recover this data!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false
      }).then((result) => {
        if (result.value) {
          $.ajax({
            type: "delete",
            url: "<?php echo base_url('bot/ajax_delete/') ?>" + temp.id,
            dataType: "json",
            success: function(response) {
              if (response.status) {
                $("#" + _table_list).DataTable().ajax.reload(null, false);
                notify(response.data, "success");
              } else {
                notify(response.data, "danger");
              };
            }
          });
        };
      });
    });

    // Handle view
    $("#" + _table_list).on("click", "a.action-view", function(e) {
      e.preventDefault();
      var temp = table_bot.row($(this).closest('tr')).data();

      $("#" + _modal_view + " .approval-preview").html("Please wait...");

      $.ajax({
        type: "get",
        url: "<?= base_url('bot/ajax_get_preview/') ?>" + temp.id,
        success: function(response) {
          $(".approval-preview").html(response);
          $(".modal-bot-view-title").html("Bill Of Tools : " + getStatus(temp.status));
        }
      });

      _modal_source = "view";
    });

    // Handle submit / publish
    $(document).on("click", "button.bot-action-sent", function(e) {
      e.preventDefault();
      var ref = $(this).attr("data-id");

      swal({
        title: "Are you sure to submit?",
        text: "Once submited, you will not be able to recover this data!",
        type: "question",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false
      }).then((result) => {
        if (result.value) {
          $.ajax({
            type: "get",
            url: "<?php echo base_url('bot/ajax_set_status/') ?>",
            data: {
              bot_id: ref,
              status: 1,
            },
            dataType: "json",
            success: function(response) {
              if (response.status) {
                notify(response.data, "success");

                setTimeout(function() {
                  location.href = "<?= base_url('bot') ?>";
                }, 2000);
              } else {
                notify(response.data, "danger");
              };
            }
          });
        };
      });
    });

    // Handle bot print
    $(document).on("click", "button.action-bot-print", function(e) {
      e.preventDefault();
      $(".bot-print-area").printThis();
    });
  });

  function getStatus(status) {
    switch (status.toString()) {
      case '0':
        return 'Draft';
        break;
      case '1':
        return 'Submit';
        break;
      default:
        return 'Undefined';
        break;
    };
  };
</script>