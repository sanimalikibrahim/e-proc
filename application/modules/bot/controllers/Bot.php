<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . 'controllers/AppBackend.php');

class Bot extends AppBackend
{
  function __construct()
  {
    parent::__construct();
    $this->load->model([
      'AppModel',
      'SupplierbarangModel',
      'BarangjenisModel',
      'BotModel',
      'BotitemModel',
    ]);
    $this->load->library('form_validation');
  }

  public function index()
  {
    $this->_tableList('index');
  }

  private function _tableList($module = 'index', $pageTitle = null)
  {
    $agent = new Mobile_Detect;
    $pageTitle = (!is_null($pageTitle)) ? ' › ' . $pageTitle : '';
    $data = array(
      'app' => $this->app(),
      'main_js' => $this->load_main_js('bot', false, array(
        'controller' => $this,
        'module' => $module
      )),
      'card_title' => 'Bill Of Tools' . $pageTitle,
      'is_mobile' => $agent->isMobile(),
      'module' => $module
    );
    $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
    $this->template->load_view('index', $data, TRUE);
    $this->template->render();
  }

  public function wizard($id = null, $wizardId = 1)
  {
    $src = $this->input->get('src');

    if ($src === 'revisi') {
      $isRevisi = true;
    } else {
      $isRevisi = false;
    };

    switch ($wizardId) {
      case '1':
        $this->_wizardForm_1($id, $isRevisi);
        break;
      case '2':
        $this->_wizardForm_2($id, $isRevisi);
        break;
      default:
        $this->_wizardForm_1($id, $isRevisi);
        break;
    };
  }

  private function _wizardForm_1($id = null, $isRevisi = false)
  {
    $agent = new Mobile_Detect;
    $bot = $this->BotModel->getDetail(array('id' => $id, 'status' => 0));

    if (!is_null($id) && is_null($bot)) {
      show_404();
    } else {
      $data = array(
        'app' => $this->app(),
        'main_js' => $this->load_main_js('bot', false, array(
          'controller' => $this,
          'bot_id' => $id,
          'is_revisi' => $isRevisi,
        )),
        'card_title' => 'Bill Of Tools',
        'is_mobile' => $agent->isMobile(),
        'bot' => $bot,
        'is_revisi' => $isRevisi,
        'generate_nomor' => $this->BotModel->generateNomor(),
      );
      $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
      $this->template->load_view('wizard_form', $data, TRUE);
      $this->template->render();
    };
  }

  private function _wizardForm_2($id = null)
  {
    $agent = new Mobile_Detect;
    $bot = $this->BotModel->getDetail(array('id' => $id, 'status' => 0));
    $barang = $this->SupplierbarangModel->getAll();

    if (!is_null($bot)) {
      $data = array(
        'app' => $this->app(),
        'controller' => $this,
        'main_js' => $this->load_main_js('bot', false, array(
          'controller' => $this,
          'bot_id' => $id
        )),
        'card_title' => 'Bill Of Tools › Item',
        'is_mobile' => $agent->isMobile(),
        'bot' => $bot,
        'bot_item' => $this->BotitemModel->getAll_sorted($id),
        'barang_list' => $this->init_list($barang, 'id', 'nama_barang'),
      );
      $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
      $this->template->load_view('wizard_item_form', $data, TRUE);
      $this->template->render();
    } else {
      show_404();
    };
  }

  public function ajax_get_list()
  {
    $this->handle_ajax_request();
    $module = $this->input->get('src');

    switch ($module) {
      case 'index':
        $status = array();
        $pic = array('created_by' => $this->session->userdata('user')['id']);
        break;
      default:
        $status = array();
        $pic = array('created_by' => $this->session->userdata('user')['id']);
        break;
    };

    $dtAjax_config = array(
      'table_name' => 'bot',
      'order_column' => 6,
      'order_column_dir' => 'desc',
      'static_conditional' => $pic,
      'static_conditional_in_key' => 'status',
      'static_conditional_in' => $status,
    );
    $response = $this->AppModel->getData_dtAjax($dtAjax_config);

    echo json_encode($response);
  }

  public function ajax_save($id = null)
  {
    $this->handle_ajax_request();
    $this->form_validation->set_rules($this->BotModel->rules());

    if ($this->form_validation->run() === true) {
      if (is_null($id)) {
        echo json_encode($this->BotModel->insert());
      } else {
        $isRevisi = $this->input->post('is_revisi');

        if (!is_null($isRevisi)) {
          echo json_encode($this->BotModel->revisi($id));
        } else {
          echo json_encode($this->BotModel->update($id));
        };
      };
    } else {
      $errors = validation_errors('<div>- ', '</div>');
      echo json_encode(array('status' => false, 'data' => $errors));
    };
  }

  public function ajax_set_status()
  {
    $this->handle_ajax_request();
    $botId = $this->input->get('bot_id');
    $status = $this->input->get('status');

    echo json_encode($this->BotModel->setStatus($botId, $status));
  }

  public function ajax_delete($id)
  {
    $this->handle_ajax_request();
    echo json_encode($this->BotModel->delete($id));
  }

  public function ajax_get_preview($id)
  {
    $this->handle_ajax_request();
    $bot = $this->BotModel->getDetail(array('id' => $id));

    if (!is_null($bot)) {
      $agent = new Mobile_Detect;
      $data = array(
        'app' => $this->app(),
        'main_js' => $this->load_main_js('bot'),
        'card_title' => 'Bot › View',
        'controller' => $this,
        'data_id' => $id,
        'bot' => $bot,
        'bot_item' => $this->BotitemModel->getAll_sorted($id),
      );

      if ($agent->isMobile()) {
        $this->load->view('_view_mobile', $data);
      } else {
        $this->load->view('_view', $data);
      };
    } else {
      show_404();
    };
  }

  // Bot Item
  public function ajax_get_bot_item_table($id = null)
  {
    $this->handle_ajax_request();
    $barang = $this->SupplierbarangModel->getAll();

    echo $this->load->view('_table_item', array(
      'bot' => $this->BotModel->getDetail(array('id' => $id)),
      'bot_item' => $this->BotitemModel->getAll_sorted($id),
      'barang_list' => $this->init_list($barang, 'id', 'nama_barang'),
    ), TRUE);
  }

  public function ajax_save_item($id = null)
  {
    $this->handle_ajax_request();
    $this->form_validation->set_rules($this->BotitemModel->rules());

    if ($this->form_validation->run() === true) {
      if (is_null($id)) {
        $transaction = $this->BotitemModel->insert();
      } else {
        $transaction = $this->BotitemModel->update($id);
      };

      if ($transaction['status'] === true) {
        // Store params when exist
        if (!is_null($this->input->post('description_as_barang'))) {
          $params = $this->input->post('description_param');
          $paramsPayload = array();

          if (!is_null($params)) {
            foreach ($params as $index => $item) {
              $paramsPayload[] = array(
                'bot_id' => $this->input->post('bot_id'),
                'bot_item_parent_id' => $transaction['data_id'],
                'description' => $item['label'] . ' : ' . $item['value'],
                'is_italic' => 1,
              );
            };

            if (count($paramsPayload) > 0) {
              $this->BotitemModel->insertBatch($paramsPayload);
            };
          };
        };
        // END ## Store params when exist
      };

      echo json_encode($transaction);
    } else {
      $errors = validation_errors('<div>- ', '</div>');
      echo json_encode(array('status' => false, 'data' => $errors));
    };
  }

  public function ajax_add_empty_row($botId = null)
  {
    $this->handle_ajax_request();
    $_POST['bot_id'] = $botId;
    echo json_encode($this->BotitemModel->insert());
  }

  public function ajax_delete_item($id)
  {
    $this->handle_ajax_request();
    echo json_encode($this->BotitemModel->delete($id));
  }

  public function ajax_get_barang_param()
  {
    // $this->handle_ajax_request();
    $barangId = $this->input->get('barang_id');
    $barang = $this->SupplierbarangModel->getDetail(array('id' => $barangId));
    $result = null;

    if (!is_null($barang)) {
      $barangJenis = $this->BarangjenisModel->getAll_parameter($barang->jenis);
      $barangParameter = $barang->parameter;

      $result['unit'] = $barang->satuan;
      $result['unit_price'] = $barang->harga_satuan;
      $result['parameter'] = array();

      if (!is_null($barangParameter) && !empty($barangParameter)) {
        $barangParameter = json_decode($barangParameter);

        foreach ($barangParameter as $index => $item) {
          $findParamLabel = $this->searchInArrayObj($barangJenis, 'key', $index);

          if (count($findParamLabel) > 0) {
            $result['parameter'][] = array(
              'label' => $findParamLabel->label,
              'value' => $item
            );
          };
        };
      };
    };

    echo json_encode($result);
  }
  // END ## Bot Item

  public function get_status($status)
  {
    switch ($status) {
      case '0':
        return 'Draft';
        break;
      case '1':
        return 'Submit';
        break;
      default:
        return 'Undefined';
        break;
    };
  }
}
