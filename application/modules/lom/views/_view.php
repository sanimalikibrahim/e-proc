<?php require_once('main.css.php') ?>

<div class="preview-po" style="border: 0; padding: 0;">
  <div class="preview-header">
    <!-- Kop Surat -->
    <?php include_once(APPPATH . 'modules/_partial/kop_surat.php') ?>
    <!-- END ## Kop Surat -->
  </div>
  <div class="preview-body">
    <table style="width: 100%">
      <tr>
        <td style="font-size: 12px;" colspan="3">
          <b><u>LIST OF MATERIAL</u></b>
          <div class="mb-2"></div>
        </td>
      </tr>
      <tr>
        <td>
          <table style="width: 100%; margin-bottom: 15px;">
            <tr>
              <td style="font-size: 12px; width: 80px;">Tanggal</td>
              <td style="font-size: 12px; width: 10px;">:</td>
              <td style="font-size: 12px;"><?= isset($lom->tanggal) ? $controller->localizeDate($lom->tanggal) : null ?></td>
            </tr>
            <tr>
              <td style="font-size: 12px; width: 80px;">Nomor</td>
              <td c width: 10px;">:</td>
              <td style="font-size: 12px;"><?= isset($lom->nomor) ? $lom->nomor : null ?></td>
            </tr>
            <tr>
              <td colspan="3" class="p-0" style="height: 10px;"></td>
            </tr>
            <tr>
              <td style="font-size: 12px; width: 100px;">Project</td>
              <td style="font-size: 12px; width: 10px;">:</td>
              <td style="font-size: 12px;" colspan="2"><?= (isset($lom->project_name)) ? $lom->project_name : null ?></td>
            </tr>
            <tr>
              <td style="font-size: 12px; width: 100px;">Customer</td>
              <td style="font-size: 12px; width: 10px;">:</td>
              <td style="font-size: 12px;" colspan="2"><?= (isset($lom->customer_name)) ? $lom->customer_name : null ?></td>
            </tr>
          </table>
        </td>
        <td align="right" valign="top">
          <table style="width: auto; margin-bottom: 15px;">
            <tr>
              <td style="font-size: 12px; width: 80px;">Tanggal PO</td>
              <td style="font-size: 12px; width: 10px;">:</td>
              <td style="font-size: 12px;"><?= isset($lom->po_tanggal) ? $controller->localizeDate($lom->po_tanggal) : null ?></td>
            </tr>
            <tr>
              <td style="font-size: 12px; width: 80px;">Nomor PO</td>
              <td c width: 10px;">:</td>
              <td style="font-size: 12px;"><?= isset($lom->po_nomor) ? $lom->po_nomor : null ?></td>
            </tr>
            <tr>
              <td colspan="3" class="p-0" style="height: 10px;"></td>
            </tr>
          </table>
        </td>
      </tr>
    </table>

    <table class="table-order-item" style="border-collapse: collapse;">
      <thead style="text-align: center;">
        <tr>
          <th width="80">No</th>
          <th colspan="2">Nama Part / Material</th>
          <th colspan="2">Qty</th>
        </tr>
      </thead>
      <tbody>
        <?php
        $no = 1;
        ?>
        <?php if (isset($lom_item) && count($lom_item) > 0) : ?>
          <?php foreach ($lom_item as $key => $item) : ?>
            <?php
            $isPreferences = '';
            $isPreferences .= ($item->is_bold == 1) ? 'font-weight: 600;' : '';
            $isPreferences .= ($item->is_italic == 1) ? 'font-style: italic;' : '';
            ?>
            <tr class="item-row-wrapper item-row-wrapper-<?= $item->id ?>" style="<?= $isPreferences ?>">
              <td align="center" style="height: 30.48px;"><?= $item->nomor ?></td>
              <td style="border-right-color: transparent;"><?= $item->description ?></td>
              <td><?= $item->note ?></td>
              <td class="text-center" width="70"><?= (!is_null($item->quantity)) ? number_format($item->quantity, 0) : null ?></td>
              <td class="text-center" width="70"><?= $item->unit ?></td>
            </tr>
          <?php endforeach; ?>
        <?php else : ?>
          <tr>
            <td colspan="5" style="padding: 15px;">
              No data available in table
            </td>
          </tr>
        <?php endif; ?>
      </tbody>
    </table>
    <div style="margin-bottom: 15px;"></div>

    <?php if (!is_null($lom->note) && !empty($lom->note)) : ?>
      <span style="font-size: 12px;">Notes :</span>
      <div style="font-size: 12px;"><?= (isset($lom->note)) ? str_replace("\r\n", '<br/>', htmlspecialchars_decode($lom->note)) : null ?></div>
    <?php endif ?>
  </div>
</div>