<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . 'controllers/AppBackend.php');

class Lom extends AppBackend
{
  function __construct()
  {
    parent::__construct();
    $this->load->model([
      'AppModel',
      'SupplierbarangModel',
      'BarangjenisModel',
      'LomModel',
      'LomitemModel',
      'BomModel',
    ]);
    $this->load->library('form_validation');
  }

  public function index()
  {
    $this->_tableList('index');
  }

  private function _tableList($module = 'index', $pageTitle = null)
  {
    $agent = new Mobile_Detect;
    $pageTitle = (!is_null($pageTitle)) ? ' › ' . $pageTitle : '';
    $data = array(
      'app' => $this->app(),
      'main_js' => $this->load_main_js('lom', false, array(
        'controller' => $this,
        'module' => $module
      )),
      'card_title' => 'List Of Material' . $pageTitle,
      'is_mobile' => $agent->isMobile(),
      'module' => $module
    );
    $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
    $this->template->load_view('index', $data, TRUE);
    $this->template->render();
  }

  public function wizard($id = null, $wizardId = 1)
  {
    $src = $this->input->get('src');

    if ($src === 'revisi') {
      $isRevisi = true;
    } else {
      $isRevisi = false;
    };

    switch ($wizardId) {
      case '1':
        $this->_wizardForm_1($id, $isRevisi);
        break;
      case '2':
        $this->_wizardForm_2($id, $isRevisi);
        break;
      default:
        $this->_wizardForm_1($id, $isRevisi);
        break;
    };
  }

  private function _wizardForm_1($id = null, $isRevisi = false)
  {
    $agent = new Mobile_Detect;
    $lom = $this->LomModel->getDetail(array('id' => $id, 'status' => 0));

    if (!is_null($id) && is_null($lom)) {
      show_404();
    } else {
      $data = array(
        'app' => $this->app(),
        'main_js' => $this->load_main_js('lom', false, array(
          'controller' => $this,
          'lom_id' => $id,
          'is_revisi' => $isRevisi,
        )),
        'card_title' => 'List Of Material',
        'is_mobile' => $agent->isMobile(),
        'lom' => $lom,
        'is_revisi' => $isRevisi,
        'generate_nomor' => $this->LomModel->generateNomor(),
      );
      $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
      $this->template->load_view('wizard_form', $data, TRUE);
      $this->template->render();
    };
  }

  private function _wizardForm_2($id = null)
  {
    $agent = new Mobile_Detect;
    $lom = $this->LomModel->getDetail(array('id' => $id, 'status' => 0));
    $barang = $this->SupplierbarangModel->getAll();

    if (!is_null($lom)) {
      $data = array(
        'app' => $this->app(),
        'controller' => $this,
        'main_js' => $this->load_main_js('lom', false, array(
          'controller' => $this,
          'lom_id' => $id
        )),
        'card_title' => 'List Of Material › Item',
        'is_mobile' => $agent->isMobile(),
        'lom' => $lom,
        'lom_item' => $this->LomitemModel->getAll_sorted($id),
        'barang_list' => $this->init_list($barang, 'id', 'nama_barang'),
      );
      $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
      $this->template->load_view('wizard_item_form', $data, TRUE);
      $this->template->render();
    } else {
      show_404();
    };
  }

  public function ajax_get_list()
  {
    $this->handle_ajax_request();
    $module = $this->input->get('src');

    switch ($module) {
      case 'index':
        $status = array();
        $pic = array('created_by' => $this->session->userdata('user')['id']);
        break;
      default:
        $status = array();
        $pic = array('created_by' => $this->session->userdata('user')['id']);
        break;
    };

    $dtAjax_config = array(
      'table_name' => 'lom',
      'order_column' => 6,
      'order_column_dir' => 'desc',
      'static_conditional' => $pic,
      'static_conditional_in_key' => 'status',
      'static_conditional_in' => $status,
    );
    $response = $this->AppModel->getData_dtAjax($dtAjax_config);

    echo json_encode($response);
  }

  public function ajax_save($id = null)
  {
    $this->handle_ajax_request();
    $this->form_validation->set_rules($this->LomModel->rules());

    if ($this->form_validation->run() === true) {
      if (is_null($id)) {
        echo json_encode($this->LomModel->insert());
      } else {
        $isRevisi = $this->input->post('is_revisi');

        if (!is_null($isRevisi)) {
          echo json_encode($this->LomModel->revisi($id));
        } else {
          echo json_encode($this->LomModel->update($id));
        };
      };
    } else {
      $errors = validation_errors('<div>- ', '</div>');
      echo json_encode(array('status' => false, 'data' => $errors));
    };
  }

  public function ajax_set_status()
  {
    $this->handle_ajax_request();
    $lomId = $this->input->get('lom_id');
    $status = $this->input->get('status');

    echo json_encode($this->LomModel->setStatus($lomId, $status));
  }

  public function ajax_delete($id)
  {
    $this->handle_ajax_request();
    echo json_encode($this->LomModel->delete($id));
  }

  public function ajax_get_preview($id)
  {
    $this->handle_ajax_request();
    $lom = $this->LomModel->getDetail(array('id' => $id));

    if (!is_null($lom)) {
      $agent = new Mobile_Detect;
      $data = array(
        'app' => $this->app(),
        'main_js' => $this->load_main_js('lom'),
        'card_title' => 'Lom › View',
        'controller' => $this,
        'data_id' => $id,
        'lom' => $lom,
        'lom_item' => $this->LomitemModel->getAll_sorted($id),
      );

      if ($agent->isMobile()) {
        $this->load->view('_view_mobile', $data);
      } else {
        $this->load->view('_view', $data);
      };
    } else {
      show_404();
    };
  }

  // Lom Item
  public function ajax_get_lom_item_table($id = null)
  {
    $this->handle_ajax_request();
    $barang = $this->SupplierbarangModel->getAll();

    echo $this->load->view('_table_item', array(
      'lom' => $this->LomModel->getDetail(array('id' => $id)),
      'lom_item' => $this->LomitemModel->getAll_sorted($id),
      'barang_list' => $this->init_list($barang, 'id', 'nama_barang'),
    ), TRUE);
  }

  public function ajax_save_item($id = null)
  {
    $this->handle_ajax_request();
    $this->form_validation->set_rules($this->LomitemModel->rules());

    if ($this->form_validation->run() === true) {
      if (is_null($id)) {
        $transaction = $this->LomitemModel->insert();
      } else {
        $transaction = $this->LomitemModel->update($id);
      };

      if ($transaction['status'] === true) {
        // Store params when exist
        if (!is_null($this->input->post('description_as_barang'))) {
          $params = $this->input->post('description_param');
          $paramsPayload = array();

          if (!is_null($params)) {
            foreach ($params as $index => $item) {
              $paramsPayload[] = array(
                'lom_id' => $this->input->post('lom_id'),
                'lom_item_parent_id' => $transaction['data_id'],
                'description' => $item['label'] . ' : ' . $item['value'],
                'is_italic' => 1,
              );
            };

            if (count($paramsPayload) > 0) {
              $this->LomitemModel->insertBatch($paramsPayload);
            };
          };
        };
        // END ## Store params when exist
      };

      echo json_encode($transaction);
    } else {
      $errors = validation_errors('<div>- ', '</div>');
      echo json_encode(array('status' => false, 'data' => $errors));
    };
  }

  public function ajax_add_empty_row($lomId = null)
  {
    $this->handle_ajax_request();
    $_POST['lom_id'] = $lomId;
    echo json_encode($this->LomitemModel->insert());
  }

  public function ajax_delete_item($id)
  {
    $this->handle_ajax_request();
    echo json_encode($this->LomitemModel->delete($id));
  }

  public function ajax_get_barang_param()
  {
    // $this->handle_ajax_request();
    $barangId = $this->input->get('barang_id');
    $barang = $this->SupplierbarangModel->getDetail(array('id' => $barangId));
    $result = null;

    if (!is_null($barang)) {
      $barangJenis = $this->BarangjenisModel->getAll_parameter($barang->jenis);
      $barangParameter = $barang->parameter;

      $result['unit'] = $barang->satuan;
      $result['unit_price'] = $barang->harga_satuan;
      $result['parameter'] = array();

      if (!is_null($barangParameter) && !empty($barangParameter)) {
        $barangParameter = json_decode($barangParameter);

        foreach ($barangParameter as $index => $item) {
          $findParamLabel = $this->searchInArrayObj($barangJenis, 'key', $index);

          if (count($findParamLabel) > 0) {
            $result['parameter'][] = array(
              'label' => $findParamLabel->label,
              'value' => $item
            );
          };
        };
      };
    };

    echo json_encode($result);
  }
  // END ## Lom Item

  public function generate_bom($id = null)
  {
    $generate = $this->BomModel->generateFromLom($id);

    if ($generate['status'] === true) {
      redirect(base_url('bom/wizard/' . $generate['data_id'] . '/1'));
    } else {
      show_error($generate['data'], '500');
    };
  }

  public function get_status($status)
  {
    switch ($status) {
      case '0':
        return 'Draft';
        break;
      case '1':
        return 'Submit';
        break;
      default:
        return 'Undefined';
        break;
    };
  }
}
