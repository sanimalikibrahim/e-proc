<?php require_once('main.css.php') ?>

<div class="preview-po" style="border: 0; padding: 0;">
  <div class="preview-header">
    <!-- Kop Surat -->
    <?php include_once(APPPATH . 'modules/_partial/kop_surat.php') ?>
    <!-- END ## Kop Surat -->
  </div>
  <div class="preview-body">
    <table style="width: 100%">
      <tr>
        <td style="font-size: 12px;" colspan="3">
          <b><u>BILL OF MATERIAL</u></b>
          <div class="mb-2"></div>
        </td>
      </tr>
      <tr>
        <td>
          <table style="width: 100%; margin-bottom: 15px;">
            <tr>
              <td style="font-size: 12px; width: 80px;">Tanggal</td>
              <td style="font-size: 12px; width: 10px;">:</td>
              <td style="font-size: 12px;"><?= isset($bom->tanggal) ? $controller->localizeDate($bom->tanggal) : null ?></td>
            </tr>
            <tr>
              <td style="font-size: 12px; width: 80px;">Nomor</td>
              <td c width: 10px;">:</td>
              <td style="font-size: 12px;"><?= isset($bom->nomor) ? $bom->nomor : null ?></td>
            </tr>
            <tr>
              <td colspan="3" class="p-0" style="height: 10px;"></td>
            </tr>
            <tr>
              <td style="font-size: 12px; width: 100px;">Project Name</td>
              <td style="font-size: 12px; width: 10px;">:</td>
              <td style="font-size: 12px;" colspan="2"><?= (isset($bom->project_name)) ? $bom->project_name : null ?></td>
            </tr>
            <tr>
              <td style="font-size: 12px; width: 100px;">Customer Name</td>
              <td style="font-size: 12px; width: 10px;">:</td>
              <td style="font-size: 12px;" colspan="2"><?= (isset($bom->customer_name)) ? $bom->customer_name : null ?></td>
            </tr>
            <tr>
              <td style="font-size: 12px; width: 120px;">Customer Address</td>
              <td style="font-size: 12px; width: 10px;">:</td>
              <td style="font-size: 12px;" colspan="2"><?= (isset($bom->customer_address)) ? $bom->customer_address : null ?></td>
            </tr>
          </table>
        </td>
        <td align="right" valign="top">
          <table style="width: auto; margin-bottom: 15px;">
            <tr>
              <td style="font-size: 12px; width: 80px;">Tanggal PO</td>
              <td style="font-size: 12px; width: 10px;">:</td>
              <td style="font-size: 12px;"><?= isset($bom->po_tanggal) ? $controller->localizeDate($bom->po_tanggal) : null ?></td>
            </tr>
            <tr>
              <td style="font-size: 12px; width: 80px;">Nomor PO</td>
              <td c width: 10px;">:</td>
              <td style="font-size: 12px;"><?= isset($bom->po_nomor) ? $bom->po_nomor : null ?></td>
            </tr>
            <tr>
              <td colspan="3" class="p-0" style="height: 10px;"></td>
            </tr>
          </table>
        </td>
      </tr>
    </table>

    <table class="table-order-item" style="border-collapse: collapse;">
      <thead style="text-align: center;">
        <tr>
          <th rowspan="3" width="80">NO</th>
          <th rowspan="3" colspan="2">MATERIAL / PART</th>
          <th rowspan="3" colspan="2">QTY</th>
          <th colspan="9">MOS</th>
          <th rowspan="3" width="110">TO COLLECTED</th>
          <th rowspan="3" width="80">REMARK</th>
        </tr>
        <tr>
          <th colspan="3">I</th>
          <th colspan="3">II</th>
          <th colspan="3">III</th>
        </tr>
        <tr>
          <th width="50">Date</th>
          <th width="50">Qty</th>
          <th width="50">CL(√)</th>
          <th width="50">Date</th>
          <th width="50">Qty</th>
          <th width="50">CL(√)</th>
          <th width="50">Date</th>
          <th width="50">Qty</th>
          <th width="50">CL(√)</th>
        </tr>
      </thead>
      <tbody>
        <?php
        $no = 1;
        ?>
        <?php if (isset($bom_item) && count($bom_item) > 0) : ?>
          <?php foreach ($bom_item as $key => $item) : ?>
            <?php
            $isPreferences = '';
            $isPreferences .= ($item->is_bold == 1) ? 'font-weight: 600;' : '';
            $isPreferences .= ($item->is_italic == 1) ? 'font-style: italic;' : '';
            ?>
            <tr class="item-row-wrapper item-row-wrapper-<?= $item->id ?>" style="<?= $isPreferences ?>">
              <td align="center" style="height: 30.48px;"><?= $item->nomor ?></td>
              <td style="border-right-color: transparent;"><?= $item->description ?></td>
              <td><?= $item->note ?></td>
              <td class="text-center" width="50"><?= (!is_null($item->quantity)) ? number_format($item->quantity, 0) : null ?></td>
              <td class="text-center" width="50"><?= $item->unit ?></td>
              <td class="text-center" width="60"><?= (!is_null($item->mos1_date)) ? date('d-M', strtotime($item->mos1_date)) : null ?></td>
              <td class="text-center" width="50"><?= $item->mos1_qty ?></td>
              <td class="text-center" width="50"><?= ($item->mos1_cl == 1) ? '√' : null ?></td>
              <td class="text-center" width="60"><?= (!is_null($item->mos2_date)) ? date('d-M', strtotime($item->mos2_date)) : null ?></td>
              <td class="text-center" width="50"><?= $item->mos2_qty ?></td>
              <td class="text-center" width="50"><?= ($item->mos2_cl == 1) ? '√' : null ?></td>
              <td class="text-center" width="60"><?= (!is_null($item->mos3_date)) ? date('d-M', strtotime($item->mos3_date)) : null ?></td>
              <td class="text-center" width="50"><?= $item->mos3_qty ?></td>
              <td class="text-center" width="50"><?= ($item->mos3_cl == 1) ? '√' : null ?></td>
              <td class="text-center"><?= $item->to_collected ?></td>
              <td class="text-center"><?= $item->remark ?></td>
            </tr>
          <?php endforeach; ?>
        <?php else : ?>
          <tr>
            <td colspan="16" style="padding: 15px;">
              No data available in table
            </td>
          </tr>
        <?php endif; ?>
      </tbody>
    </table>
    <div style="margin-bottom: 15px;"></div>

    <?php if (!is_null($bom->note) && !empty($bom->note)) : ?>
      <span style="font-size: 12px;">Notes :</span>
      <div style="font-size: 12px;"><?= (isset($bom->note)) ? str_replace("\r\n", '<br/>', htmlspecialchars_decode($bom->note)) : null ?></div>
    <?php endif ?>
  </div>
</div>