<div class="modal fade" id="modal-bom-view" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-xl modal-dialog-centered" style="<?php echo ($is_mobile) ? 'max-width: 98%' : '' ?>">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title pull-left modal-bom-view-title">Bill Of Material</h5>
      </div>
      <div class="spinner">
        <div class="lds-hourglass"></div>
      </div>
      <div class="modal-body">
        <div class="approval-preview bom-print-area">Please wait...</div>
      </div>
      <div class="modal-footer">
        <div class="btn-group" role="group">
          <button class="btn btn-secondary btn--icon-text action-bom-print">
            <i class="zmdi zmdi-print"></i> Print
          </button>
        </div>
        <button type="button" class="btn btn-light btn--icon-text" data-dismiss="modal">
          Close
        </button>
      </div>
    </div>
  </div>
</div>