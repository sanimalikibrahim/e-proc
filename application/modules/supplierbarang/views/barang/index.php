<section id="barang">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title"><?php echo (isset($card_title)) ? $card_title : '' ?></h4>
            <h6 class="card-subtitle"><?php echo (isset($card_subTitle)) ? $card_subTitle : '' ?></h6>

            <div class="table-action">
                <div class="buttons">
                    <a href="<?= base_url('supplierbarang/?ref=cxsmi&source=' . $supplier_id) ?>" class="btn btn--raised btn-dark btn--icon-text">
                        <i class="zmdi zmdi-arrow-left"></i>
                    </a>
                    <button class="btn btn--raised btn-primary btn--icon-text barang-action-add" data-toggle="modal" data-target="#modal-form-barang">
                        <i class="zmdi zmdi-plus-circle"></i> Buat Baru
                    </button>
                    <a href="<?= base_url('supplierbarang/xlsx_barang/?ref=cxsmi&id=' . $supplier_id . '&source=' . md5($supplier_id)) ?>" class="btn btn--raised btn-success btn--icon-text">
                        <i class="zmdi zmdi-download"></i> Export Excel
                    </a>
                </div>
            </div>

            <?php include_once('form.php') ?>

            <div class="table-responsive">
                <table id="table-barang" class="table table-bordered">
                    <thead class="thead-default">
                        <tr>
                            <th width="100">No</th>
                            <th>Kode</th>
                            <th>Nama Barang</th>
                            <th>Jumlah</th>
                            <th>Harga</th>
                            <th>Total Harga</th>
                            <th width="160">Created At</th>
                            <th width="170" style="text-align: center;">#</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</section>