<script type="text/javascript">
  $(document).ready(function() {

    var _key = "";
    var _section = "barang";
    var _table = "table-barang";
    var _modal = "modal-form-barang";
    var _form = "form-barang";
    var _button_separator = `<span class="text-muted">|</span>`;
    var _parameters = null;
    var _parameter_jenis = null;

    // Initialize DataTables: Index
    if ($("#" + _table)[0]) {
      var table_barang = $("#" + _table).DataTable({
        processing: true,
        serverSide: true,
        ajax: {
          url: "<?php echo base_url('supplierbarang/ajax_get_all_barang/?ref=cxsmi&supplier_id=' . $supplier_id) ?>",
          type: "get"
        },
        columns: [{
            data: null,
            render: function(data, type, row, meta) {
              return meta.row + meta.settings._iDisplayStart + 1;
            }
          },
          {
            data: "kode_barang"
          },
          {
            data: "nama_barang"
          },
          {
            data: "quantity",
            render: function(data, type, row, meta) {
              return $.fn.dataTable.render.number(',', '.', 0, '').display(data) + " " + row.satuan;
            }
          },
          {
            data: "harga_satuan",
            render: $.fn.dataTable.render.number(',', '.', 0)
          },
          {
            data: "total_harga",
            render: $.fn.dataTable.render.number(',', '.', 0)
          },

          {
            data: "created_at",
            render: function(data, type, row, meta) {
              return moment(data).format('Y-m-d H:mm:ss');
            }
          },
          {
            data: null,
            className: "center",
            render: function(data, type, row, meta) {
              return `
                <div class="action">
                  <a href="javascript:;" class="btn btn-sm btn-light btn-table-action action-edit" data-toggle="modal" data-target="#${_modal}"><i class="zmdi zmdi-edit"></i> Ubah</a>&nbsp;
                  <a href="javascript:;" class="btn btn-sm btn-danger btn-table-action action-delete"><i class="zmdi zmdi-delete"></i> Hapus</a>
                </div>
              `;
            }
          }
        ],
        autoWidth: !1,
        responsive: {
          details: {
            renderer: function(api, rowIdx, columns) {
              var hideColumn = [];
              var data = $.map(columns, function(col, i) {
                return ($.inArray(col.columnIndex, hideColumn)) ?
                  '<tr data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                  '<td class="dt-details-td">' + col.title + ':' + '</td> ' +
                  '<td class="dt-details-td">' + col.data + '</td>' +
                  '</tr>' :
                  '';
              }).join('');

              return data ? $('<table/>').append(data) : false;
            },
            type: "inline",
            target: 'tr',
          }
        },
        columnDefs: [{
          className: 'desktop',
          targets: [0, 1, 2, 3, 4, 5, 6, 7]
        }, {
          className: 'tablet',
          targets: [0, 2, 3, 4]
        }, {
          className: 'mobile',
          targets: [0, 2]
        }, {
          responsivePriority: 2,
          targets: -1
        }],
        pageLength: 15,
        language: {
          searchPlaceholder: "Cari...",
          sProcessing: '<div style="text-align: center;"><div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div></div>'
        },
        sDom: '<"dataTables_ct"><"dataTables__top"fb>rt<"dataTables__bottom"ip><"clear">',
        buttons: [{
          extend: "excelHtml5",
          title: "Export Result"
        }, {
          extend: "print",
          title: "Export Result"
        }],
        initComplete: function(a, b) {
          $(this).closest(".dataTables_wrapper").find(".dataTables__top").prepend(
            '<div class="dataTables_buttons hidden-sm-down actions">' +
            '<span class="actions__item zmdi zmdi-refresh" data-table-action="reload" title="Reload" />' +
            '</div>'
          );
        },
      });

      $(".dataTables_filter input[type=search]").focus(function() {
        $(this).closest(".dataTables_filter").addClass("dataTables_filter--toggled")
      });

      $(".dataTables_filter input[type=search]").blur(function() {
        $(this).closest(".dataTables_filter").removeClass("dataTables_filter--toggled")
      });

      $("body").on("click", "[data-table-action]", function(a) {
        a.preventDefault();
        var b = $(this).data("table-action");
        if ("reload" === b) {
          $("#" + _table).DataTable().ajax.reload(null, false);
        };
      });
    };

    // Handle data add
    $("#" + _section).on("click", "button." + _section + "-action-add", function(e) {
      e.preventDefault();
      resetForm();
    });

    // Handle data edit
    $("#" + _table).on("click", "a.action-edit", function(e) {
      e.preventDefault();
      resetForm();
      var temp = table_barang.row($(this).closest('tr')).data();

      // Set key for update params, important!
      _key = temp.id;

      $.each(temp, function(key, item) {
        $(`#${_form} .${_section}-${key}`).val(item).trigger("input").trigger("change");
      });

      // Extract parameters
      _parameter_jenis = temp.jenis;
      if (temp.parameter != null) {
        _parameters = JSON.parse(temp.parameter);
      };
    });

    // Handle data submit
    $("#" + _modal + " ." + _section + "-action-save").on("click", function(e) {
      e.preventDefault();
      $.ajax({
        type: "post",
        url: "<?php echo base_url('supplierbarang/ajax_save_barang/') ?>" + _key,
        data: $("#" + _form).serialize(),
        success: function(response) {
          var response = JSON.parse(response);
          if (response.status === true) {
            resetForm();
            $("#" + _modal).modal("hide");
            $("#" + _table).DataTable().ajax.reload(null, false);
            notify(response.data, "success");
          } else {
            notify(response.data, "danger");
          };
        }
      });
    });

    // Handle data delete
    $("#" + _table).on("click", "a.action-delete", function(e) {
      e.preventDefault();
      var temp = table_barang.row($(this).closest('tr')).data();

      swal({
        title: "Anda akan menghapus data, lanjutkan?",
        text: "Setelah dihapus, data tidak dapat dikembalikan lagi!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: "Ya",
        cancelButtonText: "Tidak",
        closeOnConfirm: false
      }).then((result) => {
        if (result.value) {
          $.ajax({
            type: "delete",
            url: "<?php echo base_url('supplierbarang/ajax_delete_barang/') ?>" + temp.id,
            dataType: "json",
            success: function(response) {
              if (response.status) {
                $("#" + _table).DataTable().ajax.reload(null, false);
                notify(response.data, "success");
              } else {
                notify(response.data, "danger");
              };
            }
          });
        };
      });
    });

    // Perform calculating
    $("#" + _form + " .barang-quantity").on("keyup click", function() {
      calculateAmount();
    });
    $("#" + _form + " .barang-harga_satuan").on("keyup", function() {
      calculateAmount();
    });

    // Handle jenis parameter
    $(document).on("change", ".barang-jenis", function() {
      var value = $(this).val();
      var parameter = $(".wrapper-barang-parameter");
      var component = "";

      if (value != "" && value != null) {
        $.ajax({
          type: "get",
          url: "<?= base_url('supplierbarang/get_jenis_parameter') ?>",
          data: {
            jenis_nama: value
          },
          dataType: "json",
          success: function(response) {
            if (response.length > 0) {
              response.map((item, index) => {
                component += `
                  <div class="form-group">
                    <label>${item.label}</label>
                    <input type="text" name="parameter[${item.key}]" class="form-control barang-parameter barang-parameter-${item.key}" placeholder="${item.label}" />
                    <i class="form-group__bar"></i>
                  </div>
                `;
              });

              if (component != "") {
                parameter.html(component);

                // Extract parameter
                if (_parameters != null && _parameter_jenis == value) {
                  $.each(_parameters, function(key, item) {
                    $(`#${_form} .${_section}-parameter-${key}`).val(item).trigger("input").trigger("change");
                  });
                };
              };
            } else {
              parameter.html("<i>Tidak ada parameter untuk jenis '" + value + "'.</i>");
            };
          }
        });
      };
    });

    // Handle form reset
    resetForm = () => {
      _key = "";
      _parameters = null;
      _parameter_jenis = null;
      $(`#${_form}`).trigger("reset");
      $(`#${_form} .${_section}-jenis`).val($(`#${_form} .${_section}-jenis option:first`).val()).trigger("change");
      $(".wrapper-barang-parameter").html("<i>Silahkan pilih jenis terlebih dahulu!</i>");
    };

    // Handle calculating
    calculateAmount = () => {
      var quantity = $("#" + _form + " .barang-quantity").val().replace(/[^\d]/g, "");
      quantity = (quantity != "") ? quantity : 0;

      var unit_price = $("#" + _form + " .barang-harga_satuan").val().replace(/[^\d]/g, "");
      unit_price = (unit_price != "") ? unit_price : 0;

      var amount = $("#" + _form + " .barang-total_harga");
      var result = unit_price * quantity;

      amount.val(result).trigger("input");
    };

  });
</script>