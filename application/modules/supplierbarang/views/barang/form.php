<div class="modal fade" id="modal-form-barang" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title pull-left">
          Barang
        </h5>
      </div>
      <div class="spinner">
        <div class="lds-hourglass"></div>
      </div>
      <div class="modal-body">
        <form id="form-barang" autocomplete="off">
          <!-- CSRF -->
          <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
          <input type="hidden" name="supplier_id" value="<?= $supplier_id ?>" />

          <div class="form-group">
            <label required>Supplier</label>
            <div class="form-control" style="background-color: #e0e5ec;">
              <?= isset($supplier->nama_supplier) ? $supplier->nama_supplier : '-' ?>
            </div>
            <i class="form-group__bar"></i>
          </div>
          <div class="row">
            <div class="col-xs-12 col-md-6">
              <div class="form-group">
                <label required>Jenis</label>
                <select name="jenis" class="form-control barang-jenis select2" required>
                  <?= $barang_jenis_list ?>
                </select>
                <i class="form-group__bar"></i>
              </div>
            </div>
            <div class="col-xs-12 col-md-6">
              <div class="form-group">
                <label required>Kode Barang</label>
                <input type="text" name="kode_barang" class="form-control barang-kode_barang" maxlength="30" placeholder="Kode Barang" required />
                <i class="form-group__bar"></i>
              </div>
            </div>
          </div>
          <div class="form-group">
            <label required>Nama Barang</label>
            <input type="text" name="nama_barang" class="form-control barang-nama_barang" maxlength="255" placeholder="Nama Barang" required />
            <i class="form-group__bar"></i>
          </div>
          <div class="form-group">
            <label>Parameter</label>
            <div class="form-control wrapper-barang-parameter">
              <i>Silahkan pilih jenis terlebih dahulu!</i>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-12 col-md-6">
              <div class="form-group">
                <label required>Jumlah</label>
                <input type="number" name="quantity" class="form-control mask-number barang-quantity" min="0" placeholder="Quantity" required />
                <i class="form-group__bar"></i>
              </div>
            </div>
            <div class="col-xs-12 col-md-6">
              <div class="form-group">
                <label required>Satuan</label>
                <input type="text" name="satuan" class="form-control barang-satuan" maxlength="30" placeholder="Satuan" required />
                <i class="form-group__bar"></i>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-12 col-md-6">
              <div class="form-group">
                <label required>Harga Satuan</label>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">Rp</span>
                  </div>
                  <input type="text" name="harga_satuan" class="form-control mask-money barang-harga_satuan" min="0" placeholder="Harga Satuan" required />
                </div>
                <i class="form-group__bar"></i>
              </div>
            </div>
            <div class="col-xs-12 col-md-6">
              <div class="form-group">
                <label required>Total Harga</label>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">Rp</span>
                  </div>
                  <input type="text" name="total_harga" class="form-control mask-money barang-total_harga" min="0" placeholder="Total Harga" required readonly />
                </div>
                <i class="form-group__bar"></i>
              </div>
            </div>
          </div>

          <small class="form-text text-muted">
            Fields with red stars (<label required></label>) are required.
          </small>

        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success btn--icon-text barang-action-save">
          <i class="zmdi zmdi-save"></i> Simpan
        </button>
        <button type="button" class="btn btn-light btn--icon-text barang-action-cancel" data-dismiss="modal">
          Batal
        </button>
      </div>
    </div>
  </div>
</div>