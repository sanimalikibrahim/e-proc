<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . 'controllers/AppBackend.php');

class Supplierbarang extends AppBackend
{
  function __construct()
  {
    parent::__construct();
    $this->load->model(array(
      'AppModel',
      'BarangjenisModel',
      'SupplierModel',
      'SupplierbarangModel',
    ));
    $this->load->library('form_validation');
  }

  // Supplier
  public function index()
  {
    $data = array(
      'app' => $this->app(),
      'main_js' => $this->load_main_js('supplierbarang'),
      'card_title' => 'Supplier Barang',
    );
    $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
    $this->template->load_view('index', $data, TRUE);
    $this->template->render();
  }

  public function ajax_get_all_supplier()
  {
    $this->handle_ajax_request();
    $dtAjax_config = array(
      'table_name' => 'view_supplier',
      'order_column' => 3,
      'order_column_dir' => 'desc'
    );
    $response = $this->AppModel->getData_dtAjax($dtAjax_config);
    echo json_encode($response);
  }

  public function ajax_save_supplier($id = null)
  {
    $this->handle_ajax_request();
    $this->form_validation->set_rules($this->SupplierModel->rules());

    if ($this->form_validation->run() === true) {
      if (is_null($id)) {
        echo json_encode($this->SupplierModel->insert());
      } else {
        echo json_encode($this->SupplierModel->update($id));
      };
    } else {
      $errors = validation_errors('<div>- ', '</div>');
      echo json_encode(array('status' => false, 'data' => $errors));
    };
  }

  public function ajax_delete_supplier($id)
  {
    $this->handle_ajax_request();
    echo json_encode($this->SupplierModel->delete($id));
  }

  private function check_supplier($id)
  {
    $model = $this->SupplierModel->getDetail(array('id' => $id));

    if (!is_null($model)) {
      return $model;
    } else {
      show_404();
    };
  }
  // END ## Supplier

  // Barang
  public function item()
  {
    $supplierId = $this->input->get('id');
    $supplier = $this->check_supplier($supplierId);
    $barangJenis = $this->BarangjenisModel->getAll_nama();

    $data = array(
      'app' => $this->app(),
      'main_js' => $this->load_main_js('supplierbarang/views/barang/main.js.php', true, array(
        'supplier_id' => $supplierId,
        'supplier' => $supplier
      )),
      'card_title' => 'Supplier Barang › ' . $supplier->nama_supplier,
      'supplier_id' => $supplierId,
      'supplier' => $supplier,
      'barang_jenis' => $barangJenis,
      'barang_jenis_list' => $this->init_list($barangJenis, 'nama', 'nama'),
    );
    $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
    $this->template->load_view('barang/index', $data, TRUE);
    $this->template->render();
  }

  public function ajax_get_all_barang()
  {
    $this->handle_ajax_request();

    $supplierId = $this->input->get('supplier_id');
    $dtAjax_config = array(
      'table_name' => 'view_supplier_barang',
      'order_column' => 6,
      'order_column_dir' => 'desc',
      'static_conditional' => array(
        'supplier_id' => $supplierId
      )
    );
    $response = $this->AppModel->getData_dtAjax($dtAjax_config);
    echo json_encode($response);
  }

  public function ajax_save_barang($id = null)
  {
    $this->handle_ajax_request();
    $this->form_validation->set_rules($this->SupplierbarangModel->rules());

    if ($this->form_validation->run() === true) {
      $parameter = $this->input->post('parameter');

      if (!is_null($parameter)) {
        $_POST['parameter'] = json_encode($parameter);
      };

      if (is_null($id)) {
        echo json_encode($this->SupplierbarangModel->insert());
      } else {
        echo json_encode($this->SupplierbarangModel->update($id));
      };
    } else {
      $errors = validation_errors('<div>- ', '</div>');
      echo json_encode(array('status' => false, 'data' => $errors));
    };
  }

  public function ajax_delete_barang($id)
  {
    $this->handle_ajax_request();
    echo json_encode($this->SupplierbarangModel->delete($id));
  }

  public function get_jenis_parameter()
  {
    $this->handle_ajax_request();
    $jenisNama = $this->input->get('jenis_nama');
    $barangJenisParam = $this->BarangjenisModel->getAll_parameter($jenisNama);

    echo json_encode($barangJenisParam);
  }

  public function xlsx_barang()
  {
    try {
      $supplierId = $this->input->get('id');

      $fileTemplate = FCPATH . 'directory/templates/template-list-supplier_barang.xlsx';
      $startAttributeRow = 5;
      $startDataRow = 6;
      $payload = array();
      $model = $this->SupplierbarangModel->getAll(array('supplier_id' => $supplierId));

      if (count($model) > 0) {
        $no = 1;
        $supplierName = isset($model[0]->nama_supplier) ? $model[0]->nama_supplier : '-';

        foreach ($model as $index => $item) {
          $payload[] = (object) array(
            'no' => $no++,
            'jenis' => $item->jenis,
            'kode_barang' => $item->kode_barang,
            'merk' => $item->merk,
            'nama_barang' => $item->nama_barang,
            'jumlah' => $item->quantity,
            'satuan' => $item->satuan,
            'harga_satuan' => $item->harga_satuan,
            'total_harga' => $item->total_harga
          );
        };

        $inject  = '$a3 = $sheet->getCell("A3")->getFormattedValue();';
        $inject .= '$a3 = str_replace(\'${supplier_name}\', "' . $supplierName . '", $a3);'; // Supplier Name
        $inject .= '$sheet->setCellValue("A3", strtoupper($a3));';

        $supplierNameEx = strtolower($supplierName);
        $supplierNameEx = preg_replace("![^a-z0-9]+!i", "-", $supplierNameEx);
        $outputFileName = 'barang_' . $supplierNameEx . '.xlsx';

        $this->generateExcelByTemplate($fileTemplate, $startAttributeRow, $startDataRow, $payload, $outputFileName, $inject);
      } else {
        echo 'Tidak ditemukan data.';
      };
    } catch (\Throwable $th) {
      echo 'Terjadi kesalahan ketika membuat file.';
    };
  }
  // END ## Barang
}
