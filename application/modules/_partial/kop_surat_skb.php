<table class="table-header">
  <tr>
    <td colspan="2">
      <table class="table-header">
        <td style="width: 80px;" valign="top">
          <img src="<?php echo base_url('themes/_public/img/logo/pt-arya-jaya.png') ?>" class="logo" />
        </td>
        <td style="padding-left: 20px; padding-right: 20px;" valign="top">
          <div class="kop-surat">
            <h5>PT. ARYA JAYA</h5>
            <span>Jl. Batununggal Indah Raya No.305, Kota Bandung - Jawa Barat, Indonesia</span> <br />
            <span>Phone : (022) 7322343</span> <br />
            <span>Email : sales@aryajaya.co.id, Web : www.aryajaya.co.id</span>
          </div>
        </td>
        <td style="width: 80px; text-align: right;" valign="top">
          <table style="width: 250px;">
            <tr>
              <td colspan="3" style="text-align: center; font-weight: bold; font-size: 16px; border: 2px solid #000; padding: 5px 10px;">
                SURAT KIRIM BARANG
              </td>
            </tr>
            <tr>
              <td colspan="3" style="height: 10px;"></td>
            </tr>
            <tr>
              <td style="font-size: 12px; width: 80px; text-align: left;" valign="top">Nomor</td>
              <td style="width: 15px; text-align: center;" valign="top">:</td>
              <td style="font-size: 12px; text-align: left; font-weight: 600;"><?= isset($skb->nomor) ? $skb->nomor : null ?></td>
            </tr>
            <tr>
              <td style="font-size: 12px; width: 80px; text-align: left;" valign="top">Tanggal</td>
              <td style="width: 15px; text-align: center;" valign="top">:</td>
              <td style="font-size: 12px; text-align: left; font-weight: 600;"><?= isset($skb->tanggal) ? $controller->localizeDate($skb->tanggal) : null ?></td>
            </tr>
          </table>
        </td>
      </table>
      <hr class="double-line">
    </td>
  </tr>
</table>