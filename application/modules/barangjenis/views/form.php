<div class="modal fade" id="modal-form-barangjenis" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title pull-left">
          <?= (isset($card_title)) ? $card_title : 'Form' ?>
        </h5>
      </div>
      <div class="spinner">
        <div class="lds-hourglass"></div>
      </div>
      <div class="modal-body">
        <form id="form-barangjenis" autocomplete="off">
          <!-- CSRF -->
          <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />

          <div class="form-group">
            <label required>Nama</label>

            <div class="input-group">
              <div class="form-control p-0">
                <div class="form-check m-2">
                  <input type="checkbox" name="is_parent_nama" class="form-check-input" id="is_parent_nama" value="1">
                  <label class="form-check-label mt-1" for="is_parent_nama">Existing</label>
                </div>
              </div>
              <!-- Manual -->
              <input type="text" name="nama_manual" class="form-control barangjenis-nama-manual" maxlength="150" placeholder="Nama" required style="width: 270px;" />
              <!-- Auto -->
              <select name="nama_auto" class="form-control barangjenis-nama-auto" style="width: 270px; display: none;">
                <?= $barangjenis_nama_list ?>
              </select>
              <!-- Hidden temp -->
              <input type="hidden" name="nama" class="barangjenis-nama" readonly />
            </div>
          </div>

          <div class="form-group">
            <label>Parameter</label>
            <input type="text" name="parameter" class="form-control barangjenis-parameter" maxlength="150" placeholder="Parameter" />
            <i class="form-group__bar"></i>
          </div>

          <small class="form-text text-muted">
            Fields with red stars (<label required></label>) are required.
          </small>

        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success btn--icon-text barangjenis-action-save">
          <i class="zmdi zmdi-save"></i> Simpan
        </button>
        <button type="button" class="btn btn-light btn--icon-text barangjenis-action-cancel" data-dismiss="modal">
          Batal
        </button>
      </div>
    </div>
  </div>
</div>