<?php require_once('main.css.php') ?>

<div class="preview-po" style="border: 0; padding: 0;">
  <div class="preview-header">
    <!-- Kop Surat -->
    <?php include_once(APPPATH . 'modules/_partial/kop_surat_skb.php') ?>
    <!-- END ## Kop Surat -->
  </div>
  <div class="preview-body">
    <table style="width: 100%; margin-bottom: 15px;">
      <tr>
        <td style="font-size: 12px; width: 60px;" valign="top">Kepada</td>
        <td style="font-size: 12px; width: 10px;" valign="top">:</td>
        <td style="font-size: 12px; font-weight: bold;"><?= isset($skb->customer_name) ? $skb->customer_name : null ?></td>
      </tr>
      <tr>
        <td style="font-size: 12px; width: 60px;" valign="top">Alamat</td>
        <td style="width: 10px;" valign="top">:</td>
        <td style="font-size: 12px;">
          <p class="m-0"><?= isset($skb->project_name) ? $skb->project_name : null ?></p>
          <p class="m-0"><?= isset($skb->customer_address) ? $skb->customer_address : null ?></p>
        </td>
      </tr>
      <tr>
        <td style="font-size: 12px; width: 60px;" valign="top">UP</td>
        <td style="width: 10px;" valign="top">:</td>
        <td style="font-size: 12px;"><?= isset($skb->customer_attn) ? $skb->customer_attn : null ?></td>
      </tr>
      <tr>
        <td colspan="3" class="p-0" style="height: 20px;"></td>
      </tr>
      <tr>
        <td colspan="3" style="font-size: 12px;">
          Bersama surat ini kami kirimkan :
        </td>
      </tr>
    </table>

    <table class="table-order-item" style="border-collapse: collapse;">
      <thead style="text-align: center;">
        <tr>
          <th width="60">No</th>
          <th>Description</th>
          <th colspan="2">Qty</th>
          <th>Note</th>
        </tr>
      </thead>
      <tbody>
        <?php
        $no = 1;
        ?>
        <?php if (isset($skb_item) && count($skb_item) > 0) : ?>
          <?php foreach ($skb_item as $key => $item) : ?>
            <?php
            $isPreferences = '';
            $isPreferences .= ($item->is_bold == 1) ? 'font-weight: 600;' : '';
            $isPreferences .= ($item->is_italic == 1) ? 'font-style: italic;' : '';
            ?>
            <tr class="item-row-wrapper item-row-wrapper-<?= $item->id ?>" style="<?= $isPreferences ?>">
              <td align="center" style="height: 30.48px;"><?= $item->nomor ?></td>
              <td><?= $item->description ?></td>
              <td class="text-center" width="70"><?= (!is_null($item->quantity)) ? number_format($item->quantity, 0) : null ?></td>
              <td class="text-center" width="70"><?= $item->unit ?></td>
              <td><?= $item->note ?></td>
            </tr>
          <?php endforeach; ?>
        <?php else : ?>
          <tr>
            <td colspan="5" style="padding: 15px;">
              No data available in table
            </td>
          </tr>
        <?php endif; ?>
      </tbody>
    </table>
    <div style="margin-bottom: 15px;"></div>

    <?php if (!is_null($skb->note) && !empty($skb->note)) : ?>
      <span style="font-size: 12px;">Notes :</span>
      <div style="font-size: 12px;"><?= (isset($skb->note)) ? str_replace("\r\n", '<br/>', htmlspecialchars_decode($skb->note)) : null ?></div>
    <?php endif ?>

    <div style="margin-bottom: 15px;"></div>
    <div style="font-size: 12px;">
      Atas perhatiannya kami ucapkan terima kasih.
    </div>
    <div style="margin-bottom: 20px;"></div>

    <hr class="double-line">

    <table style="width: 100%">
      <tr style="text-align: center;">
        <td width="50">
          <span style="font-size: 12px;">Diserahkan Oleh</span>
          <br><br><br><br><br>
          (&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)
        </td>
        <td width="50">
          <span style="font-size: 12px;">Diterima Oleh</span>
          <br><br><br><br><br>
          (&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)
        </td>
      </tr>
    </table>
  </div>
</div>