<script type="text/javascript">
  $(document).ready(function() {
    var _key = "";
    var _section = "skb";
    var _table_list = "table-list";
    var _table_item_list = "table-skb_item";
    var _form_skb = "form-skb";
    var _skbId = "<?= $skb_id ?>";
    var _modal_upload = "modal-form-upload";
    var _form_upload = "form-skb-upload";
    var _modal_approval = "modal-form-approval";
    var _form_approval = "form-skb-approval";
    var _modal_view = "modal-skb-view";
    var _modal_view_riwayat = "modal-skb-view-riwayat";
    var _module = "<?= $module ?>";
    var _modal_source = "";
    var _listFilter = "<?= (!is_null($this->input->get('ref'))) ? $this->input->get('ref') : null ?>";

    // Initialize DataTables : List
    if ($("#" + _table_list)[0]) {
      var table_skb = $("#" + _table_list).DataTable({
        processing: true,
        serverSide: true,
        ajax: {
          url: "<?php echo base_url('skb/ajax_get_list/?src=') ?>" + _module,
          type: "get"
        },
        columns: [{
            data: null,
            render: function(data, type, row, meta) {
              return meta.row + meta.settings._iDisplayStart + 1;
            }
          },
          {
            data: "nomor"
          },
          {
            data: "tanggal"
          },
          {
            data: "project_name"
          },
          {
            data: "customer_name"
          },
          {
            data: "status",
            render: function(data, type, row, meta) {
              var statusName = getStatus(data);
              var badgeType = "info";
              var revisi = (row.revisi_nomor != null && row.revisi_nomor != "") ? " (Rev. " + row.revisi_nomor + ")" : "";

              return '<span class="badge badge-' + badgeType + '">' + statusName + revisi + '</span>';
            }
          },
          {
            data: "created_at"
          },
          {
            data: null,
            className: "text-center",
            render: function(data, type, row, meta) {
              var htmlDom = "-";

              htmlDom = '<div class="action">';
              htmlDom += '<a href="javascript:;" class="action-view btn btn-light btn-sm mr-1" data-toggle="modal" data-target="#' + _modal_view + '"><i class="zmdi zmdi-eye"></i></a>&nbsp;';

              if (row.status == "0") {
                // Draft
                htmlDom += '<a href="<?= base_url('skb/wizard/') ?>' + row.id + '/1" class="action-edit btn btn-dark btn-sm mr-1"><i class="zmdi zmdi-edit"></i></a>';
              };

              htmlDom += '</div>';

              return htmlDom;
            }
          }
        ],
        autoWidth: !1,
        responsive: {
          details: {
            // display: $.fn.dataTable.Responsive.display.modal(),
            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
              tableClass: "table dt-details"
            }),
            type: "inline",
            target: 'tr',
          }
        },
        columnDefs: [{
          className: 'desktop',
          targets: [0, 1, 2, 3, 4, 5, 6, 7]
        }, {
          className: 'tablet',
          targets: [0, 1, 2]
        }, {
          className: 'mobile',
          targets: [0, 3]
        }, {
          responsivePriority: 1,
          targets: 0
        }, {
          responsivePriority: 2,
          targets: -1
        }],
        pageLength: 15,
        language: {
          searchPlaceholder: "Search...",
          sProcessing: '<div style="text-align: center;"><div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div></div>'
        },
        sDom: '<"dataTables_ct"><"dataTables__top"fb>rt<"dataTables__bottom"ip><"clear">',
        buttons: [{
          extend: "excelHtml5",
          title: "Export Result"
        }, {
          extend: "print",
          title: "Export Result"
        }],
        search: {
          search: _listFilter
        },
        initComplete: function(a, b) {
          $(this).closest(".dataTables_wrapper").find(".dataTables__top").prepend(
            '<div class="dataTables_buttons hidden-sm-down actions">' +
            '<span class="actions__item zmdi zmdi-refresh" data-table-action="reload" title="Reload" />' +
            '</div>'
          );
        },
      });

      $(".dataTables_filter input[type=search]").focus(function() {
        $(this).closest(".dataTables_filter").addClass("dataTables_filter--toggled")
      });

      $(".dataTables_filter input[type=search]").blur(function() {
        $(this).closest(".dataTables_filter").removeClass("dataTables_filter--toggled")
      });

      $("body").on("click", "[data-table-action]", function(a) {
        a.preventDefault();
        var b = $(this).data("table-action");
        if ("reload" === b) {
          $("#" + _table_list).DataTable().ajax.reload(null, false);
        };
      });
    };

    // skb ITEM
    $(document).on("click", "a.item-form-input-cancel", function(e) {
      e.preventDefault();
      var addButton = $(".item-button-row");
      var form = $(".item-form-row");
      var formHeader = $(".header-form-row");
      var rowWrapper = $(".item-row-wrapper");

      addButton.fadeIn(100);
      form.fadeOut(100);
      formHeader.fadeIn(100);
      rowWrapper.removeClass("bg-green text-white");
    });

    $(document).on("click", "a.item-form-input-save", function(e) {
      e.preventDefault();
      var ref = $(this).attr("data-id");
      var addButton = $(".item-button-row");
      var form = $(".item-form-row-" + ref);
      var formHeader = $(".header-form-row");
      var formData = $("#item-form-input-" + ref);
      var rowWrapper = $(".item-row-wrapper-" + ref);

      adjustSpinner(ref);
      try {
        $.ajax({
          type: "post",
          url: "<?php echo base_url('skb/ajax_save_item/') ?>" + _key,
          data: formData.serialize(),
          success: function(response) {
            var response = JSON.parse(response);

            if (response.status === true) {
              fetchDataSkbItem().then(() => {
                addButton.fadeIn(100);
                form.fadeOut(100);
                formHeader.fadeIn(100);
                rowWrapper.removeClass("bg-green text-white");
              });

              notify(response.data, "success");
            } else {
              notify(response.data, "danger");
            };
          }
        });
      } catch (error) {
        notify("Failed to save your data.", "danger");
      };
    });

    $(document).on("click", "a.item-button-row-edit", function(e) {
      e.preventDefault();
      var ref = $(this).attr("data-id");
      var skb_item_parent_id = $(this).attr("data-skb_item_parent_id");
      var nomor = $(this).attr("data-nomor");
      var description = $(this).attr("data-description");
      var note = $(this).attr("data-note");
      var quantity = $(this).attr("data-quantity");
      var unit = $(this).attr("data-unit");
      var is_bold = $(this).attr("data-is_bold");
      var is_italic = $(this).attr("data-is_italic");

      var addButton = $(".item-button-row");
      var form = $(".item-form-row-" + ref);
      var formIcon = $(".item-form-row-icon-" + ref);
      var formHeader = $(".header-form-row");
      var formInput = "#item-form-input-" + ref;
      var rowWrapper = $(".item-row-wrapper-" + ref);
      var wrapperParam = $(".wrapper-params-" + ref);
      var checkboxAsBarang = $(".description-as_barang-" + ref);

      formIcon.html('E');
      addButton.fadeOut(100);
      form.fadeIn(100);
      formHeader.fadeOut(100);
      rowWrapper.addClass("bg-green text-white");

      wrapperParam.hide();
      $(".skb_item-description_manual-" + ref).show();
      $(".skb_item-description_auto-" + ref).select2().next().hide();
      checkboxAsBarang.attr("disabled", true);

      // Fill input value
      _key = ref;
      $(formInput).trigger("reset");
      $(formInput + " .skb_item-skb_item_parent_id-" + ref).val(skb_item_parent_id).trigger("input");
      $(formInput + " .skb_item-nomor-" + ref).val(nomor).trigger("input");
      $(formInput + " .skb_item-description-" + ref).val(description).trigger("input");
      $(formInput + " .skb_item-description_manual-" + ref).val(description).trigger("input");
      $(formInput + " .skb_item-note-" + ref).val(note).trigger("input");
      $(formInput + " .skb_item-quantity-" + ref).val(quantity).trigger("input");
      $(formInput + " .skb_item-unit-" + ref).val(unit).trigger("input");

      if (is_bold == 1) {
        $(formInput + " .skb_item-is_bold-" + ref).attr("checked", true);
      } else {
        $(formInput + " .skb_item-is_bold-" + ref).removeAttr("checked");
      };
      if (is_italic == 1) {
        $(formInput + " .skb_item-is_italic-" + ref).attr("checked", true);
      } else {
        $(formInput + " .skb_item-is_italic-" + ref).removeAttr("checked");
      };

      $('.mask-decimal').mask('#,##0.00', {
        reverse: true
      });
      $('.mask-money').mask('#,##0', {
        reverse: true
      });
    });

    async function fetchDataSkbItem() {
      var skbId = "<?= $skb_id ?>";

      await $.ajax({
        type: "get",
        url: "<?php echo base_url('skb/ajax_get_skb_item_table/') ?>" + skbId,
        success: function(response) {
          $("#table-skb-item").html(response);
        }
      });
    };

    function adjustSpinner(ref) {
      $(".spinner-" + ref).css("width", $(".item-form-input-wrapper-" + ref).width() + 27);
      $(".spinner-" + ref).css("height", $(".item-form-input-wrapper-" + ref).height() + 27);
      $(".spinner-" + ref).css("margin-top", "-13px");
      $(".spinner-" + ref).css("margin-left", "-13px");
    };

    function adjustSpinnerHeader() {
      $(".spinner-header").css("width", $(".header-form-input-wrapper").width());
      $(".spinner-header").css("height", $(".header-form-input-wrapper").height());
      $(".spinner-header").css("margin-top", "-13px");
      $(".spinner-header").css("margin-left", "-13px");
    };
    // END ## skb ITEM

    // Handle data submit
    $("#" + _form_skb + " button.skb-action-save").on("click", function(e) {
      e.preventDefault();

      $.ajax({
        type: "post",
        url: "<?php echo base_url('skb/ajax_save/') ?>" + _skbId,
        data: $("#" + _form_skb).serialize(),
        success: function(response) {
          var response = JSON.parse(response);
          if (response.status === true) {
            notify(response.data, "success");
            location.href = "<?= base_url('skb/wizard/') ?>" + response.data_id + "/2";
          } else {
            notify(response.data, "danger");
          };
        }
      });
    });

    // Handle data delete
    $("#" + _table_list).on("click", "a.action-delete", function(e) {
      e.preventDefault();
      var temp = table_skb.row($(this).closest('tr')).data();

      swal({
        title: "Are you sure to delete?",
        text: "Once deleted, you will not be able to recover this data!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false
      }).then((result) => {
        if (result.value) {
          $.ajax({
            type: "delete",
            url: "<?php echo base_url('skb/ajax_delete/') ?>" + temp.id,
            dataType: "json",
            success: function(response) {
              if (response.status) {
                $("#" + _table_list).DataTable().ajax.reload(null, false);
                notify(response.data, "success");
              } else {
                notify(response.data, "danger");
              };
            }
          });
        };
      });
    });

    // Handle view
    $("#" + _table_list).on("click", "a.action-view", function(e) {
      e.preventDefault();
      var temp = table_skb.row($(this).closest('tr')).data();

      $("#" + _modal_view + " .approval-preview").html("Please wait...");

      $.ajax({
        type: "get",
        url: "<?= base_url('skb/ajax_get_preview/') ?>" + temp.id,
        success: function(response) {
          $(".approval-preview").html(response);
          $(".modal-skb-view-title").html("Surat Kirim Barang : " + getStatus(temp.status));
        }
      });

      _modal_source = "view";
    });

    // Handle submit / publish
    $(document).on("click", "button.skb-action-sent", function(e) {
      e.preventDefault();
      var ref = $(this).attr("data-id");

      swal({
        title: "Are you sure to submit?",
        text: "Once submited, you will not be able to recover this data!",
        type: "question",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false
      }).then((result) => {
        if (result.value) {
          $.ajax({
            type: "get",
            url: "<?php echo base_url('skb/ajax_set_status/') ?>",
            data: {
              skb_id: ref,
              status: 1,
            },
            dataType: "json",
            success: function(response) {
              if (response.status) {
                notify(response.data, "success");

                setTimeout(function() {
                  location.href = "<?= base_url('skb') ?>";
                }, 2000);
              } else {
                notify(response.data, "danger");
              };
            }
          });
        };
      });
    });

    // Handle skb print
    $(document).on("click", "button.action-skb-print", function(e) {
      e.preventDefault();
      $(".skb-print-area").printThis();
    });
  });

  function getStatus(status) {
    switch (status.toString()) {
      case '0':
        return 'Draft';
        break;
      case '1':
        return 'Submit';
        break;
      default:
        return 'Undefined';
        break;
    };
  };
</script>