<?php require_once('main.css.php') ?>
<?php $revisiNomor = null ?>
<?php $revisiNomorLabel = null ?>

<section id="suratjalan">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <span class="badge badge-info" style="position: absolute; margin-top: 2px;">
                        <?php if (isset($is_revisi) && $is_revisi == true) : ?>
                            <?php $revisiNomor = (!is_null($skb->revisi_nomor)) ? (int) $skb->revisi_nomor + 1 : 1; ?>
                            REV
                        <?php else : ?>
                            <?php $revisiNomorLabel = (isset($skb->revisi_nomor) && !is_null($skb->revisi_nomor)) ? ' : Rev. ' . $skb->revisi_nomor : null; ?>
                            <?= (isset($skb->id)) ? 'EDIT' : 'NEW' ?>
                        <?php endif ?>
                    </span>
                    <div style="margin-left: 50px;">
                        <h4 class="card-title"><?php echo (isset($card_title)) ? $card_title . $revisiNomorLabel : '' ?></h4>
                        <h6 class="card-subtitle"><?php echo (isset($card_subTitle)) ? $card_subTitle : '' ?></h6>
                    </div>
                    <div class="clear-card"></div>
                </div>
            </div>
            <div class="clear-card"></div>

            <form id="form-skb" enctype="multipart/form-data" autocomplete="off">
                <input type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" value="<?= $this->security->get_csrf_hash(); ?>" />

                <!-- Temporary hidden field -->
                <?php if (isset($is_revisi) && $is_revisi == true) : ?>
                    <input type="hidden" name="is_revisi" value="true" readonly />
                    <input type="hidden" name="revisi_nomor" value="<?= $revisiNomor ?>" readonly />

                    <div class="alert alert-secondary mb-4">
                        <i class="zmdi zmdi-info"></i>
                        <b>Revisi <?= $revisiNomor ?></b>, silahkan masukan nomor baru! (Prev: <?= $skb->nomor ?>)
                    </div>
                <?php endif ?>

                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label required>Nomor</label>
                            <input type="text" name="nomor" class="form-control skb-nomor" placeholder="Nomor" value="<?= (isset($skb->nomor)) ? $skb->nomor : $generate_nomor ?>" readonly required />
                            <i class="form-group__bar"></i>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label required>Date</label>
                            <input type="text" name="tanggal" class="form-control skb-tanggal flatpickr-date bg-white" placeholder="Date" value="<?= (isset($skb->tanggal)) ? $skb->tanggal : null ?>" required />
                            <i class=" form-group__bar"></i>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label required>Project Name</label>
                    <input type="text" name="project_name" class="form-control skb-project_name" placeholder="Project Name" value="<?= (isset($skb->project_name)) ? $skb->project_name : null ?>" required />
                    <i class="form-group__bar"></i>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label required>Customer Name</label>
                            <input type="text" name="customer_name" class="form-control skb-customer_name" placeholder="Customer Name" value="<?= (isset($skb->customer_name)) ? $skb->customer_name : null ?>" required />
                            <i class="form-group__bar"></i>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="form-group">
                            <label>Customer Attn</label>
                            <input type="text" name="customer_attn" class="form-control skb-customer_attn" placeholder="Customer Atn" value="<?= (isset($skb->customer_attn)) ? $skb->customer_attn : null ?>" />
                            <i class="form-group__bar"></i>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label>Customer Address</label>
                    <input type="text" name="customer_address" class="form-control skb-customer_address" placeholder="Customer Address" value="<?= (isset($skb->customer_address)) ? $skb->customer_address : null ?>" />
                    <i class="form-group__bar"></i>
                </div>
                <div class="form-group">
                    <label>Note</label>
                    <textarea name="note" class="form-control textarea-autosize text-counter skb-note" rows="1" data-max-length="1000" placeholder="Note" style="overflow: hidden; overflow-wrap: break-word; height: 31px;"><?= (isset($skb->note)) ? $skb->note : null ?></textarea>
                    <i class="form-group__bar"></i>
                </div>

                <small class="form-text text-muted">
                    Fields with red stars (<label required></label>) are required.
                </small>

                <div class="row">
                    <div class="col">
                        <div class="buttons-container">
                            <div class="row">
                                <div class="col">
                                    <a href="<?= base_url('skb') ?>" class="btn btn-dark">Cancel</a>
                                </div>
                                <div class="col text-right">
                                    <button class="btn btn--raised btn-primary btn--icon-text btn-custom skb-action-save spinner-action-button">
                                        Next
                                        <i class="zmdi zmdi-long-arrow-right"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>
</section>