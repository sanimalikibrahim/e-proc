<div class="table-responsive">
  <table class="table table-sm table-bordered table-hover">
    <thead style="text-align: center;">
      <tr>
        <th width="80">No</th>
        <th>Description</th>
        <th colspan="2">Qty</th>
        <th>Note</th>
        <th width="60" class="text-center">#</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td colspan="8" class="p-0" style="height: 5px;"></td>
      </tr>
      <?php if (count($skb_item) > 0) : ?>
        <?php foreach ($skb_item as $index => $item) : ?>
          <?php
          $isPreferences = '';
          $isPreferences .= ($item->is_bold == 1) ? 'font-weight: 600;' : '';
          $isPreferences .= ($item->is_italic == 1) ? 'font-style: italic;' : '';
          ?>
          <div class="item-rows item-rows-<?= $item->id ?>">
            <!-- Row data -->
            <tr class="item-row-wrapper item-row-wrapper-<?= $item->id ?>" style="<?= $isPreferences ?>">
              <td align="center" style="height: 30.48px;">
                <?= $item->nomor ?>
              </td>
              <td>
                <?= $item->description ?>
              </td>
              <td class="text-center" width="70"><?= (!is_null($item->quantity)) ? number_format($item->quantity, 0) : null ?></td>
              <td class="text-center" width="70"><?= $item->unit ?></td>
              <td><?= $item->note ?></td>
              <td align="center">
                <a href="javascript:;" data-id="<?= $item->id ?>" data-skb_item_parent_id="<?= $item->skb_item_parent_id ?>" data-nomor="<?= $item->nomor ?>" data-description="<?= $item->description ?>" data-note="<?= $item->note ?>" data-quantity="<?= $item->quantity ?>" data-unit="<?= $item->unit ?>" data-is_bold="<?= $item->is_bold ?>" data-is_italic="<?= $item->is_italic ?>" class="btn btn-dark btn-xs item-button-row item-button-row-edit item-button-row-edit-<?= $item->id ?>" title="Edit">
                  <i class="zmdi zmdi-edit"></i>
                </a>
              </td>
            </tr>
            <!-- END ## Row data -->
            <!-- Item inline form -->
            <tr class="item-form-row item-form-row-<?= $item->id ?> bg-light" style="display: none;">
              <td align="center">
                <span class="badge badge-warning item-form-row-icon-<?= $item->id ?>">N</span>
              </td>
              <td colspan="7" class="pl-0">
                <div class="item-form-input item-form-input-wrapper-<?= $item->id ?> bg-white p-3">
                  <!-- Loader -->
                  <div class="spinner spinner-<?= $item->id ?>">
                    <div class="lds-hourglass"></div>
                  </div>
                  <form method="post" id="item-form-input-<?= $item->id ?>">
                    <!-- Temporary hidden field -->
                    <input type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" value="<?= $this->security->get_csrf_hash(); ?>" />
                    <input type="hidden" name="skb_id" value="<?= $skb->id ?>" readonly />
                    <input type="hidden" name="skb_item_parent_id" class="skb_item-skb_item_parent_id-<?= $item->id ?>" value="<?= $item->id ?>" readonly />

                    <div class="row">
                      <div class="col-xs-12 col-md-12">
                        <div class="row">
                          <div class="col-xs-12 col-md-2">
                            <div class="form-group mb-3">
                              <label>Item</label>
                              <input type="text" name="nomor" class="form-control skb_item-nomor skb_item-nomor-<?= $item->id ?>" placeholder="Item" />
                              <i class="form-group__bar"></i>
                            </div>
                          </div>
                          <div class="col-xs-12 col-md-10">
                            <label>Preferences</label>
                            <div class="group-control mb-3">
                              <div class="row">
                                <div class="col-xs-12 col-md-1">
                                  <div class="form-check m-0">
                                    <input type="checkbox" name="is_bold" class="form-check-input skb_item-is_bold-<?= $item->id ?>" id="pref-bold-<?= $item->id ?>" value="1">
                                    <label class="form-check-label" for="pref-bold-<?= $item->id ?>">Bold</label>
                                  </div>
                                </div>
                                <div class="col-xs-12 col-md-1">
                                  <div class="form-check m-0">
                                    <input type="checkbox" name="is_italic" class="form-check-input skb_item-is_italic-<?= $item->id ?>" id="pref-italic-<?= $item->id ?>" value="1">
                                    <label class="form-check-label" for="pref-italic-<?= $item->id ?>">Italic</label>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-xs-12 col-md-12">
                          <div class="row">
                            <div class="col-xs-12 col-md-8">
                              <div class="form-group mb-3">
                                <label>Description</label>
                                <input type="text" name="description" class="form-control skb_item-description skb_item-description-<?= $item->id ?>" data-id="<?= $item->id ?>" placeholder="Description" />
                                <i class="form-group__bar"></i>
                              </div>
                            </div>
                            <div class="col-xs-12 col-md-4">
                              <div class="form-group mb-3">
                                <label>Note</label>
                                <input type="text" name="note" class="form-control skb_item-note skb_item-note-<?= $item->id ?>" placeholder="Note" />
                                <i class="form-group__bar"></i>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-xs-12 col-md-2">
                            <div class="form-group mb-3">
                              <label>Quantity</label>
                              <input type="number" name="quantity" class="form-control skb_item-quantity skb_item-quantity-<?= $item->id ?>" data-id="<?= $item->id ?>" placeholder="Quantity" min="1" max="<?= $item->quantity ?>" />
                              <i class="form-group__bar"></i>
                            </div>
                          </div>
                          <div class="col-xs-12 col-md-2">
                            <div class="form-group mb-3">
                              <label>Unit</label>
                              <input type="text" name="unit" class="form-control skb_item-unit skb_item-unit-<?= $item->id ?>" placeholder="Unit" />
                              <i class="form-group__bar"></i>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-xs-12 col-md-12">
                        <a href="javascript:;" class="btn btn-secondary item-form-input-cancel">Cancel</a>
                        <a href="javascript:;" class="btn btn-success item-form-input-save" data-id="<?= $item->id ?>"><i class="zmdi zmdi-save"></i> Save</a>
                      </div>
                    </div>
                  </form>
                </div>
              </td>
            </tr>
            <!-- END ## Item inline form -->
          </div>
        <?php endforeach ?>
      <?php endif ?>
      </td>
      </tr>
</div>
<!-- END ## Header inline form -->
</tbody>
</table>
</div>