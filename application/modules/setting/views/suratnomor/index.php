<div class="body-loading">
    <div class="body-loading-content">
        <div class="card">
            <div class="card-body">
                <i class="zmdi zmdi-spinner zmdi-hc-spin"></i>
                Please wait...
                <div class="mb-2"></div>
                <span style="color: #9c9c9c; font-size: 1rem;">Don't close this tab activity!</span>
            </div>
        </div>
    </div>
</div>

<section id="setting">
    <div class="card">
        <div class="card-body">

            <div class="row">
                <div class="col-xs-10 col-md-10">
                    <h4 class="card-title"><?php echo (isset($card_title)) ? $card_title : '' ?></h4>
                    <h6 class="card-subtitle"><?php echo (isset($card_subTitle)) ? $card_subTitle : '' ?></h6>
                    <div class="clear-card"></div>
                </div>
            </div>
            <div class="clear-card"></div>

            <form id="form-suratnomor" enctype="multipart/form-data" autocomplete="off">
                <!-- CSRF -->
                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />

                <div class="row">
                    <div class="col-xs-10 col-md-6">
                        <div class="mb-3" style="border: 1px solid #c5ccd6; padding: 8px 10px;">
                            Parameter :
                            <table class="table table-sm table-bordered mt-3 mb-0">
                                <tr>
                                    <td>
                                        <b class="text-primary">{INC}</b>
                                        <p class="m-0">
                                            Auto increment, nomor akan ditambah 1 mengikuti format yang sama.<br />
                                            <small class="text-muted">Contoh: 0001, 0002, etc...</small>
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b class="text-primary">{MONTH}</b>
                                        <p class="m-0">
                                            Menampilkan urutan bulan dalam format romawi. <br />
                                            <small class="text-muted">Contoh: I, II, III, etc...</small>
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b class="text-primary">{MONTH_NUM}</b>
                                        <p class="m-0">
                                            Menampilkan urutan bulan dalam format nomor. <br />
                                            <small class="text-muted">Contoh: 1, 2, 3, etc...</small>
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b class="text-primary">{YEAR}</b>
                                        <p class="m-0">
                                            Menampilkan tahun dalam 4 digit. <br />
                                            <small class="text-muted">Contoh: <?= date('Y') ?></small>
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b class="text-primary">{YEAR_2}</b>
                                        <p class="m-0">
                                            Menampilkan tahun dalam 2 digit. <br />
                                            <small class="text-muted">Contoh: <?= date('y') ?></small>
                                        </p>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="col-xs-10 col-md-6">
                        <div class="form-group">
                            <label required>RAB</label>
                            <input type="text" name="rab" class="form-control setting-rab" placeholder="Format Nomor" value="<?php echo (!is_null($nomor_rab)) ? $nomor_rab->format_nomor : '' ?>" />
                            <i class="form-group__bar"></i>
                        </div>

                        <small class="form-text text-muted">
                            Fields with red stars (<label required></label>) are required.
                        </small>

                        <div class="mt-3">
                            <button class="btn btn--raised btn-primary btn--icon-text btn-block page-action-save spinner-action-button">
                                Simpan Perubahan
                                <div class="spinner-action"></div>
                            </button>
                        </div>
                    </div>
                </div>
            </form>

        </div>
    </div>
</section>