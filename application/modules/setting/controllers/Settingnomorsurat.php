<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . 'controllers/AppBackend.php');

class Settingnomorsurat extends AppBackend
{
  function __construct()
  {
    parent::__construct();
    $this->load->model(array(
      'SettingAppModel',
      'SuratnomorModel',
    ));
  }

  public function index()
  {
    $unit = $this->session->userdata('user')['unit'];
    $subUnit = $this->session->userdata('user')['sub_unit'];

    $nomorRab = $this->SuratnomorModel->getDetail(array(
      'LOWER(unit)' => strtolower($unit),
      'LOWER(sub_unit)' => strtolower($subUnit),
      'ref' => 'rab'
    ));
    $nomorQuotation = $this->SuratnomorModel->getDetail(array(
      'LOWER(unit)' => strtolower($unit),
      'LOWER(sub_unit)' => strtolower($subUnit),
      'ref' => 'quotation'
    ));

    $data = array(
      'app' => $this->app(),
      'main_js' => $this->load_main_js('setting/views/suratnomor/main.js.php', true),
      'card_title' => 'Pengaturan › Nomor Surat',
      'nomor_rab' => $nomorRab,
      'nomor_quotation' => $nomorQuotation,
    );
    $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
    $this->template->load_view('suratnomor/index', $data, TRUE);
    $this->template->render();
  }

  public function ajax_save()
  {
    $this->handle_ajax_request();
    $this->SuratnomorModel->setFormatByRef('rab', $this->input->post('rab'));
    $this->SuratnomorModel->setFormatByRef('quotation', $this->input->post('quotation'));

    echo json_encode(array('status' => true, 'data' => 'Data has been saved.'));
  }
}
