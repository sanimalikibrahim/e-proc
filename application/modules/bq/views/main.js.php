<script type="text/javascript">
  $(document).ready(function() {
    var _key = "";
    var _section = "bq";
    var _table_list = "table-list";
    var _table_item_list = "table-bq_item";
    var _form_bq = "form-bq";
    var _bqId = "<?= $bq_id ?>";
    var _modal_view = "modal-bq-view";
    var _module = "<?= $module ?>";
    var _modal_source = "";
    var _listFilter = "<?= (!is_null($this->input->get('ref'))) ? $this->input->get('ref') : null ?>";
    var _role = "<?= $this->session->userdata('user')['role'] ?>";

    // Initialize DataTables : List
    if ($("#" + _table_list)[0]) {
      var table_bq = $("#" + _table_list).DataTable({
        processing: true,
        serverSide: true,
        ajax: {
          url: "<?php echo base_url('bq/ajax_get_list/?src=') ?>" + _module,
          type: "get"
        },
        columns: [{
            data: null,
            render: function(data, type, row, meta) {
              return meta.row + meta.settings._iDisplayStart + 1;
            }
          },
          {
            data: "nomor"
          },
          {
            data: "tanggal"
          },
          {
            data: "project_name"
          },
          {
            data: "status",
            render: function(data, type, row, meta) {
              var statusName = getStatus(data);
              var badgeType = "info";
              var revisi = (row.revisi_nomor != null && row.revisi_nomor != "") ? " (Rev. " + row.revisi_nomor + ")" : "";

              if (row.is_created_quotation == 1) {
                statusName = "Quotation Sudah Dibuat";
              };

              return '<span class="badge badge-' + badgeType + '">' + statusName + revisi + '</span>';
            }
          },
          {
            data: "created_at"
          },
          {
            data: null,
            className: "text-center",
            render: function(data, type, row, meta) {
              var htmlDom = "-";

              htmlDom = '<div class="action">';
              htmlDom += '<a href="javascript:;" class="action-view btn btn-light btn-sm mr-1" data-toggle="modal" data-target="#' + _modal_view + '"><i class="zmdi zmdi-eye"></i></a>&nbsp;';

              if (row.status == "0") {
                // Draft
                htmlDom += '<a href="<?= base_url('bq/edit/') ?>' + row.id + '" class="action-edit btn btn-dark btn-sm mr-1"><i class="zmdi zmdi-edit"></i></a>&nbsp;';
              };

              if (row.status == "1" && row.is_created_quotation == 0 && _role.toLowerCase() == "sales") {
                htmlDom += `<a href="<?= base_url('bq/generate_quotation/') ?>${row.id}" class="action-generate_quotation btn btn-success btn-sm">Buat Quotation</a>`;
              };

              htmlDom += '</div>';

              return htmlDom;
            }
          },
        ],
        autoWidth: !1,
        responsive: {
          details: {
            // display: $.fn.dataTable.Responsive.display.modal(),
            renderer: $.fn.dataTable.Responsive.renderer.tableAll({
              tableClass: "table dt-details"
            }),
            type: "inline",
            target: 'tr',
          }
        },
        columnDefs: [{
          className: 'desktop',
          targets: [0, 1, 2, 3, 4, 5, 6]
        }, {
          className: 'tablet',
          targets: [0, 1, 2]
        }, {
          className: 'mobile',
          targets: [0, 3]
        }, {
          responsivePriority: 1,
          targets: 0
        }, {
          responsivePriority: 2,
          targets: -1
        }],
        pageLength: 15,
        language: {
          searchPlaceholder: "Search...",
          sProcessing: '<div style="text-align: center;"><div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div></div>'
        },
        sDom: '<"dataTables_ct"><"dataTables__top"fb>rt<"dataTables__bottom"ip><"clear">',
        buttons: [{
          extend: "excelHtml5",
          title: "Export Result"
        }, {
          extend: "print",
          title: "Export Result"
        }],
        search: {
          search: _listFilter
        },
        initComplete: function(a, b) {
          $(this).closest(".dataTables_wrapper").find(".dataTables__top").prepend(
            '<div class="dataTables_buttons hidden-sm-down actions">' +
            '<span class="actions__item zmdi zmdi-refresh" data-table-action="reload" title="Reload" />' +
            '</div>'
          );
        },
      });

      $(".dataTables_filter input[type=search]").focus(function() {
        $(this).closest(".dataTables_filter").addClass("dataTables_filter--toggled")
      });

      $(".dataTables_filter input[type=search]").blur(function() {
        $(this).closest(".dataTables_filter").removeClass("dataTables_filter--toggled")
      });

      $("body").on("click", "[data-table-action]", function(a) {
        a.preventDefault();
        var b = $(this).data("table-action");
        if ("reload" === b) {
          $("#" + _table_list).DataTable().ajax.reload(null, false);
        };
      });
    };

    // BQ ITEM
    $(document).on("click", "a.item-form-input-cancel", function(e) {
      e.preventDefault();
      var addButton = $(".item-button-row");
      var form = $(".item-form-row");
      var formHeader = $(".header-form-row");
      var rowWrapper = $(".item-row-wrapper");

      addButton.fadeIn(100);
      form.fadeOut(100);
      formHeader.fadeIn(100);
      rowWrapper.removeClass("bg-green text-white");
    });

    $(document).on("click", "a.item-form-input-save", function(e) {
      e.preventDefault();
      var ref = $(this).attr("data-id");
      var addButton = $(".item-button-row");
      var form = $(".item-form-row-" + ref);
      var formHeader = $(".header-form-row");
      var formData = $("#item-form-input-" + ref);
      var rowWrapper = $(".item-row-wrapper-" + ref);

      adjustSpinner(ref);
      try {
        $.ajax({
          type: "post",
          url: "<?php echo base_url('bq/ajax_save_item/') ?>" + _key,
          data: formData.serialize(),
          success: function(response) {
            var response = JSON.parse(response);

            if (response.status === true) {
              fetchDataBqItem().then(() => {
                addButton.fadeIn(100);
                form.fadeOut(100);
                formHeader.fadeIn(100);
                rowWrapper.removeClass("bg-green text-white");
              });

              notify(response.data, "success");
            } else {
              notify(response.data, "danger");
            };
          }
        });
      } catch (error) {
        notify("Failed to save your data.", "danger");
      };
    });

    $(document).on("click", "a.item-button-row-edit", function(e) {
      e.preventDefault();
      var ref = $(this).attr("data-id");
      var bq_item_parent_id = $(this).attr("data-bq_item_parent_id");
      var nomor = $(this).attr("data-nomor");
      var description = $(this).attr("data-description");
      var note = $(this).attr("data-note");
      var quantity = $(this).attr("data-quantity");
      var unit = $(this).attr("data-unit");
      var material_unit_price = $(this).attr("data-material_unit_price");
      material_unit_price = (material_unit_price == 0) ? null : material_unit_price;
      var material_total_price = $(this).attr("data-material_total_price");
      material_total_price = (material_total_price == 0) ? null : material_total_price;
      var labour_unit_price = $(this).attr("data-labour_unit_price");
      labour_unit_price = (labour_unit_price == 0) ? null : labour_unit_price;
      var labour_total_price = $(this).attr("data-labour_total_price");
      labour_total_price = (labour_total_price == 0) ? null : labour_total_price;
      var is_bold = $(this).attr("data-is_bold");
      var is_italic = $(this).attr("data-is_italic");

      var addButton = $(".item-button-row");
      var form = $(".item-form-row-" + ref);
      var formIcon = $(".item-form-row-icon-" + ref);
      var formHeader = $(".header-form-row");
      var formInput = "#item-form-input-" + ref;
      var rowWrapper = $(".item-row-wrapper-" + ref);
      var wrapperParam = $(".wrapper-params-" + ref);
      var checkboxAsBarang = $(".description-as_barang-" + ref);

      formIcon.html('E');
      addButton.fadeOut(100);
      form.fadeIn(100);
      formHeader.fadeOut(100);
      rowWrapper.addClass("bg-green text-white");

      wrapperParam.hide();
      $(".bq_item-description_manual-" + ref).show();
      $(".bq_item-description_auto-" + ref).select2().next().hide();
      checkboxAsBarang.attr("disabled", true);

      // Fill input value
      _key = ref;
      $(formInput).trigger("reset");
      $(formInput + " .bq_item-bq_item_parent_id-" + ref).val(bq_item_parent_id).trigger("input");
      $(formInput + " .bq_item-nomor-" + ref).val(nomor).trigger("input");
      $(formInput + " .bq_item-description-" + ref).val(description).trigger("input");
      $(formInput + " .bq_item-description_manual-" + ref).val(description).trigger("input");
      $(formInput + " .bq_item-note-" + ref).val(note).trigger("input");
      $(formInput + " .bq_item-quantity-" + ref).val(quantity).trigger("input");
      $(formInput + " .bq_item-unit-" + ref).val(unit).trigger("input");
      $(formInput + " .bq_item-material_unit_price-" + ref).val(material_unit_price).trigger("input");
      $(formInput + " .bq_item-material_total_price-" + ref).val(material_total_price).trigger("input");
      $(formInput + " .bq_item-labour_unit_price-" + ref).val(labour_unit_price).trigger("input");
      $(formInput + " .bq_item-labour_total_price-" + ref).val(labour_total_price).trigger("input");

      if (is_bold == 1) {
        $(formInput + " .bq_item-is_bold-" + ref).attr("checked", true);
      } else {
        $(formInput + " .bq_item-is_bold-" + ref).removeAttr("checked");
      };
      if (is_italic == 1) {
        $(formInput + " .bq_item-is_italic-" + ref).attr("checked", true);
      } else {
        $(formInput + " .bq_item-is_italic-" + ref).removeAttr("checked");
      };

      $('.mask-decimal').mask('#,##0.00', {
        reverse: true
      });
      $('.mask-money').mask('#,##0', {
        reverse: true
      });
    });

    $(document).on("click keyup", ".bq_item-quantity", function(e) {
      e.preventDefault();
      var ref = $(this).attr("data-id");

      calculateMaterialPrice(ref);
      calculateLabourPrice(ref);
    });

    $(document).on("keyup", ".bq_item-material_unit_price", function(e) {
      e.preventDefault();
      var ref = $(this).attr("data-id");

      calculateMaterialPrice(ref);
    });

    $(document).on("keyup", ".bq_item-labour_unit_price", function(e) {
      e.preventDefault();
      var ref = $(this).attr("data-id");

      calculateLabourPrice(ref);
    });

    async function fetchDataBqItem() {
      var bqId = "<?= $bq_id ?>";

      await $.ajax({
        type: "get",
        url: "<?php echo base_url('bq/ajax_get_bq_item_table/') ?>" + bqId,
        success: function(response) {
          $("#table-bq-item").html(response);
        }
      });
    };

    function calculateMaterialPrice(ref) {
      var quantity = $(".bq_item-quantity-" + ref).val().replace(/[^\d.]/g, "");
      quantity = (quantity != "") ? quantity : 0;

      var unitPrice = $(".bq_item-material_unit_price-" + ref).val().replace(/[^\d]/g, "");
      unitPrice = (unitPrice != "") ? unitPrice : 0;

      var total = $(".bq_item-material_total_price-" + ref);
      var totalTemp = quantity * unitPrice;

      if (quantity == "" && unitPrice == "") {
        totalTemp = "";
      };

      total.val(totalTemp).trigger("input");
    };

    function calculateLabourPrice(ref) {
      var quantity = $(".bq_item-quantity-" + ref).val().replace(/[^\d.]/g, "");
      quantity = (quantity != "") ? quantity : 0;

      var unitPrice = $(".bq_item-labour_unit_price-" + ref).val().replace(/[^\d]/g, "");
      unitPrice = (unitPrice != "") ? unitPrice : 0;

      var total = $(".bq_item-labour_total_price-" + ref);
      var totalTemp = quantity * unitPrice;

      if (quantity == "" && unitPrice == "") {
        totalTemp = "";
      };

      total.val(totalTemp).trigger("input");
    };

    function adjustSpinner(ref) {
      $(".spinner-" + ref).css("width", $(".item-form-input-wrapper-" + ref).width() + 27);
      $(".spinner-" + ref).css("height", $(".item-form-input-wrapper-" + ref).height() + 27);
      $(".spinner-" + ref).css("margin-top", "-13px");
      $(".spinner-" + ref).css("margin-left", "-13px");
    };
    // END ## BQ ITEM

    // Handle view
    $("#" + _table_list).on("click", "a.action-view", function(e) {
      e.preventDefault();
      var temp = table_bq.row($(this).closest('tr')).data();

      $("#" + _modal_view + " .approval-preview").html("Please wait...");

      $.ajax({
        type: "get",
        url: "<?= base_url('bq/ajax_get_preview/') ?>" + temp.id,
        success: function(response) {
          $(".approval-preview").html(response);
          $(".modal-bq-view-title").html("Bill Of Quantity : " + getStatus(temp.status));
        }
      });

      _modal_source = "view";
    });

    // Handle generate Quotation
    $("#" + _table_list).on("click", "a.action-generate_quotation", function(e) {
      e.preventDefault();
      var url = $(this).attr("href");

      swal({
        title: "Anda akan membuat Quotation, lanjutkan?",
        text: "Sumber data diambil berdasarkan Bill Of Quantity.",
        type: "question",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false
      }).then((result) => {
        if (result.value) {
          location.href = url;
        };
      });
    });

    // Handle submit / publish
    $(document).on("click", "button.bq-action-sent", function(e) {
      e.preventDefault();
      var ref = $(this).attr("data-id");

      swal({
        title: "Are you sure to submit?",
        text: "Once submited, you will not be able to recover this data!",
        type: "question",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false
      }).then((result) => {
        if (result.value) {
          $.ajax({
            type: "get",
            url: "<?php echo base_url('bq/ajax_set_status/') ?>",
            data: {
              bq_id: ref,
              status: 1,
            },
            dataType: "json",
            success: function(response) {
              if (response.status) {
                notify(response.data, "success");

                setTimeout(function() {
                  location.href = "<?= base_url('bq') ?>";
                }, 2000);
              } else {
                notify(response.data, "danger");
              };
            }
          });
        };
      });
    });

    // Handle bq print
    $(document).on("click", "button.action-bq-print", function(e) {
      e.preventDefault();
      $(".bq-print-area").printThis();
    });
  });

  function getStatus(status) {
    switch (status) {
      case '0':
        return 'Draft';
        break;
      case '1':
        return 'Submit';
        break;
      default:
        return 'Undefined';
        break;
    };
  };
</script>