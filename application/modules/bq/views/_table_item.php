<div class="table-responsive">
  <table class="table table-sm table-bordered table-hover">
    <thead style="text-align: center;">
      <tr>
        <th rowspan="2" width="80">Item</th>
        <th rowspan="2" colspan="2">Works Description</th>
        <th rowspan="2" width="70">Qty</th>
        <th rowspan="2" width="70">Unit</th>
        <th colspan="2">Material / Equipments</th>
        <th colspan="2">Labour</th>
        <th rowspan="2" width="50" class="text-center">#</th>
      </tr>
      <tr>
        <th width="110">Unit</th>
        <th width="110">Total</th>
        <th width="110">Unit</th>
        <th width="110">Total</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td colspan="9" class="p-0" style="height: 5px;"></td>
      </tr>
      <?php
      $subTotal_material = 0;
      $subTotal_labour = 0;
      ?>
      <?php if (count($bq_item) > 0) : ?>
        <?php foreach ($bq_item as $index => $item) : ?>
          <?php
          $isPreferences = '';
          $isPreferences .= ($item->is_bold == 1) ? 'font-weight: 600;' : '';
          $isPreferences .= ($item->is_italic == 1) ? 'font-style: italic;' : '';
          $subTotal_material = (!is_null($item->material_total_price) && !empty($item->material_total_price)) ? (float) $item->material_total_price + $subTotal_material : 0 + $subTotal_material;
          $subTotal_labour = (!is_null($item->labour_total_price) && !empty($item->labour_total_price)) ? (float) $item->labour_total_price + $subTotal_labour : 0 + $subTotal_labour;
          ?>
          <div class="item-rows item-rows-<?= $item->id ?>">
            <!-- Row data -->
            <tr class="item-row-wrapper item-row-wrapper-<?= $item->id ?>" style="<?= $isPreferences ?>">
              <td align="center" style="height: 30.48px;">
                <?= $item->nomor ?>
              </td>
              <td style="border-right-color: transparent;">
                <?= $item->description ?>
              </td>
              <td><?= $item->note ?></td>
              <td class="text-right"><?= (!is_null($item->quantity)) ? number_format($item->quantity, 0) : null ?></td>
              <td><?= $item->unit ?></td>
              <td class="text-right"><?= (!is_null($item->material_unit_price)) ? number_format($item->material_unit_price, 0) : null ?></td>
              <td class="text-right"><?= (!is_null($item->material_total_price)) ? number_format($item->material_total_price, 0) : null ?></td>
              <td class="text-right"><?= (!is_null($item->labour_unit_price)) ? number_format($item->labour_unit_price, 0) : null ?></td>
              <td class="text-right"><?= (!is_null($item->labour_total_price)) ? number_format($item->labour_total_price, 0) : null ?></td>
              <td align="center">
                <a href="javascript:;" data-id="<?= $item->id ?>" data-bq_item_parent_id="<?= $item->bq_item_parent_id ?>" data-nomor="<?= $item->nomor ?>" data-description="<?= $item->description ?>" data-note="<?= $item->note ?>" data-quantity="<?= $item->quantity ?>" data-unit="<?= $item->unit ?>" data-material_unit_price="<?= number_format($item->material_unit_price, 0, '', '') ?>" data-material_total_price="<?= number_format($item->material_total_price, 0, '', '') ?>" data-labour_unit_price="<?= number_format($item->labour_unit_price, 0, '', '') ?>" data-labour_total_price="<?= number_format($item->labour_total_price, 0, '', '') ?>" data-is_bold="<?= $item->is_bold ?>" data-is_italic="<?= $item->is_italic ?>" class="btn btn-dark btn-xs item-button-row item-button-row-edit item-button-row-edit-<?= $item->id ?>" title="Edit">
                  <i class="zmdi zmdi-edit"></i>
                </a>
              </td>
            </tr>
            <!-- END ## Row data -->
            <!-- Item inline form -->
            <tr class="item-form-row item-form-row-<?= $item->id ?> bg-light" style="display: none;">
              <td align="center">
                <span class="badge badge-warning item-form-row-icon-<?= $item->id ?>">N</span>
              </td>
              <td colspan="9" class="pl-0">
                <div class="item-form-input item-form-input-wrapper-<?= $item->id ?> bg-white p-3">
                  <!-- Loader -->
                  <div class="spinner spinner-<?= $item->id ?>">
                    <div class="lds-hourglass"></div>
                  </div>
                  <form method="post" id="item-form-input-<?= $item->id ?>">
                    <!-- Temporary hidden field -->
                    <input type="hidden" name="<?= $this->security->get_csrf_token_name(); ?>" value="<?= $this->security->get_csrf_hash(); ?>" />
                    <input type="hidden" name="bq_id" value="<?= $bq->id ?>" readonly />
                    <input type="hidden" name="bq_item_parent_id" class="bq_item-bq_item_parent_id-<?= $item->id ?>" value="<?= $item->id ?>" readonly />

                    <div class="row">
                      <div class="col-xs-12 col-md-12">
                        <div class="row">
                          <div class="col-xs-12 col-md-2">
                            <div class="form-group mb-3">
                              <label>Item</label>
                              <input type="text" name="nomor" class="form-control bq_item-nomor bq_item-nomor-<?= $item->id ?>" placeholder="Item" readonly />
                              <i class="form-group__bar"></i>
                            </div>
                          </div>
                          <div class="col-xs-12 col-md-10">
                            <label>Preferences</label>
                            <div class="group-control mb-3">
                              <div class="row">
                                <div class="col-xs-12 col-md-1">
                                  <div class="form-check m-0">
                                    <input type="checkbox" name="is_bold" class="form-check-input bq_item-is_bold-<?= $item->id ?>" id="pref-bold-<?= $item->id ?>" value="1" disabled>
                                    <label class="form-check-label" for="pref-bold-<?= $item->id ?>">Bold</label>
                                  </div>
                                </div>
                                <div class="col-xs-12 col-md-1">
                                  <div class="form-check m-0">
                                    <input type="checkbox" name="is_italic" class="form-check-input bq_item-is_italic-<?= $item->id ?>" id="pref-italic-<?= $item->id ?>" value="1" disabled>
                                    <label class="form-check-label" for="pref-italic-<?= $item->id ?>">Italic</label>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-xs-12 col-md-12">
                          <div class="row">
                            <div class="col-xs-12 col-md-8">
                              <div class="form-group mb-3">
                                <label>Works Description</label>
                                <div class="input-group">
                                  <!-- Manual -->
                                  <input type="text" name="description_manual" class="form-control bq_item-description_manual bq_item-description_manual-<?= $item->id ?>" data-id="<?= $item->id ?>" placeholder="Works Description" style="border-top-right-radius: 0.35rem; border-bottom-right-radius: 0.35rem;" readonly />
                                  <!-- Auto -->
                                  <select name="description_auto" class="form-control bq_item-description_auto bq_item-description_auto-<?= $item->id ?> select2" data-id="<?= $item->id ?>" style="border-top-right-radius: 0.35rem; border-bottom-right-radius: 0.35rem; display: none;" disabled>
                                    <?= $barang_list ?>
                                  </select>
                                  <!-- Hidden temp -->
                                  <input type="hidden" name="description" class="bq_item-description bq_item-description-<?= $item->id ?>" readonly />
                                </div>
                                <!-- Params -->
                                <div class="form-control wrapper-params wrapper-params-<?= $item->id ?>" style="display: none;"></div>
                                <!-- END ## Params -->
                              </div>
                            </div>
                            <div class="col-xs-12 col-md-4">
                              <div class="form-group mb-3">
                                <label>Note</label>
                                <input type="text" name="note" class="form-control bq_item-note bq_item-note-<?= $item->id ?>" placeholder="Note" readonly />
                                <i class="form-group__bar"></i>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-xs-12 col-md-2">
                            <div class="form-group mb-3">
                              <label>Quantity</label>
                              <input type="number" name="quantity" class="form-control bq_item-quantity bq_item-quantity-<?= $item->id ?>" data-id="<?= $item->id ?>" placeholder="Quantity" min="1" readonly />
                              <i class="form-group__bar"></i>
                            </div>
                          </div>
                          <div class="col-xs-12 col-md-2">
                            <div class="form-group mb-3">
                              <label>Unit</label>
                              <input type="text" name="unit" class="form-control bq_item-unit bq_item-unit-<?= $item->id ?>" placeholder="Unit" readonly />
                              <i class="form-group__bar"></i>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-xs-12 col-md-6">
                            <label>Material / Equipments</label>
                            <div class="group-control mb-3">
                              <div class="row">
                                <div class="col-xs-12 col-md-6">
                                  <div class="form-group mb-3">
                                    <label>Unit</label>
                                    <input type="text" name="material_unit_price" class="form-control mask-money bq_item-material_unit_price bq_item-material_unit_price-<?= $item->id ?>" data-id="<?= $item->id ?>" placeholder="Unit" />
                                    <i class="form-group__bar"></i>
                                  </div>
                                </div>
                                <div class="col-xs-12 col-md-6">
                                  <div class="form-group mb-3">
                                    <label>Total</label>
                                    <input type="text" name="material_total_price" class="form-control mask-money bq_item-material_total_price bq_item-material_total_price-<?= $item->id ?>" data-id="<?= $item->id ?>" placeholder="Total" readonly />
                                    <i class="form-group__bar"></i>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="col-xs-12 col-md-6">
                            <label>Labour</label>
                            <div class="group-control mb-3">
                              <div class="row">
                                <div class="col-xs-12 col-md-6">
                                  <div class="form-group mb-3">
                                    <label>Unit</label>
                                    <input type="text" name="labour_unit_price" class="form-control mask-money bq_item-labour_unit_price bq_item-labour_unit_price-<?= $item->id ?>" data-id="<?= $item->id ?>" placeholder="Unit" />
                                    <i class="form-group__bar"></i>
                                  </div>
                                </div>
                                <div class="col-xs-12 col-md-6">
                                  <div class="form-group mb-3">
                                    <label>Total</label>
                                    <input type="text" name="labour_total_price" class="form-control mask-money bq_item-labour_total_price bq_item-labour_total_price-<?= $item->id ?>" data-id="<?= $item->id ?>" placeholder="Total" readonly />
                                    <i class="form-group__bar"></i>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-xs-12 col-md-12">
                        <a href="javascript:;" class="btn btn-secondary item-form-input-cancel">Cancel</a>
                        <a href="javascript:;" class="btn btn-success item-form-input-save" data-id="<?= $item->id ?>"><i class="zmdi zmdi-save"></i> Save</a>
                      </div>
                    </div>
                  </form>
                </div>
              </td>
            </tr>
            <!-- END ## Item inline form -->
          </div>
        <?php endforeach ?>
      <?php endif ?>
      </td>
      </tr>
</div>
<!-- END ## Header inline form -->
</tbody>
<tfoot>
  <tr>
    <td colspan="9" class="p-0" style="height: 5px;"></td>
  </tr>
  <tr>
    <th colspan="7" class="text-right"><?= number_format($subTotal_material) ?></th>
    <th>&nbsp;</th>
    <th class="text-right"><?= number_format($subTotal_labour) ?></th>
    <th>&nbsp;</th>
  </tr>
  <tr>
    <td colspan="9" class="p-0" style="height: 5px;"></td>
  </tr>
  <tr>
    <td colspan="8" class="text-center">
      <span style="padding-left: 16rem;">T O T A L</span>
    </td>
    <th class="text-right"><u><?= number_format($subTotal_material + $subTotal_labour) ?></u></th>
    <th>&nbsp;</th>
  </tr>
</tfoot>
</table>
</div>