<?php require_once('main.css.php') ?>
<?php $revisiNomor = null ?>
<?php $revisiNomorLabel = null ?>

<section id="suratjalan">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <span class="badge badge-info" style="position: absolute; margin-top: 2px;">
                        <?php if (isset($is_revisi) && $is_revisi == true) : ?>
                            <?php $revisiNomor = (!is_null($bq->revisi_nomor)) ? (int) $bq->revisi_nomor + 1 : 1; ?>
                            REV
                        <?php else : ?>
                            <?php $revisiNomorLabel = (isset($bq->revisi_nomor) && !is_null($bq->revisi_nomor)) ? ' : Rev. ' . $bq->revisi_nomor : null; ?>
                            <?= (isset($bq->id)) ? 'EDIT' : 'NEW' ?>
                        <?php endif ?>
                    </span>
                    <div style="margin-left: 50px;">
                        <h4 class="card-title"><?php echo (isset($card_title)) ? $card_title . $revisiNomorLabel : '' ?></h4>
                        <h6 class="card-subtitle"><?php echo (isset($card_subTitle)) ? $card_subTitle : '' ?></h6>
                    </div>
                    <div class="clear-card"></div>
                </div>
            </div>
            <div class="clear-card"></div>

            <!-- bq -->
            <ul class="list-group list-group-flush">
                <li class="list-group-item">
                    <div class="row">
                        <div class="col-xs-12 col-auto"><b>Nomor :</b></div>
                        <div class="col-xs-12 col-md-10"><?= isset($bq->nomor) ? $bq->nomor : '' ?></div>
                    </div>
                </li>
                <li class="list-group-item">
                    <div class="row">
                        <div class="col-xs-12 col-auto"><b>Date :</b></div>
                        <div class="col-xs-12 col-md-10"><?= isset($bq->tanggal) ? $controller->localizeDate($bq->tanggal) : '' ?></div>
                    </div>
                </li>
                <li class="list-group-item">
                    <div class="row">
                        <div class="col-xs-12 col-auto"><b>Project Name :</b></div>
                        <div class="col-xs-12 col-md-10"><?= isset($bq->project_name) ? $bq->project_name : '' ?></div>
                    </div>
                </li>
                <li class="list-group-item">
                    <div class="row">
                        <div class="col-xs-12 col-auto"><b>Customer Name :</b></div>
                        <div class="col-xs-12 col-md-10"><?= isset($bq->customer_name) ? $bq->customer_name : '' ?></div>
                    </div>
                </li>
            </ul>
            <!-- END ## bq -->

            <div class="mb-4"></div>
            <div id="table-bq-item">
                <?php include_once("_table_item.php"); ?>
            </div>

            <small class="form-text text-muted">
                Fields with red stars (<label required></label>) are required.
            </small>

            <div class="row">
                <div class="col">
                    <div class="buttons-container">
                        <div class="row">
                            <div class="col">
                                <a href="<?= base_url('bq') ?>" class="btn btn--raised btn-dark btn--icon-text btn-custom mr-2">
                                    Cancel
                                </a>
                            </div>
                            <div class="col text-right">
                                <button class="btn btn--raised btn-primary btn--icon-text btn-custom bq-action-sent spinner-action-button" data-id="<?= $bq->id ?>">
                                    Submit
                                    <i class="zmdi zmdi-long-arrow-right"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>