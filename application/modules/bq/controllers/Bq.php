<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . 'controllers/AppBackend.php');

class Bq extends AppBackend
{
  function __construct()
  {
    parent::__construct();
    $this->load->model([
      'AppModel',
      'SupplierbarangModel',
      'BarangjenisModel',
      'BqModel',
      'BqitemModel',
      'QuotationModel',
    ]);
    $this->load->library('form_validation');
  }

  public function index()
  {
    $this->_tableList('index');
  }

  private function _tableList($module = 'index', $pageTitle = null)
  {
    $agent = new Mobile_Detect;
    $pageTitle = (!is_null($pageTitle)) ? ' › ' . $pageTitle : '';
    $data = array(
      'app' => $this->app(),
      'main_js' => $this->load_main_js('bq', false, array(
        'controller' => $this,
        'module' => $module
      )),
      'card_title' => 'Bill Of Quantity' . $pageTitle,
      'is_mobile' => $agent->isMobile(),
      'module' => $module
    );
    $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
    $this->template->load_view('index', $data, TRUE);
    $this->template->render();
  }

  public function edit($id = null)
  {
    $agent = new Mobile_Detect;
    $bq = $this->BqModel->getDetail(array('id' => $id, 'status' => 0));
    $barang = $this->SupplierbarangModel->getAll();

    if (!is_null($bq)) {
      $data = array(
        'app' => $this->app(),
        'controller' => $this,
        'main_js' => $this->load_main_js('bq', false, array(
          'controller' => $this,
          'bq_id' => $id
        )),
        'card_title' => 'Bill Of Quantity › Item',
        'is_mobile' => $agent->isMobile(),
        'bq' => $bq,
        'bq_item' => $this->BqitemModel->getAll_sorted($id),
        'barang_list' => $this->init_list($barang, 'id', 'nama_barang'),
      );
      $this->template->set('title', $data['card_title'] . ' | ' . $data['app']->app_name, TRUE);
      $this->template->load_view('wizard_item_form', $data, TRUE);
      $this->template->render();
    } else {
      show_404();
    };
  }

  public function ajax_get_list()
  {
    $this->handle_ajax_request();
    $module = $this->input->get('src');

    switch ($module) {
      case 'index':
        $status = array();
        $pic = array();
        break;
      default:
        $status = array(1);
        $pic = array();
        break;
    };

    $dtAjax_config = array(
      'table_name' => 'bq',
      'order_column' => 5,
      'order_column_dir' => 'desc',
      'static_conditional' => $pic,
      'static_conditional_in_key' => 'status',
      'static_conditional_in' => $status,
    );
    $response = $this->AppModel->getData_dtAjax($dtAjax_config);

    echo json_encode($response);
  }

  public function ajax_set_status()
  {
    $this->handle_ajax_request();
    $bqId = $this->input->get('bq_id');
    $status = $this->input->get('status');

    echo json_encode($this->BqModel->setStatus($bqId, $status));
  }

  public function ajax_get_preview($id)
  {
    $this->handle_ajax_request();
    $bq = $this->BqModel->getDetail(array('id' => $id));

    if (!is_null($bq)) {
      $agent = new Mobile_Detect;
      $data = array(
        'app' => $this->app(),
        'main_js' => $this->load_main_js('bq'),
        'card_title' => 'Bill Of Quantity › View',
        'controller' => $this,
        'data_id' => $id,
        'bq' => $bq,
        'bq_item' => $this->BqitemModel->getAll_sorted($id),
      );

      if ($agent->isMobile()) {
        $this->load->view('_view_mobile', $data);
      } else {
        $this->load->view('_view', $data);
      };
    } else {
      show_404();
    };
  }

  // Bill Of Quantity Item
  public function ajax_get_bq_item_table($id = null)
  {
    $this->handle_ajax_request();
    $barang = $this->SupplierbarangModel->getAll();

    echo $this->load->view('_table_item', array(
      'bq' => $this->BqModel->getDetail(array('id' => $id)),
      'bq_item' => $this->BqitemModel->getAll_sorted($id),
      'barang_list' => $this->init_list($barang, 'id', 'nama_barang'),
    ), TRUE);
  }

  public function ajax_save_item($id = null)
  {
    $this->handle_ajax_request();
    $this->form_validation->set_rules($this->BqitemModel->rules());

    if ($this->form_validation->run() === true) {
      if (is_null($id)) {
        $transaction = $this->BqitemModel->insert();
      } else {
        $transaction = $this->BqitemModel->update($id);
      };

      if ($transaction['status'] === true) {
        // Store params when exist
        if (!is_null($this->input->post('description_as_barang'))) {
          $params = $this->input->post('description_param');
          $paramsPayload = array();

          if (!is_null($params)) {
            foreach ($params as $index => $item) {
              $paramsPayload[] = array(
                'bq_id' => $this->input->post('bq_id'),
                'bq_item_parent_id' => $transaction['data_id'],
                'description' => $item['label'] . ' : ' . $item['value'],
                'is_italic' => 1,
              );
            };

            if (count($paramsPayload) > 0) {
              $this->BqitemModel->insertBatch($paramsPayload);
            };
          };
        };
        // END ## Store params when exist

        // Update master grand total
        $this->BqModel->updateTotalPrice($this->input->post('bq_id'));
      };

      echo json_encode($transaction);
    } else {
      $errors = validation_errors('<div>- ', '</div>');
      echo json_encode(array('status' => false, 'data' => $errors));
    };
  }

  public function ajax_add_empty_row($bqId = null)
  {
    $this->handle_ajax_request();
    $_POST['bq_id'] = $bqId;
    echo json_encode($this->BqitemModel->insert());
  }

  public function ajax_delete_item($id)
  {
    $this->handle_ajax_request();
    echo json_encode($this->BqitemModel->delete($id));
  }

  public function ajax_get_barang_param()
  {
    // $this->handle_ajax_request();
    $barangId = $this->input->get('barang_id');
    $barang = $this->SupplierbarangModel->getDetail(array('id' => $barangId));
    $result = null;

    if (!is_null($barang)) {
      $barangJenis = $this->BarangjenisModel->getAll_parameter($barang->jenis);
      $barangParameter = $barang->parameter;

      $result['unit'] = $barang->satuan;
      $result['material_unit_price'] = $barang->harga_satuan;
      $result['parameter'] = array();

      if (!is_null($barangParameter) && !empty($barangParameter)) {
        $barangParameter = json_decode($barangParameter);

        foreach ($barangParameter as $index => $item) {
          $findParamLabel = $this->searchInArrayObj($barangJenis, 'key', $index);

          if (count($findParamLabel) > 0) {
            $result['parameter'][] = array(
              'label' => $findParamLabel->label,
              'value' => $item
            );
          };
        };
      };
    };

    echo json_encode($result);
  }
  // END ## Bill Of Quantity Item

  public function generate_quotation($id = null)
  {
    $generate = $this->QuotationModel->generateFromBq($id);

    if ($generate['status'] === true) {
      redirect(base_url('quotation/wizard/' . $generate['data_id'] . '/1'));
    } else {
      show_error($generate['data'], '500');
    };
  }

  public function get_status($status)
  {
    switch ($status) {
      case '0':
        return 'Draft';
        break;
      case '1':
        return 'Submit';
        break;
      default:
        return 'Belum Input';
        break;
    };
  }
}
