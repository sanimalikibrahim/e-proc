<footer class="footer hidden-xs-down">
  <p>© <?php echo $app->app_name ?></p>

  <ul class="nav footer__nav">
    <a class="nav-link" href="#" target="_blank">
      PT. ARYA JAYA | Version <?php echo $app->app_version ?>
    </a>
  </ul>
</footer>