-- MySQL dump 10.13  Distrib 5.5.62, for Win64 (AMD64)
--
-- Host: localhost    Database: e_proc
-- ------------------------------------------------------
-- Server version	5.7.33

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `barang_jenis`
--

DROP TABLE IF EXISTS `barang_jenis`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `barang_jenis` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(150) NOT NULL,
  `parameter` varchar(150) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `nama` (`nama`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `barang_jenis`
--

LOCK TABLES `barang_jenis` WRITE;
/*!40000 ALTER TABLE `barang_jenis` DISABLE KEYS */;
INSERT INTO `barang_jenis` VALUES (1,'AC','Brand','2022-04-12 14:46:31',1,NULL,NULL),(2,'AC','Type','2022-04-12 14:46:54',1,NULL,NULL),(3,'AC','Model','2022-04-12 14:46:54',1,NULL,NULL),(4,'AC','Capacity','2022-04-12 14:46:54',1,NULL,NULL),(5,'AC','Static Pressure','2022-04-12 14:46:54',1,'2022-04-13 08:33:29',1),(6,'AC','Speed','2022-04-12 14:46:54',1,NULL,NULL),(7,'AC','Power','2022-04-12 14:46:54',1,NULL,NULL),(8,'AC','Electric Power','2022-04-12 14:46:54',1,'2022-04-12 15:44:55',1),(9,'Mounting','Diameter','2022-04-12 14:50:16',1,'2022-04-12 14:50:24',1),(10,'Mounting','Tinggi','2022-04-12 14:50:32',1,'2022-04-12 15:45:26',1),(15,'Lainnya','','2022-04-12 15:43:57',1,NULL,NULL),(16,'Joiting','Tinggi','2022-04-12 15:47:15',1,NULL,NULL),(17,'Joiting','Lebar','2022-04-12 15:47:55',1,'2022-04-14 09:47:59',1);
/*!40000 ALTER TABLE `barang_jenis` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bom`
--

DROP TABLE IF EXISTS `bom`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bom` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lom_id` int(11) DEFAULT NULL,
  `nomor` varchar(30) NOT NULL,
  `tanggal` date NOT NULL,
  `po_nomor` varchar(30) DEFAULT NULL,
  `po_tanggal` date DEFAULT NULL,
  `customer_name` varchar(150) NOT NULL,
  `customer_address` varchar(255) DEFAULT NULL,
  `customer_attn` varchar(150) DEFAULT NULL,
  `project_name` varchar(255) NOT NULL,
  `note` text,
  `status` int(1) NOT NULL DEFAULT '0',
  `is_create_bot` int(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bom`
--

LOCK TABLES `bom` WRITE;
/*!40000 ALTER TABLE `bom` DISABLE KEYS */;
INSERT INTO `bom` VALUES (1,1,'00001/BOM/AJM/IV/22','2022-04-27','008/SPPPP/RS-SANTOSA/APG/III/2','2022-04-25','PT. Arya Jaya','Jl. Bandung Raya, Kota Bandung - Jawa Barat','Bpk. Saha','Pengembangan Aplikasi eProc v2','',1,1,'2022-04-27 09:52:19',16,'2022-04-27 09:59:08',16);
/*!40000 ALTER TABLE `bom` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bom_item`
--

DROP TABLE IF EXISTS `bom_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bom_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bom_id` int(11) NOT NULL,
  `bom_item_parent_id` int(11) DEFAULT NULL,
  `nomor` varchar(30) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `quantity` float(20,0) DEFAULT NULL,
  `unit` varchar(30) DEFAULT NULL,
  `to_collected` int(11) DEFAULT NULL,
  `remark` varchar(30) DEFAULT NULL,
  `mos1_date` date DEFAULT NULL,
  `mos1_qty` int(11) DEFAULT NULL,
  `mos1_cl` int(1) DEFAULT NULL,
  `mos2_date` date DEFAULT NULL,
  `mos2_qty` int(11) DEFAULT NULL,
  `mos2_cl` int(1) DEFAULT NULL,
  `mos3_date` date DEFAULT NULL,
  `mos3_qty` int(11) DEFAULT NULL,
  `mos3_cl` int(1) DEFAULT NULL,
  `is_bold` int(1) DEFAULT '0',
  `is_italic` int(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `rab_id` (`bom_id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bom_item`
--

LOCK TABLES `bom_item` WRITE;
/*!40000 ALTER TABLE `bom_item` DISABLE KEYS */;
INSERT INTO `bom_item` VALUES (41,1,NULL,'','Main Unit','',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL),(42,1,41,'1','Axial Fan Fresh Air fan','',1,'unit',1,'Done','2022-04-27',1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(43,1,42,NULL,'Type : Axial Fan',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,1),(44,1,42,NULL,'Brand : Vanco',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,1),(45,1,42,NULL,'Model : VAD/6-630',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,1),(46,1,42,NULL,'Power : 1500 watt',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,1),(47,1,42,NULL,'Speed : 960 rpm',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,1),(48,1,42,NULL,'Capacity : 10 mmwg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,1),(49,1,42,NULL,'Electric Power : 380V/3Ph/1,5kW',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,1),(50,1,42,NULL,'Static Pressure : 11520 CMH',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,1),(51,1,41,'2','Jointing Untuk tray Basket','',1,'pcs',1,'Done','2022-04-27',1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(52,1,51,NULL,'Lebar : 50',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,1),(53,1,51,NULL,'Tinggi : 100',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,1),(54,1,NULL,'3','Pre Filter G4','',1,'pcs',1,'Done','2022-04-27',1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `bom_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bot`
--

DROP TABLE IF EXISTS `bot`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bot` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bom_id` int(11) DEFAULT NULL,
  `nomor` varchar(30) NOT NULL,
  `tanggal` date NOT NULL,
  `po_nomor` varchar(30) DEFAULT NULL,
  `po_tanggal` date DEFAULT NULL,
  `customer_name` varchar(150) NOT NULL,
  `customer_address` varchar(255) DEFAULT NULL,
  `customer_attn` varchar(150) DEFAULT NULL,
  `project_name` varchar(255) NOT NULL,
  `note` text,
  `status` int(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bot`
--

LOCK TABLES `bot` WRITE;
/*!40000 ALTER TABLE `bot` DISABLE KEYS */;
INSERT INTO `bot` VALUES (1,1,'00001/BOT/AJM/IV/22','2022-04-27','008/SPPPP/RS-SANTOSA/APG/III/2','2022-04-25','PT. Arya Jaya','Jl. Bandung Raya, Kota Bandung - Jawa Barat','Bpk. Saha','Pengembangan Aplikasi eProc v2','',1,'2022-04-27 11:12:31',16,'2022-04-27 11:25:14',16);
/*!40000 ALTER TABLE `bot` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bot_item`
--

DROP TABLE IF EXISTS `bot_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bot_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bot_id` int(11) NOT NULL,
  `bot_item_parent_id` int(11) DEFAULT NULL,
  `nomor` varchar(30) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `quantity` float(20,0) DEFAULT NULL,
  `unit` varchar(30) DEFAULT NULL,
  `to_collected` int(11) DEFAULT NULL,
  `remark` varchar(30) DEFAULT NULL,
  `mos1_date` date DEFAULT NULL,
  `mos1_qty` int(11) DEFAULT NULL,
  `mos1_cl` int(1) DEFAULT NULL,
  `mos2_date` date DEFAULT NULL,
  `mos2_qty` int(11) DEFAULT NULL,
  `mos2_cl` int(1) DEFAULT NULL,
  `mos3_date` date DEFAULT NULL,
  `mos3_qty` int(11) DEFAULT NULL,
  `mos3_cl` int(1) DEFAULT NULL,
  `is_bold` int(1) DEFAULT '0',
  `is_italic` int(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `rab_id` (`bot_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bot_item`
--

LOCK TABLES `bot_item` WRITE;
/*!40000 ALTER TABLE `bot_item` DISABLE KEYS */;
INSERT INTO `bot_item` VALUES (2,1,NULL,'1','Sewa Scaffolding','PT. TOSAYAGI',1,'lot',1,'Done','2022-04-27',1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(3,1,NULL,'2','Flushing Atap','Bpk. Endoy',7,'titik',7,'Done','2022-04-27',7,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `bot_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bq`
--

DROP TABLE IF EXISTS `bq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rab_id` int(11) NOT NULL,
  `nomor` varchar(30) NOT NULL,
  `tanggal` date NOT NULL,
  `customer_name` varchar(150) NOT NULL,
  `customer_address` varchar(255) DEFAULT NULL,
  `customer_attn` varchar(150) DEFAULT NULL,
  `project_name` varchar(255) NOT NULL,
  `note` text,
  `total_price` float(20,0) DEFAULT '0',
  `status` int(1) NOT NULL DEFAULT '0',
  `is_created_quotation` int(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `rab_id` (`rab_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bq`
--

LOCK TABLES `bq` WRITE;
/*!40000 ALTER TABLE `bq` DISABLE KEYS */;
INSERT INTO `bq` VALUES (1,3,'00001/BQ/AJM/IV/22','2022-04-27','PT. Arya Jaya','Jl. Bandung Raya, Kota Bandung - Jawa Barat','Bpk. Saha','Pengembangan Aplikasi eProc v2','- Harga bisa berubah jika terdapat perubahan pada task\r\n- Harga dibayar per 3 termin',7555000,1,1,'2022-04-27 08:25:35',15,NULL,NULL);
/*!40000 ALTER TABLE `bq` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bq_item`
--

DROP TABLE IF EXISTS `bq_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bq_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bq_id` int(11) NOT NULL,
  `bq_item_parent_id` int(11) DEFAULT NULL,
  `nomor` varchar(30) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `quantity` float(20,0) DEFAULT NULL,
  `unit` varchar(30) DEFAULT NULL,
  `material_unit_price` float(20,0) DEFAULT NULL,
  `material_total_price` float(20,0) DEFAULT NULL,
  `labour_unit_price` float(20,0) DEFAULT NULL,
  `labour_total_price` float(20,0) DEFAULT NULL,
  `is_bold` int(1) DEFAULT '0',
  `is_italic` int(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `rab_id` (`bq_id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bq_item`
--

LOCK TABLES `bq_item` WRITE;
/*!40000 ALTER TABLE `bq_item` DISABLE KEYS */;
INSERT INTO `bq_item` VALUES (41,1,NULL,'','Main Unit','',NULL,'',NULL,NULL,NULL,NULL,1,NULL),(42,1,41,'1','Axial Fan Fresh Air fan','',1,'unit',4500000,4500000,350000,350000,NULL,NULL),(43,1,42,NULL,'Type : Axial Fan',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,1),(44,1,42,NULL,'Brand : Vanco',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,1),(45,1,42,NULL,'Model : VAD/6-630',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,1),(46,1,42,NULL,'Power : 1500 watt',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,1),(47,1,42,NULL,'Speed : 960 rpm',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,1),(48,1,42,NULL,'Capacity : 10 mmwg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,1),(49,1,42,NULL,'Electric Power : 380V/3Ph/1,5kW',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,1),(50,1,42,NULL,'Static Pressure : 11520 CMH',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,1),(51,1,41,'2','Jointing Untuk tray Basket','',1,'pcs',50000,50000,5000,5000,NULL,NULL),(52,1,51,NULL,'Lebar : 50',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,1),(53,1,51,NULL,'Tinggi : 100',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,1),(54,1,NULL,'3','Pre Filter G4','',1,'pcs',2500000,2500000,150000,150000,NULL,NULL);
/*!40000 ALTER TABLE `bq_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `document`
--

DROP TABLE IF EXISTS `document`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `document` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `ref` varchar(255) DEFAULT NULL,
  `ref_id` int(30) DEFAULT NULL,
  `description` text,
  `file_raw_name` varchar(100) DEFAULT NULL,
  `file_raw_name_thumb` varchar(100) DEFAULT NULL,
  `file_name` varchar(255) NOT NULL,
  `file_name_thumb` varchar(255) DEFAULT NULL,
  `file_size` varchar(30) DEFAULT NULL,
  `file_type` varchar(100) DEFAULT NULL,
  `file_ext` varchar(15) DEFAULT NULL,
  `is_verified` int(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(30) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ref` (`ref`),
  KEY `ref_id` (`ref_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `document`
--

LOCK TABLES `document` WRITE;
/*!40000 ALTER TABLE `document` DISABLE KEYS */;
INSERT INTO `document` VALUES (1,'rab',3,'00003/RAB/AJM/IV/22','8ec05721007bbbf019e305ca8b25921f.pdf',NULL,'directory/rab/8ec05721007bbbf019e305ca8b25921f.pdf',NULL,'86.54','application/pdf','.pdf',0,'2022-04-19 05:42:39',12),(5,'rab',3,'00003/RAB/AJM/IV/22','055ced7df120458ce2e5ef8bcf4f6030.jpg','055ced7df120458ce2e5ef8bcf4f6030_thumb.jpg','directory/rab/055ced7df120458ce2e5ef8bcf4f6030.jpg','directory/rab/055ced7df120458ce2e5ef8bcf4f6030_thumb.jpg','12.16','image/jpeg','.jpg',0,'2022-04-19 06:53:55',12);
/*!40000 ALTER TABLE `document` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lom`
--

DROP TABLE IF EXISTS `lom`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lom` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rab_id` int(11) DEFAULT NULL,
  `nomor` varchar(30) NOT NULL,
  `tanggal` date NOT NULL,
  `po_nomor` varchar(30) DEFAULT NULL,
  `po_tanggal` date DEFAULT NULL,
  `customer_name` varchar(150) NOT NULL,
  `customer_address` varchar(255) DEFAULT NULL,
  `customer_attn` varchar(150) DEFAULT NULL,
  `project_name` varchar(255) NOT NULL,
  `note` text,
  `status` int(1) NOT NULL DEFAULT '0',
  `is_create_bom` int(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lom`
--

LOCK TABLES `lom` WRITE;
/*!40000 ALTER TABLE `lom` DISABLE KEYS */;
INSERT INTO `lom` VALUES (1,3,'00001/LOM/AJM/IV/22','2022-04-27','008/SPPPP/RS-SANTOSA/APG/III/2','2022-04-25','PT. Arya Jaya','Jl. Bandung Raya, Kota Bandung - Jawa Barat','Bpk. Saha','Pengembangan Aplikasi eProc v2','',1,1,'2022-04-27 09:41:39',16,'2022-04-27 09:46:49',16);
/*!40000 ALTER TABLE `lom` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lom_item`
--

DROP TABLE IF EXISTS `lom_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lom_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lom_id` int(11) NOT NULL,
  `lom_item_parent_id` int(11) DEFAULT NULL,
  `nomor` varchar(30) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `quantity` float(20,0) DEFAULT NULL,
  `unit` varchar(30) DEFAULT NULL,
  `is_bold` int(1) DEFAULT '0',
  `is_italic` int(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `rab_id` (`lom_id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lom_item`
--

LOCK TABLES `lom_item` WRITE;
/*!40000 ALTER TABLE `lom_item` DISABLE KEYS */;
INSERT INTO `lom_item` VALUES (41,1,NULL,'','Main Unit','',NULL,'',1,NULL),(42,1,41,'1','Axial Fan Fresh Air fan','',1,'unit',NULL,NULL),(43,1,42,NULL,'Type : Axial Fan',NULL,NULL,NULL,0,1),(44,1,42,NULL,'Brand : Vanco',NULL,NULL,NULL,0,1),(45,1,42,NULL,'Model : VAD/6-630',NULL,NULL,NULL,0,1),(46,1,42,NULL,'Power : 1500 watt',NULL,NULL,NULL,0,1),(47,1,42,NULL,'Speed : 960 rpm',NULL,NULL,NULL,0,1),(48,1,42,NULL,'Capacity : 10 mmwg',NULL,NULL,NULL,0,1),(49,1,42,NULL,'Electric Power : 380V/3Ph/1,5kW',NULL,NULL,NULL,0,1),(50,1,42,NULL,'Static Pressure : 11520 CMH',NULL,NULL,NULL,0,1),(51,1,41,'2','Jointing Untuk tray Basket','',1,'pcs',NULL,NULL),(52,1,51,NULL,'Lebar : 50',NULL,NULL,NULL,0,1),(53,1,51,NULL,'Tinggi : 100',NULL,NULL,NULL,0,1),(54,1,NULL,'3','Pre Filter G4','',1,'pcs',NULL,NULL);
/*!40000 ALTER TABLE `lom_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu`
--

DROP TABLE IF EXISTS `menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `parent_id` int(30) DEFAULT NULL,
  `name` varchar(150) NOT NULL,
  `order_pos` int(5) DEFAULT NULL,
  `link` text NOT NULL,
  `link_tobase` enum('0','1') NOT NULL DEFAULT '1',
  `icon` varchar(30) DEFAULT NULL,
  `is_newtab` enum('0','1') NOT NULL DEFAULT '0',
  `role_pic` text,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(255) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`),
  KEY `parent_id` (`parent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=132 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu`
--

LOCK TABLES `menu` WRITE;
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
INSERT INTO `menu` VALUES (1,NULL,'Beranda',1,'dashboard','1','zmdi zmdi-home','0','[\"Administrator\",\"Engineering\",\"Finance\",\"Procurement\",\"Sales\",\"Teknik\"]','2019-12-23 17:55:38',NULL,'2022-04-19 08:26:00',NULL),(2,5,'Pengguna',98,'user','1','zmdi zmdi-accounts-alt','0','[\"Administrator\"]','2019-12-23 17:56:14',NULL,'2022-02-01 13:28:55',NULL),(4,5,'Konfigurasi Menu',99,'menuconfiguration','1','','0','[\"Administrator\"]','2019-12-26 05:54:49',NULL,'2022-02-01 13:28:51',NULL),(5,NULL,'Pengaturan',98,'setting, menuconfiguration, user, settingnomorsurat','0','zmdi zmdi-settings','0','[\"Administrator\",\"Engineering\",\"Finance\",\"Procurement\",\"Sales\",\"Teknik\"]','2019-12-26 05:55:40',NULL,'2022-04-19 08:26:07',NULL),(6,5,'Aplikasi',2,'setting/application','1','','0','[\"Administrator\"]','2019-12-26 05:55:59',NULL,'2020-07-26 03:55:29',NULL),(7,5,'SMTP',3,'setting/smtp','1','','0','[\"Administrator\"]','2019-12-26 05:56:38',NULL,'2020-07-26 03:55:29',NULL),(20,5,'Akun',1,'setting/account','1','','0','[\"Administrator\",\"Engineering\",\"Finance\",\"Procurement\",\"Sales\",\"Teknik\"]','2020-06-23 07:31:39',NULL,'2022-04-14 13:45:04',NULL),(35,5,'Dashboard Image',4,'setting/dashboard','1','','0','[\"Administrator\"]','2020-08-04 06:40:08',NULL,'2020-08-04 07:01:22',NULL),(71,NULL,'Logout',99,'logout','1','zmdi zmdi-sign-in','0','[\"Administrator\",\"Engineering\",\"Finance\",\"Procurement\",\"Sales\",\"Teknik\"]','2020-12-08 21:58:07',NULL,'2022-04-14 13:44:53',NULL),(91,NULL,'Referensi',97,'unit, subunit, supplierbarang, barangjenis','0','zmdi zmdi-layers','0','[\"Administrator\",\"Engineering\",\"Procurement\"]','2021-08-04 16:52:07',NULL,'2022-09-06 11:13:49',NULL),(111,91,'Supplier Barang',2,'supplierbarang','1','','0','[\"Administrator\",\"Engineering\",\"Procurement\"]','2021-10-21 04:11:01',NULL,'2022-09-06 11:13:43',NULL),(117,NULL,'Rincian Anggaran Biaya',4,'rab','1','zmdi zmdi-file-text','0','[\"Engineering\",\"Finance\",\"Sales\",\"Teknik\"]','2022-03-24 07:36:41',NULL,'2022-04-27 09:28:53',NULL),(118,NULL,'Bill Of Quantity',5,'bq','1','zmdi zmdi-file-text','0','[\"Sales\"]','2022-03-24 07:37:02',NULL,'2022-04-14 13:41:19',NULL),(119,NULL,'Quotation',6,'quotation','1','zmdi zmdi-file-text','0','[\"Finance\",\"Sales\"]','2022-03-24 07:43:04',NULL,'2022-04-14 13:42:05',NULL),(120,NULL,'List Of Material',7,'lom','1','zmdi zmdi-file-text','0','[\"Teknik\"]','2022-03-24 07:43:38',NULL,'2022-04-14 13:42:44',NULL),(121,NULL,'Doc Approval Material (x)',8,'dam','1','zmdi zmdi-file-text','0','[\"Teknik\"]','2022-03-24 07:43:58',NULL,'2022-04-16 00:42:12',NULL),(122,NULL,'Bill Of Material',9,'bom','1','zmdi zmdi-file-text','0','[\"Teknik\"]','2022-03-24 07:44:15',NULL,'2022-04-15 18:33:33',NULL),(123,NULL,'Purchase Request',11,'pr','1','zmdi zmdi-file-text','0','[\"Procurement\",\"Teknik\"]','2022-03-24 07:44:37',NULL,'2022-04-16 01:26:28',NULL),(124,NULL,'Purchase Order',12,'po','1','zmdi zmdi-file-text','0','[\"Finance\",\"Procurement\"]','2022-03-24 07:45:43',NULL,'2022-04-16 09:07:09',NULL),(125,NULL,'Surat Kirim Barang',13,'skb','1','zmdi zmdi-file-text','0','[\"Procurement\"]','2022-03-24 07:46:05',NULL,'2022-04-15 18:33:40',NULL),(128,91,'Jenis Barang',1,'barangjenis','1','','0','[\"Administrator\",\"Engineering\",\"Procurement\"]','2022-04-12 14:37:39',NULL,'2022-09-06 11:13:34',NULL),(129,91,'Unit',3,'unit','1','','0','[\"Administrator\"]','2022-04-14 13:47:10',NULL,NULL,NULL),(130,91,'Sub Unit',4,'subunit','1','','0','[\"Administrator\"]','2022-04-14 13:47:20',NULL,NULL,NULL),(131,NULL,'Bill Of Tools',10,'bot','1','zmdi zmdi-file-text','0','[\"Teknik\"]','2022-04-15 18:34:15',NULL,NULL,NULL);
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notification`
--

DROP TABLE IF EXISTS `notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notification` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `user_from` int(30) NOT NULL,
  `user_to` int(30) NOT NULL,
  `ref` varchar(150) DEFAULT NULL,
  `ref_id` int(30) DEFAULT NULL,
  `description` text NOT NULL,
  `link` text NOT NULL,
  `is_read` enum('0','1') NOT NULL DEFAULT '0',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_from` (`user_from`),
  KEY `user_to` (`user_to`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notification`
--

LOCK TABLES `notification` WRITE;
/*!40000 ALTER TABLE `notification` DISABLE KEYS */;
/*!40000 ALTER TABLE `notification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `po`
--

DROP TABLE IF EXISTS `po`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `po` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pr_id` int(11) DEFAULT NULL,
  `supplier_id` int(11) NOT NULL,
  `tipe` varchar(30) DEFAULT NULL,
  `ref_nomor` varchar(30) DEFAULT NULL,
  `nomor` varchar(30) NOT NULL,
  `tanggal` date NOT NULL,
  `note` text,
  `total_price` float(20,0) DEFAULT '0',
  `status` int(1) NOT NULL DEFAULT '0',
  `is_editable` int(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `supplier_id` (`supplier_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `po`
--

LOCK TABLES `po` WRITE;
/*!40000 ALTER TABLE `po` DISABLE KEYS */;
INSERT INTO `po` VALUES (1,1,1,'Standar','','00001/PO/AJM/VIII/22','2022-08-31','Supplier’s Signed and Stamped',214200,0,0,'2022-08-31 07:57:29',14,'2022-08-31 07:58:03',14);
/*!40000 ALTER TABLE `po` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `po_item`
--

DROP TABLE IF EXISTS `po_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `po_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `po_id` int(11) NOT NULL,
  `po_item_parent_id` int(11) DEFAULT NULL,
  `nomor` varchar(30) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `quantity` float(20,0) DEFAULT NULL,
  `unit` varchar(30) DEFAULT NULL,
  `unit_price` float(20,0) DEFAULT NULL,
  `total_price` float(20,0) DEFAULT NULL,
  `is_bold` int(1) DEFAULT '0',
  `is_italic` int(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `rab_id` (`po_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `po_item`
--

LOCK TABLES `po_item` WRITE;
/*!40000 ALTER TABLE `po_item` DISABLE KEYS */;
INSERT INTO `po_item` VALUES (1,1,NULL,'1','Cross Flow Fan Assy PN. 4023347','',1,'pcs',214200,214200,NULL,NULL);
/*!40000 ALTER TABLE `po_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pr`
--

DROP TABLE IF EXISTS `pr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `supplier_id` int(11) NOT NULL,
  `nomor` varchar(30) NOT NULL,
  `tanggal` date NOT NULL,
  `note` text,
  `total_price` float(20,0) DEFAULT '0',
  `status` int(1) NOT NULL DEFAULT '0',
  `is_editable` int(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `supplier_id` (`supplier_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pr`
--

LOCK TABLES `pr` WRITE;
/*!40000 ALTER TABLE `pr` DISABLE KEYS */;
INSERT INTO `pr` VALUES (1,1,'00001/PR/AJM/IV/22','2022-04-27','Supplier’s Signed and Stamped',214200,1,1,'2022-04-27 11:25:55',16,'2022-05-08 20:54:18',16);
/*!40000 ALTER TABLE `pr` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pr_item`
--

DROP TABLE IF EXISTS `pr_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pr_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pr_id` int(11) NOT NULL,
  `pr_item_parent_id` int(11) DEFAULT NULL,
  `nomor` varchar(30) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `quantity` float(20,0) DEFAULT NULL,
  `unit` varchar(30) DEFAULT NULL,
  `unit_price` float(20,0) DEFAULT NULL,
  `total_price` float(20,0) DEFAULT NULL,
  `is_bold` int(1) DEFAULT '0',
  `is_italic` int(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `rab_id` (`pr_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pr_item`
--

LOCK TABLES `pr_item` WRITE;
/*!40000 ALTER TABLE `pr_item` DISABLE KEYS */;
INSERT INTO `pr_item` VALUES (1,1,NULL,'1','Cross Flow Fan Assy PN. 4023347','',1,'pcs',214200,214200,NULL,NULL);
/*!40000 ALTER TABLE `pr_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quotation`
--

DROP TABLE IF EXISTS `quotation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quotation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bq_id` int(11) DEFAULT NULL,
  `nomor` varchar(30) NOT NULL,
  `tanggal` date NOT NULL,
  `customer_name` varchar(150) NOT NULL,
  `customer_address` varchar(255) DEFAULT NULL,
  `customer_attn` varchar(150) DEFAULT NULL,
  `project_name` varchar(255) NOT NULL,
  `note` text,
  `total_price` float(20,0) DEFAULT '0',
  `file_po_path` varchar(255) DEFAULT NULL,
  `file_po_name` varchar(255) DEFAULT NULL,
  `file_po_uploaded_at` timestamp NULL DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quotation`
--

LOCK TABLES `quotation` WRITE;
/*!40000 ALTER TABLE `quotation` DISABLE KEYS */;
INSERT INTO `quotation` VALUES (1,1,'00001/Q/AJM/IV/22','2022-04-27','PT. Arya Jaya','Jl. Bandung Raya, Kota Bandung - Jawa Barat','Bpk. Saha','Pengembangan Aplikasi eProc v2','- Harga diatas belum termasuk PPN\r\n- Harga diatas berlaku 10 hari sejak penawaran ini diterbitkan\r\n- Pembayaran setelah pengerjaan selesai\r\n- Pengerjaan 4-6 hari setelah PO diterima',7050000,'directory/quotation/e399a3066944bca2545a01880ec682e9.pdf','e399a3066944bca2545a01880ec682e9.pdf','2022-04-27 09:12:35',1,'2022-04-27 09:00:05',15,'2022-04-27 09:08:20',15);
/*!40000 ALTER TABLE `quotation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quotation_item`
--

DROP TABLE IF EXISTS `quotation_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quotation_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `quotation_id` int(11) NOT NULL,
  `quotation_item_parent_id` int(11) DEFAULT NULL,
  `nomor` varchar(30) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `quantity` float(20,0) DEFAULT NULL,
  `unit` varchar(30) DEFAULT NULL,
  `unit_price` float(20,0) DEFAULT NULL,
  `total_price` float(20,0) DEFAULT NULL,
  `is_bold` int(1) DEFAULT '0',
  `is_italic` int(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `rab_id` (`quotation_id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quotation_item`
--

LOCK TABLES `quotation_item` WRITE;
/*!40000 ALTER TABLE `quotation_item` DISABLE KEYS */;
INSERT INTO `quotation_item` VALUES (41,1,NULL,'','Main Unit','',NULL,'',NULL,NULL,1,NULL),(42,1,41,'1','Axial Fan Fresh Air fan','',1,'unit',4500000,4500000,NULL,NULL),(43,1,42,NULL,'Type : Axial Fan',NULL,NULL,NULL,NULL,NULL,0,1),(44,1,42,NULL,'Brand : Vanco',NULL,NULL,NULL,NULL,NULL,0,1),(45,1,42,NULL,'Model : VAD/6-630',NULL,NULL,NULL,NULL,NULL,0,1),(46,1,42,NULL,'Power : 1500 watt',NULL,NULL,NULL,NULL,NULL,0,1),(47,1,42,NULL,'Speed : 960 rpm',NULL,NULL,NULL,NULL,NULL,0,1),(48,1,42,NULL,'Capacity : 10 mmwg',NULL,NULL,NULL,NULL,NULL,0,1),(49,1,42,NULL,'Electric Power : 380V/3Ph/1,5kW',NULL,NULL,NULL,NULL,NULL,0,1),(50,1,42,NULL,'Static Pressure : 11520 CMH',NULL,NULL,NULL,NULL,NULL,0,1),(51,1,41,'2','Jointing Untuk tray Basket','',1,'pcs',50000,50000,NULL,NULL),(52,1,51,NULL,'Lebar : 50',NULL,NULL,NULL,NULL,NULL,0,1),(53,1,51,NULL,'Tinggi : 100',NULL,NULL,NULL,NULL,NULL,0,1),(54,1,NULL,'3','Pre Filter G4','',1,'pcs',2500000,2500000,NULL,NULL);
/*!40000 ALTER TABLE `quotation_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rab`
--

DROP TABLE IF EXISTS `rab`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rab` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomor` varchar(30) NOT NULL,
  `tanggal` date NOT NULL,
  `customer_name` varchar(150) NOT NULL,
  `customer_address` varchar(255) DEFAULT NULL,
  `customer_attn` varchar(150) DEFAULT NULL,
  `project_name` varchar(255) NOT NULL,
  `note` text,
  `total_price` float(20,0) DEFAULT '0',
  `status` int(1) NOT NULL DEFAULT '0',
  `is_created_bq` int(1) DEFAULT '0',
  `is_created_lom` int(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rab`
--

LOCK TABLES `rab` WRITE;
/*!40000 ALTER TABLE `rab` DISABLE KEYS */;
INSERT INTO `rab` VALUES (3,'00001/RAB/AJM/IV/22','2022-04-19','PT. Arya Jaya','Jl. Bandung Raya, Kota Bandung - Jawa Barat','Bpk. Saha','Pengembangan Aplikasi eProc v2','- Harga bisa berubah jika terdapat perubahan pada task\r\n- Harga dibayar per 3 termin',7050000,1,1,1,'2022-04-19 05:14:18',12,'2022-04-27 07:46:32',12),(4,'00002/RAB/AJM/IX/22','2022-09-06','Cust #1','Bandung','Udin','Test #1','- Satu\r\n- Dua',46800000,1,0,0,'2022-09-06 10:53:13',12,NULL,NULL),(5,'00003/RAB/AJM/IX/22','2022-09-06','Cust #2','Bandung','Samsudin','Test #2','',0,0,0,0,'2022-09-06 11:02:13',12,'2022-09-06 11:19:12',12);
/*!40000 ALTER TABLE `rab` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rab_item`
--

DROP TABLE IF EXISTS `rab_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rab_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rab_id` int(11) NOT NULL,
  `rab_item_parent_id` int(11) DEFAULT NULL,
  `nomor` varchar(30) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `quantity` float(20,0) DEFAULT NULL,
  `unit` varchar(30) DEFAULT NULL,
  `material_unit_price` float(20,0) DEFAULT NULL,
  `material_total_price` float(20,0) DEFAULT NULL,
  `labour_unit_price` float(20,0) DEFAULT NULL,
  `labour_total_price` float(20,0) DEFAULT NULL,
  `is_bold` int(1) DEFAULT '0',
  `is_italic` int(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `rab_id` (`rab_id`)
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rab_item`
--

LOCK TABLES `rab_item` WRITE;
/*!40000 ALTER TABLE `rab_item` DISABLE KEYS */;
INSERT INTO `rab_item` VALUES (41,3,NULL,'','Main Unit','',NULL,'',NULL,NULL,NULL,NULL,1,NULL),(42,3,41,'1','Axial Fan Fresh Air fan','',1,'unit',4500000,4500000,NULL,NULL,NULL,NULL),(43,3,42,NULL,'Type : Axial Fan',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,1),(44,3,42,NULL,'Brand : Vanco',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,1),(45,3,42,NULL,'Model : VAD/6-630',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,1),(46,3,42,NULL,'Power : 1500 watt',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,1),(47,3,42,NULL,'Speed : 960 rpm',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,1),(48,3,42,NULL,'Capacity : 10 mmwg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,1),(49,3,42,NULL,'Electric Power : 380V/3Ph/1,5kW',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,1),(50,3,42,NULL,'Static Pressure : 11520 CMH',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,1),(51,3,41,'2','Jointing Untuk tray Basket','',1,'pcs',50000,50000,NULL,NULL,NULL,NULL),(52,3,51,NULL,'Lebar : 50',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,1),(53,3,51,NULL,'Tinggi : 100',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,1),(54,3,NULL,'3','Pre Filter G4','',1,'pcs',2500000,2500000,NULL,NULL,NULL,NULL),(55,4,NULL,'I','Judul','',NULL,'',NULL,NULL,NULL,NULL,1,NULL),(56,4,NULL,'I.1','Sub Judul','',NULL,'',NULL,NULL,NULL,NULL,1,NULL),(57,4,56,'','Jointing Untuk tray Basket','',10,'pcs',50000,500000,53000,530000,NULL,NULL),(58,4,57,NULL,'Lebar : 50',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,1),(59,4,57,NULL,'Tinggi : 100',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,1),(60,4,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(61,4,NULL,'II','Judul 2','',NULL,'',NULL,NULL,NULL,NULL,1,1),(62,4,61,'','Barang 1','Warna Hitam',12,'Pcs',10000,120000,12500,150000,NULL,NULL),(63,4,NULL,'','Axial Fan Fresh Air fan','',5,'unit',4500000,22500000,4600000,23000000,NULL,NULL),(64,4,63,NULL,'Type : Axial Fan',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,1),(65,4,63,NULL,'Brand : Vanco',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,1),(66,4,63,NULL,'Model : VAD/6-630',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,1),(67,4,63,NULL,'Power : 1500 watt',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,1),(68,4,63,NULL,'Speed : 960 rpm',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,1),(69,4,63,NULL,'Capacity : 10 mmwg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,1),(70,4,63,NULL,'Electric Power : 380V/3Ph/1,5kW',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,1),(71,4,63,NULL,'Static Pressure : 11520 CMH',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,1),(72,5,NULL,'A','Test 1','',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `rab_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(30) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'Administrator','2020-11-08 00:04:44',1,'2021-08-05 16:17:29',NULL),(2,'Engineering','2022-04-14 13:40:24',1,'2022-04-14 13:40:28',NULL),(3,'Sales','2022-04-14 13:40:24',1,'2022-04-14 13:40:28',NULL),(4,'Teknik','2022-04-14 13:40:24',1,'2022-04-14 13:40:28',NULL),(5,'Procurement','2022-04-14 13:40:24',1,'2022-04-14 13:40:28',NULL),(6,'Finance','2022-04-14 13:40:24',1,'2022-04-14 13:40:28',NULL);
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `setting`
--

DROP TABLE IF EXISTS `setting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `setting` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `data` varchar(100) NOT NULL,
  `content` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `setting`
--

LOCK TABLES `setting` WRITE;
/*!40000 ALTER TABLE `setting` DISABLE KEYS */;
INSERT INTO `setting` VALUES (1,'app_name','E-PROC'),(2,'app_version','1.0'),(3,'template_frontend',''),(4,'template_backend','sb_admin'),(5,'theme_color','blue'),(6,'smtp_protocol','smtp'),(7,'smtp_host','smtp.gmail.com'),(8,'smtp_port','587'),(9,'smtp_user','user@example.com'),(10,'smtp_pass','password_disini'),(11,'smtp_mailtype','html'),(12,'smtp_charset','iso-8859-1'),(13,'smtp_crypto','tls'),(14,'dashboard_image_source','directory/dashboard/f6a3d675838b53fba1aef05ac4bd2c1c.jpg'),(15,'dashboard_image_width','100'),(16,'dashboard_image_object_fit','cover'),(17,'dashboard_image_object_position','bottom'),(18,'dashboard_image_box_shadow','0'),(19,'dashboard_image_max_height','550'),(20,'pengajuan_nomor','{INC}-SRT/KAH/{MONTH}/{YEAR_2}'),(21,'api_url','https://url.example.com'),(22,'api_user','user_disini'),(23,'api_password','password_disini'),(24,'max_harga_beli','500000'),(25,'ppn','11');
/*!40000 ALTER TABLE `setting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `skb`
--

DROP TABLE IF EXISTS `skb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `skb` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `supplier_id` int(11) DEFAULT NULL,
  `nomor` varchar(30) NOT NULL,
  `tanggal` date NOT NULL,
  `customer_name` varchar(150) DEFAULT NULL,
  `customer_address` varchar(255) DEFAULT NULL,
  `customer_attn` varchar(150) DEFAULT NULL,
  `project_name` varchar(255) DEFAULT NULL,
  `note` text,
  `status` int(1) NOT NULL DEFAULT '0',
  `source` varchar(30) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `supplier_id` (`supplier_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `skb`
--

LOCK TABLES `skb` WRITE;
/*!40000 ALTER TABLE `skb` DISABLE KEYS */;
/*!40000 ALTER TABLE `skb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `skb_item`
--

DROP TABLE IF EXISTS `skb_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `skb_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `skb_id` int(11) NOT NULL,
  `skb_item_parent_id` int(11) DEFAULT NULL,
  `nomor` varchar(30) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `quantity` float(20,0) DEFAULT NULL,
  `unit` varchar(30) DEFAULT NULL,
  `is_bold` int(1) DEFAULT '0',
  `is_italic` int(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `rab_id` (`skb_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `skb_item`
--

LOCK TABLES `skb_item` WRITE;
/*!40000 ALTER TABLE `skb_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `skb_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sub_unit`
--

DROP TABLE IF EXISTS `sub_unit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sub_unit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unit_id` int(11) NOT NULL,
  `nama_sub_unit` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sub_unit`
--

LOCK TABLES `sub_unit` WRITE;
/*!40000 ALTER TABLE `sub_unit` DISABLE KEYS */;
INSERT INTO `sub_unit` VALUES (1,1,'Engineering'),(2,1,'Sales'),(3,1,'Teknik'),(4,1,'Procurement'),(5,1,'Finance');
/*!40000 ALTER TABLE `sub_unit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `supplier`
--

DROP TABLE IF EXISTS `supplier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `supplier` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `nama_supplier` varchar(200) NOT NULL,
  `alamat` text,
  `telepon` varchar(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(30) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `supplier`
--

LOCK TABLES `supplier` WRITE;
/*!40000 ALTER TABLE `supplier` DISABLE KEYS */;
INSERT INTO `supplier` VALUES (1,'Tanpa Supplier','Jl. Sindangsari Village No. 03','081111123100','2022-04-12 14:19:12',1,'2022-04-16 00:34:20',1);
/*!40000 ALTER TABLE `supplier` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `supplier_barang`
--

DROP TABLE IF EXISTS `supplier_barang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `supplier_barang` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `supplier_id` int(30) NOT NULL,
  `jenis` varchar(30) NOT NULL,
  `kode_barang` varchar(30) NOT NULL,
  `nama_barang` varchar(255) NOT NULL,
  `merk` varchar(150) DEFAULT NULL,
  `satuan` varchar(30) NOT NULL,
  `quantity` float(20,0) NOT NULL,
  `harga_satuan` float(20,0) NOT NULL,
  `total_harga` float(20,0) NOT NULL,
  `parameter` json DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(30) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(30) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `supplier_id` (`supplier_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `supplier_barang`
--

LOCK TABLES `supplier_barang` WRITE;
/*!40000 ALTER TABLE `supplier_barang` DISABLE KEYS */;
INSERT INTO `supplier_barang` VALUES (1,1,'AC','A-0001','Axial Fan Fresh Air fan',NULL,'unit',100,4500000,450000000,'{\"type\": \"Axial Fan\", \"brand\": \"Vanco\", \"model\": \"VAD/6-630\", \"power\": \"1500 watt\", \"speed\": \"960 rpm\", \"capacity\": \"10 mmwg\", \"electric_power\": \"380V/3Ph/1,5kW\", \"static_pressure\": \"11520 CMH\"}','2022-04-12 14:24:57',1,'2022-04-13 08:28:46',1),(2,1,'AC','A-0002','Axial Exhaust fan',NULL,'unit',100,5000000,500000000,'{\"type\": \"Axial Fan\", \"brand\": \"Vanco\", \"model\": \"VAD/4-630\", \"power\": \"3000 watt\", \"speed\": \"1440 rpm\", \"capacity\": \"16000 CMH\", \"electric_power\": \"380V/3Ph/3kW\", \"static_pressure\": \"18 mmwg\"}','2022-04-12 14:25:31',1,'2022-04-13 08:28:12',1),(3,1,'Lainnya','A-0003','Pre Filter G4',NULL,'pcs',100,2500000,250000000,NULL,'2022-04-12 14:26:27',1,'2022-04-13 08:22:45',1),(4,1,'Joiting','A-0004','Jointing Untuk tray Basket',NULL,'pcs',100,50000,5000000,'{\"lebar\": \"50\", \"tinggi\": \"100\"}','2022-04-13 06:17:09',1,'2022-04-14 09:48:11',1);
/*!40000 ALTER TABLE `supplier_barang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `surat_nomor`
--

DROP TABLE IF EXISTS `surat_nomor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `surat_nomor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unit` varchar(100) NOT NULL,
  `sub_unit` varchar(100) NOT NULL,
  `format_nomor` varchar(50) NOT NULL,
  `ref` varchar(50) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `surat_nomor`
--

LOCK TABLES `surat_nomor` WRITE;
/*!40000 ALTER TABLE `surat_nomor` DISABLE KEYS */;
/*!40000 ALTER TABLE `surat_nomor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `unit`
--

DROP TABLE IF EXISTS `unit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_unit` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `unit`
--

LOCK TABLES `unit` WRITE;
/*!40000 ALTER TABLE `unit` DISABLE KEYS */;
INSERT INTO `unit` VALUES (1,'Head Office');
/*!40000 ALTER TABLE `unit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `nama_lengkap` varchar(255) NOT NULL,
  `role` varchar(255) DEFAULT NULL,
  `unit` varchar(100) DEFAULT NULL,
  `sub_unit` varchar(100) DEFAULT NULL,
  `profile_photo` varchar(255) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `created_by` int(30) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(30) DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `nama_lengkap` (`nama_lengkap`),
  KEY `username` (`username`) USING BTREE,
  KEY `role` (`role`(1)),
  KEY `token` (`token`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'admin@example.com','admin','21232f297a57a5a743894a0e4a801fc3','Administrator','Administrator',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2022-04-14 13:49:34'),(12,'engineering@example.com','engineering','5d554bc5f3d2cd182cdd0952b1fb87ca','Admin Engineering','Engineering','Head Office','Engineering',NULL,NULL,NULL,'2022-04-14 13:49:29',NULL,'2022-04-14 13:52:53'),(13,'finance@example.com','finance','57336afd1f4b40dfd9f5731e35302fe5','Admin Finance','Finance','Head Office','Finance',NULL,NULL,NULL,'2022-04-14 13:50:04',NULL,'2022-04-14 13:51:29'),(14,'procurement@example.com','procurement','93aa88d53279ccb3e40713d4396e198f','Admin Procurement','Procurement','Head Office','Procurement',NULL,NULL,NULL,'2022-04-14 13:50:29',NULL,'2022-04-14 13:51:24'),(15,'sales@example.com','sales','9ed083b1436e5f40ef984b28255eef18','Admin Sales','Sales','Head Office','Sales',NULL,NULL,NULL,'2022-04-14 13:50:44',NULL,'2022-04-14 13:51:18'),(16,'teknik@example.com','teknik','58029eb6d2dd138b3da6ee4b2bb71d8c','Admin Teknik','Teknik','Head Office','Teknik',NULL,NULL,NULL,'2022-04-14 13:51:03',NULL,'2022-04-14 13:51:12');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `view_menu`
--

DROP TABLE IF EXISTS `view_menu`;
/*!50001 DROP VIEW IF EXISTS `view_menu`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_menu` (
  `id` tinyint NOT NULL,
  `parent_id` tinyint NOT NULL,
  `name` tinyint NOT NULL,
  `parent_name` tinyint NOT NULL,
  `link` tinyint NOT NULL,
  `icon` tinyint NOT NULL,
  `link_tobase` tinyint NOT NULL,
  `is_newtab` tinyint NOT NULL,
  `role_pic` tinyint NOT NULL,
  `order_pos` tinyint NOT NULL,
  `created_at` tinyint NOT NULL,
  `created_by` tinyint NOT NULL,
  `updated_at` tinyint NOT NULL,
  `updated_by` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `view_notification`
--

DROP TABLE IF EXISTS `view_notification`;
/*!50001 DROP VIEW IF EXISTS `view_notification`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_notification` (
  `id` tinyint NOT NULL,
  `user_from` tinyint NOT NULL,
  `user_from_nama_lengkap` tinyint NOT NULL,
  `user_from_role` tinyint NOT NULL,
  `user_to` tinyint NOT NULL,
  `user_to_nama_lengkap` tinyint NOT NULL,
  `user_to_role` tinyint NOT NULL,
  `ref` tinyint NOT NULL,
  `ref_id` tinyint NOT NULL,
  `description` tinyint NOT NULL,
  `link` tinyint NOT NULL,
  `is_read` tinyint NOT NULL,
  `created_date` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `view_po`
--

DROP TABLE IF EXISTS `view_po`;
/*!50001 DROP VIEW IF EXISTS `view_po`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_po` (
  `id` tinyint NOT NULL,
  `pr_id` tinyint NOT NULL,
  `supplier_id` tinyint NOT NULL,
  `tipe` tinyint NOT NULL,
  `ref_nomor` tinyint NOT NULL,
  `nomor` tinyint NOT NULL,
  `tanggal` tinyint NOT NULL,
  `note` tinyint NOT NULL,
  `total_price` tinyint NOT NULL,
  `status` tinyint NOT NULL,
  `is_editable` tinyint NOT NULL,
  `created_at` tinyint NOT NULL,
  `created_by` tinyint NOT NULL,
  `updated_at` tinyint NOT NULL,
  `updated_by` tinyint NOT NULL,
  `nama_supplier` tinyint NOT NULL,
  `alamat_supplier` tinyint NOT NULL,
  `telepon_supplier` tinyint NOT NULL,
  `pr_nomor` tinyint NOT NULL,
  `pr_tanggal` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `view_rab_bq`
--

DROP TABLE IF EXISTS `view_rab_bq`;
/*!50001 DROP VIEW IF EXISTS `view_rab_bq`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_rab_bq` (
  `id` tinyint NOT NULL,
  `nomor` tinyint NOT NULL,
  `tanggal` tinyint NOT NULL,
  `customer_name` tinyint NOT NULL,
  `customer_address` tinyint NOT NULL,
  `customer_attn` tinyint NOT NULL,
  `project_name` tinyint NOT NULL,
  `note` tinyint NOT NULL,
  `total_price` tinyint NOT NULL,
  `status` tinyint NOT NULL,
  `created_at` tinyint NOT NULL,
  `created_by` tinyint NOT NULL,
  `updated_at` tinyint NOT NULL,
  `updated_by` tinyint NOT NULL,
  `bq_id` tinyint NOT NULL,
  `bq_status` tinyint NOT NULL,
  `bq_created_at` tinyint NOT NULL,
  `bq_created_by` tinyint NOT NULL,
  `bq_updated_at` tinyint NOT NULL,
  `bq_updated_by` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `view_supplier`
--

DROP TABLE IF EXISTS `view_supplier`;
/*!50001 DROP VIEW IF EXISTS `view_supplier`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_supplier` (
  `id` tinyint NOT NULL,
  `nama_supplier` tinyint NOT NULL,
  `alamat` tinyint NOT NULL,
  `telepon` tinyint NOT NULL,
  `jumlah_barang` tinyint NOT NULL,
  `created_at` tinyint NOT NULL,
  `created_by` tinyint NOT NULL,
  `updated_at` tinyint NOT NULL,
  `updated_by` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `view_supplier_barang`
--

DROP TABLE IF EXISTS `view_supplier_barang`;
/*!50001 DROP VIEW IF EXISTS `view_supplier_barang`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_supplier_barang` (
  `id` tinyint NOT NULL,
  `supplier_id` tinyint NOT NULL,
  `jenis` tinyint NOT NULL,
  `kode_barang` tinyint NOT NULL,
  `nama_barang` tinyint NOT NULL,
  `merk` tinyint NOT NULL,
  `satuan` tinyint NOT NULL,
  `quantity` tinyint NOT NULL,
  `harga_satuan` tinyint NOT NULL,
  `total_harga` tinyint NOT NULL,
  `parameter` tinyint NOT NULL,
  `created_at` tinyint NOT NULL,
  `created_by` tinyint NOT NULL,
  `updated_at` tinyint NOT NULL,
  `updated_by` tinyint NOT NULL,
  `nama_supplier` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Dumping routines for database 'e_proc'
--

--
-- Final view structure for view `view_menu`
--

/*!50001 DROP TABLE IF EXISTS `view_menu`*/;
/*!50001 DROP VIEW IF EXISTS `view_menu`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013  SQL SECURITY DEFINER */
/*!50001 VIEW `view_menu` AS select distinct `m1`.`id` AS `id`,`m1`.`parent_id` AS `parent_id`,`m1`.`name` AS `name`,`m2`.`name` AS `parent_name`,`m1`.`link` AS `link`,`m1`.`icon` AS `icon`,`m1`.`link_tobase` AS `link_tobase`,`m1`.`is_newtab` AS `is_newtab`,`m1`.`role_pic` AS `role_pic`,`m1`.`order_pos` AS `order_pos`,`m1`.`created_at` AS `created_at`,`m1`.`created_by` AS `created_by`,`m1`.`updated_at` AS `updated_at`,`m1`.`updated_by` AS `updated_by` from (`menu` `m1` left join `menu` `m2` on((`m1`.`parent_id` = `m2`.`id`))) order by `m1`.`parent_id`,`m1`.`order_pos` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_notification`
--

/*!50001 DROP TABLE IF EXISTS `view_notification`*/;
/*!50001 DROP VIEW IF EXISTS `view_notification`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013  SQL SECURITY DEFINER */
/*!50001 VIEW `view_notification` AS select `t`.`id` AS `id`,`t`.`user_from` AS `user_from`,`u1`.`nama_lengkap` AS `user_from_nama_lengkap`,`u1`.`role` AS `user_from_role`,`t`.`user_to` AS `user_to`,`u2`.`nama_lengkap` AS `user_to_nama_lengkap`,`u2`.`role` AS `user_to_role`,`t`.`ref` AS `ref`,`t`.`ref_id` AS `ref_id`,`t`.`description` AS `description`,`t`.`link` AS `link`,`t`.`is_read` AS `is_read`,`t`.`created_date` AS `created_date` from ((`notification` `t` left join `user` `u1` on((`u1`.`id` = `t`.`user_from`))) left join `user` `u2` on((`u2`.`id` = `t`.`user_to`))) order by `t`.`id` desc */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_po`
--

/*!50001 DROP TABLE IF EXISTS `view_po`*/;
/*!50001 DROP VIEW IF EXISTS `view_po`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_unicode_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013  SQL SECURITY DEFINER */
/*!50001 VIEW `view_po` AS select `po`.`id` AS `id`,`po`.`pr_id` AS `pr_id`,`po`.`supplier_id` AS `supplier_id`,`po`.`tipe` AS `tipe`,`po`.`ref_nomor` AS `ref_nomor`,`po`.`nomor` AS `nomor`,`po`.`tanggal` AS `tanggal`,`po`.`note` AS `note`,`po`.`total_price` AS `total_price`,`po`.`status` AS `status`,`po`.`is_editable` AS `is_editable`,`po`.`created_at` AS `created_at`,`po`.`created_by` AS `created_by`,`po`.`updated_at` AS `updated_at`,`po`.`updated_by` AS `updated_by`,`s`.`nama_supplier` AS `nama_supplier`,`s`.`alamat` AS `alamat_supplier`,`s`.`telepon` AS `telepon_supplier`,`pr`.`nomor` AS `pr_nomor`,`pr`.`tanggal` AS `pr_tanggal` from ((`po` left join `supplier` `s` on((`s`.`id` = `po`.`supplier_id`))) left join `pr` on((`pr`.`id` = `po`.`pr_id`))) order by `po`.`created_at` desc */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_rab_bq`
--

/*!50001 DROP TABLE IF EXISTS `view_rab_bq`*/;
/*!50001 DROP VIEW IF EXISTS `view_rab_bq`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013  SQL SECURITY DEFINER */
/*!50001 VIEW `view_rab_bq` AS select `t`.`id` AS `id`,`t`.`nomor` AS `nomor`,`t`.`tanggal` AS `tanggal`,`t`.`customer_name` AS `customer_name`,`t`.`customer_address` AS `customer_address`,`t`.`customer_attn` AS `customer_attn`,`t`.`project_name` AS `project_name`,`t`.`note` AS `note`,`t`.`total_price` AS `total_price`,`t`.`status` AS `status`,`t`.`created_at` AS `created_at`,`t`.`created_by` AS `created_by`,`t`.`updated_at` AS `updated_at`,`t`.`updated_by` AS `updated_by`,`bq`.`id` AS `bq_id`,`bq`.`status` AS `bq_status`,`bq`.`created_at` AS `bq_created_at`,`bq`.`created_by` AS `bq_created_by`,`bq`.`updated_at` AS `bq_updated_at`,`bq`.`updated_by` AS `bq_updated_by` from (`rab` `t` left join `bq` on((`bq`.`rab_id` = `t`.`id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_supplier`
--

/*!50001 DROP TABLE IF EXISTS `view_supplier`*/;
/*!50001 DROP VIEW IF EXISTS `view_supplier`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013  SQL SECURITY DEFINER */
/*!50001 VIEW `view_supplier` AS select `t`.`id` AS `id`,`t`.`nama_supplier` AS `nama_supplier`,`t`.`alamat` AS `alamat`,`t`.`telepon` AS `telepon`,(select count(`supplier_barang`.`id`) from `supplier_barang` where (`supplier_barang`.`supplier_id` = `t`.`id`)) AS `jumlah_barang`,`t`.`created_at` AS `created_at`,`t`.`created_by` AS `created_by`,`t`.`updated_at` AS `updated_at`,`t`.`updated_by` AS `updated_by` from `supplier` `t` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_supplier_barang`
--

/*!50001 DROP TABLE IF EXISTS `view_supplier_barang`*/;
/*!50001 DROP VIEW IF EXISTS `view_supplier_barang`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_unicode_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013  SQL SECURITY DEFINER */
/*!50001 VIEW `view_supplier_barang` AS select `t`.`id` AS `id`,`t`.`supplier_id` AS `supplier_id`,`t`.`jenis` AS `jenis`,`t`.`kode_barang` AS `kode_barang`,`t`.`nama_barang` AS `nama_barang`,`t`.`merk` AS `merk`,`t`.`satuan` AS `satuan`,`t`.`quantity` AS `quantity`,`t`.`harga_satuan` AS `harga_satuan`,`t`.`total_harga` AS `total_harga`,`t`.`parameter` AS `parameter`,`t`.`created_at` AS `created_at`,`t`.`created_by` AS `created_by`,`t`.`updated_at` AS `updated_at`,`t`.`updated_by` AS `updated_by`,`s`.`nama_supplier` AS `nama_supplier` from (`supplier_barang` `t` left join `supplier` `s` on((`s`.`id` = `t`.`supplier_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-09-11 20:16:20
