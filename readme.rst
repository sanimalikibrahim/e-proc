#####
Setup
#####

-  Copy & paste "application/config/database.sample.php" to "application/config/database.php" and changes connection string
-  Open terminal and goto directory root path, and type "composer install"
-  Open phpMyAdmin / database manager, and then create new database with name "e_proc" and import database file in "database/e_proc.sql"

After setup finished, you can access the app with url "localhost/sim-logistik" in your browser.

##########
Disclaimer
##########

This application is intended for use in PT Karsa Abdi Husada.
